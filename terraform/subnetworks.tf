resource "google_compute_subnetwork" "devvm_vpc_us_west_4" {
  ip_cidr_range              = "10.182.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-west4"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_west_4 projects/opstrace-dev-bee41fca/regions/us-west4/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_southamerica_west_1" {
  ip_cidr_range              = "10.194.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "southamerica-west1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_southamerica_west_1 projects/opstrace-dev-bee41fca/regions/southamerica-west1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_norhteast_3" {
  ip_cidr_range              = "10.178.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-northeast3"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_norhteast_3 projects/opstrace-dev-bee41fca/regions/asia-northeast3/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_us_west_3" {
  ip_cidr_range              = "10.180.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-west3"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_west_3 projects/opstrace-dev-bee41fca/regions/us-west3/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_east_1" {
  ip_cidr_range              = "10.140.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-east1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_east_1 projects/opstrace-dev-bee41fca/regions/asia-east1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_west_9" {
  ip_cidr_range              = "10.200.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-west9"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_west_9 projects/opstrace-dev-bee41fca/regions/europe-west9/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_west_3" {
  ip_cidr_range              = "10.156.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-west3"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_west_3 projects/opstrace-dev-bee41fca/regions/europe-west3/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_north_1" {
  ip_cidr_range              = "10.166.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-north1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_north_1 projects/opstrace-dev-bee41fca/regions/europe-north1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_west_4" {
  ip_cidr_range              = "10.164.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-west4"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_west_4 projects/opstrace-dev-bee41fca/regions/europe-west4/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_southamerica_east1" {
  ip_cidr_range              = "10.158.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "southamerica-east1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_southamerica_east1 projects/opstrace-dev-bee41fca/regions/southamerica-east1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_us_east_4" {
  ip_cidr_range              = "10.150.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-east4"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_east_4 projects/opstrace-dev-bee41fca/regions/us-east4/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_east_2" {
  ip_cidr_range              = "10.170.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-east2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_east_2 projects/opstrace-dev-bee41fca/regions/asia-east2/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_west_6" {
  ip_cidr_range              = "10.172.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-west6"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_west_6 projects/opstrace-dev-bee41fca/regions/europe-west6/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_norhteast_2" {
  ip_cidr_range              = "10.174.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-northeast2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_norhteast_2 projects/opstrace-dev-bee41fca/regions/asia-northeast2/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_us_south_1" {
  ip_cidr_range              = "10.206.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-south1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_south_1 projects/opstrace-dev-bee41fca/regions/us-south1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_southeast_1" {
  ip_cidr_range              = "10.148.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-southeast1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_southeast_1 projects/opstrace-dev-bee41fca/regions/asia-southeast1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_us_west_2" {
  ip_cidr_range              = "10.168.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-west2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_west_2 projects/opstrace-dev-bee41fca/regions/us-west2/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_central_2" {
  ip_cidr_range              = "10.186.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-central2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_central_2 projects/opstrace-dev-bee41fca/regions/europe-central2/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_australia_southeast_2" {
  ip_cidr_range              = "10.192.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "australia-southeast2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_australia_southeast_2 projects/opstrace-dev-bee41fca/regions/australia-southeast2/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_west_1" {
  ip_cidr_range              = "10.132.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-west1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_west_1 projects/opstrace-dev-bee41fca/regions/europe-west1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_northamerica_northeast_2" {
  ip_cidr_range              = "10.188.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "northamerica-northeast2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_northamerica_northeast_2 projects/opstrace-dev-bee41fca/regions/northamerica-northeast2/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_south_1" {
  ip_cidr_range              = "10.160.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-south1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_south_1 projects/opstrace-dev-bee41fca/regions/asia-south1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_us_east_1" {
  ip_cidr_range              = "10.142.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-east1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_east_1 projects/opstrace-dev-bee41fca/regions/us-east1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_me_west_1" {
  ip_cidr_range              = "10.208.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "me-west1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_me_west_1 projects/opstrace-dev-bee41fca/regions/me-west1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_us_east_5" {
  ip_cidr_range              = "10.202.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-east5"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_east_5 projects/opstrace-dev-bee41fca/regions/us-east5/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_us_central_1" {
  ip_cidr_range              = "10.128.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-central1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_central_1 projects/opstrace-dev-bee41fca/regions/us-central1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_us_west_1" {
  ip_cidr_range              = "10.138.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "us-west1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_us_west_1 projects/opstrace-dev-bee41fca/regions/us-west1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_southeast_2" {
  ip_cidr_range              = "10.184.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-southeast2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_southeast_2 projects/opstrace-dev-bee41fca/regions/asia-southeast2/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_northeast_1" {
  ip_cidr_range              = "10.146.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-northeast1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_northeast_1 projects/opstrace-dev-bee41fca/regions/asia-northeast1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_west_2" {
  ip_cidr_range              = "10.154.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-west2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_west_2 projects/opstrace-dev-bee41fca/regions/europe-west2/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_west_8" {
  ip_cidr_range              = "10.198.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-west8"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_west_8 projects/opstrace-dev-bee41fca/regions/europe-west8/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_australia_southeast_1" {
  ip_cidr_range              = "10.152.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "australia-southeast1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_australia_southeast_1 projects/opstrace-dev-bee41fca/regions/australia-southeast1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_northamerica_northeast_1" {
  ip_cidr_range              = "10.162.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "northamerica-northeast1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_northamerica_northeast_1 projects/opstrace-dev-bee41fca/regions/northamerica-northeast1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_europe_southwest_1" {
  ip_cidr_range              = "10.204.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "europe-southwest1"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_europe_southwest_1 projects/opstrace-dev-bee41fca/regions/europe-southwest1/subnetworks/devvm-vpc

resource "google_compute_subnetwork" "devvm_vpc_asia_south_2" {
  ip_cidr_range              = "10.190.0.0/20"
  name                       = "devvm-vpc"
  network                    = google_compute_network.devvm_vpc.self_link
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  project                    = var.project_id
  purpose                    = "PRIVATE"
  region                     = "asia-south2"
  stack_type                 = "IPV4_ONLY"
}
# terraform import google_compute_subnetwork.devvm_vpc_asia_south_2 projects/opstrace-dev-bee41fca/regions/asia-south2/subnetworks/devvm-vpc
