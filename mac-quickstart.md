# devvm Installation Guide

## Overview

This guide is to help Mac users set up and access `devvm`. It's aimed at non-engineers, so there's additional commentary that might be annoying for experts.

You can also [watch Pawel's video](https://youtu.be/kt7CVfIH_Zk) to see `devvm` in action.

### What is devvm?

`devvm` is a virtual machine, fully configured with the GitLab Observability Stack (GOS). This provides a working system for development and experimentation. It's also a good way to share you work with others for collaboration and review. If you break your `devvm`, you can just delete it and start again!

### Prerequisites

Before you start, you'll need the following tools:

- [Python](https://www.python.org/downloads/macos/) - run the `python3 -v` or `python -v` command in your terminal to check whether it's already installed. This is required for gcloud CLI.
- [gcloud CLI](https://cloud.google.com/sdk/docs/install) for connecting to GCP. This process should also install kubectl, which you'll need later for connecting to the remote Kubernetes cluster.
- [WireGuard](https://www.wireguard.com/install/) for creating a VPN tunnel.

## Getting Started

### Access Google Cloud Platform (GCP)

If you haven't done so already, you'll need to:

- Create a GCP account on [Sandbox Cloud](https://gitlabsandbox.cloud/).
- Request access to the **opstrace-dev-bee41fca** project through the #g_observability Slack channel.

### Authenticate with gcloud

In your terminal, run the following command:

`gcloud auth login`

You'll be prompted by your browser to authenticate with Okta and your GMail address.

Then run the following command:

`gcloud config set project opstrace-dev-bee41fca`

This configures gcloud to connect with the appropriate project on GCP.

### Clone the repository

Clone the `devvm` repository to your local machine:

`git clone git@gitlab.com:gitlab-org/opstrace/devvm.git`

## Create and access the `devvm`

### Run the shell script

Now you run the shell script to create your `devvm` as a VM (virtual machine) on GCP.

Switch to the `devvm` directory, then run the `devvm.sh` shell script with the following command:

`./devvm.sh <unique-name>`

Replace <unique-name> with a distinctive name for your VM. Your name, plus `-devvm` is a good choice. For example:

`./devvm.sh catherinepope-devvm`

This makes it easier for you to identify your VM on GCP (and you're less likely to accidentally tear down someone else's!).

You can provide additional arguments for this command to override the default settings, but this is usually unnecessary.

Wait 1-2 minutes, then you'll see confirmation that your `devvm` has been created:

```shell
NAME         = catherinepope-devvm
GOB BRANCH   = main

Created [https://www.googleapis.com/compute/v1/projects/opstrace-dev-bee41fca/zones/europe-west1-b/instances/catherinepope-devvm].
```

Sometimes [you will see errors](https://gitlab.com/gitlab-org/opstrace/devvm/#gcloud-compute-ssh-failed-with-some-cryptic-message-what-should-i-do), even though the script executed successfully. If this happens, wait a couple of minutes and retry the command.

### Access your devvm

To access your `devvm`, run the following command in the terminal:

`gcloud compute ssh <devvm-name>`

You should see a welcome message, and your prompt changes to:

`username_gitlab_com@devvm-name:~$`

For example: `cpope_gitlab_com@catherinepope-devvm:~$`.

This shows you're connected to your VM on GCP.

### Edit your /etc/hosts file

Scroll up in your terminal to find a message similar to this:

```shell
Please make sure to add these entries to your /etc/hosts file

10.15.17.1 gdk.devvm
10.15.16.129 gob.devvm
```

You need to edit your `/etc/hosts` file so you can access those IP addresses with the domains provided.

If you've not done this before, here's the simplest method:

1. Open a new tab or window in your terminal. You need to be on the local machine, rather than `devvm` (which should remain connected).
1. In your local terminal, enter `sudo vim /etc/hosts` to open the file in VIM, a simple text editor. Don't forget `sudo`, as you'll need administrator privileges.
1. Press the `i` key for insert mode.
1. Paste those two entries for `gdk.devvm` and `gob.dvvm` at the end of your file.
1. Press `esc`, then enter `:wq` to save your changes and quit the text editor.

### Get information about your devvm

In your **remote** terminal tab, enter the following command:

`sudo -u dev /home/dev/devvm/go/booter -watch`

This logs you in with the `dev` account and allows you to see the status of your deployment. It will take a few minutes, then you should see green `[OK]` signs, along with various login credentials. This information repeats on a loop, which you can cancel by pressing `ctrl + c`.

Then you can scroll up to retrieve the information you need.

### Connect to the WireGuard UI

WireGuard establishes a secure tunnel between your local and remote machines.

In your local terminal, look for a line similar to this (your IP address will be different):

`ui is now reachable at: https://35.205.176.220:5000/`

Copy and paste the URL into your browser. As it's using a self-signed certificate, you'll see a dramatic warning that it's trying to steal your credit card details. It isn't. Select **Advanced**, then **Proceed**.

Now you should see the WireGuard UI login screen.

![WireGuard UI login screen](doc/assets/wireguard-10_login.png)

You'll find your login credentials in the terminal, just above the UI URL you copied a moment ago.

Copy and paste them into the login screen, then select **Sign In**.

### Create a WireGuard client

You should now see the WireGuard clients page. In the top right-hand corner, select the **New Client** button.

![Create new WireGuard client](doc/assets/wireguard-20_new_client.png)

Give it a name (anything you like), then select **Submit**. 

![Enter name for WireGuard client](doc/assets/wireguard-30_new_client_name.png)

There's no need to complete any of the other fields.

Hopefully, you'll see your new client.

Select **Apply Config** in the top right-hand corner, then accept the confirmation message.

![Apply Config in WireGuard](doc/assets/wireguard-40_apply_config.png)

In the box for your client, select **Download* to download the `conf` file to your local machine.

![Download configuration file in WireGuard](doc/assets/wireguard-50_save_profile_file.png)

### Configure your WireGuard tunnel

Open WireGuard on your local machine. If you didn't install it earlier, follow [this link for instructions](https://www.wireguard.com/install/).

1. In WireGuard, select **Import tunnel(s) from file** on the home screen.
1. Choose the file you just downloaded, and select **Import**. 
1. Select **Allow** so WireGuard can add VPN configurations.
1. In WireGuard, select **Activate**

![Activate WireGuard tunnel](doc/assets/wireguard-55_profile_activate.png)

Your WireGuard tunnel should now show a status of *Active*.

To test connectivity, run the following command from your **local** machine:

`ping -c3 gdk.devvm`

If WireGuard is working correctly, you should see something similar to:

```shell
PING gdk.devvm (10.15.17.1) 56(84) bytes of data.
64 bytes from gdk.devvm (10.15.17.1): icmp_seq=1 ttl=64 time=28.4 ms
64 bytes from gdk.devvm (10.15.17.1): icmp_seq=2 ttl=64 time=27.7 ms
64 bytes from gdk.devvm (10.15.17.1): icmp_seq=3 ttl=64 time=27.8 ms

--- gdk.devvm ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 27.731/27.976/28.408/0.306 ms
```

### Copy kubeconfig from the remote machine

To access your Kubernetes cluster locally, you need to copy the kubeconfig file from the remote machine.

In the terminal tab for your `devvm`, enter the following command:

`sudo cp -v /home/dev/kubeconfig .`

Don't omit the period/full stop at the end, it's important!

This copies the configuration file to the current directory on the remote machine.

### Copy kubeconfig to your local machine

In the terminal tab for your local machine, enter `cd` to go to your home directory, then enter the following command:

`gcloud compute scp --zone europe-west1-b <devvm-name>:~/kubeconfig ~/kubeconfig`

This command securely copies the kubconfig file from your `devvm` to your local machine. Replace <devvm-name> with the name of your `devvm`.

You'll see a message similar to this:

`kubeconfig   100% 5615   125.2KB/s   00:00`

### Connect to your Kubernetes cluster

Your kubeconfig file provides kubectl with the credentials it needs to access your remote cluster.

First create an environment variable that points to the kubeconfig file you just copied:

`export KUBECONFIG=/Users/<username>/kubeconfig`

Replace <username> with your Mac username.

Now run the following command to check you can access your Kubernetes cluster:

`kubectl cluster-info`

You should see output similar to:

```shell
Kubernetes control plane is running at https://10.15.16.4:6443
CoreDNS is running at https://10.15.16.4:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

To view all your pods, run:

`kubectl get pods`

You'll see all the different components in your Kubernetes cluster, including Jaeger, NGINX, and Prometheus.

### Connect to GDK

You're nearly there! Now everything's running, you can connect to the GitLab Development Kit (GDK).

Again, the credentials are lurking in your remote terminal. If you cleared your screen, run the following command again:

`sudo -u dev /home/dev/devvm/go/booter -watch`

You're looking for something similar to:

```shell
GitLab is now available at https://gdk.devvm:3443/
    Username: root
    Password: VfXX96Qbod14r3eLrSH20wKfx0h1BfP3
```

Before you connect, make sure you:

- Added `gdk.devvm` to your `/etc/hosts` file.
- Configured your WireGuard tunnel.

Paste the provided URL into your browser. Again, you'll see a scary warning. Select **Advanced**, then **Proceed to gdk.devvm**.

Now log in to GDK with the credentials you just retrieved.

## Delete `devvm`

When you no longer need `devvm`, delete it with the following command:

`./devvm.sh --delete devvm-1`

## Troubleshooting

For common problems and fixes, see the [main `devvm` README](https://gitlab.com/gitlab-org/opstrace/devvm/-/blob/main/README.md).
