---
- name: Install gdk base deps
  ansible.builtin.apt:
    name:
      - make
      - curl
      - mkcert
      - moreutils
      - software-properties-common
      - libffi-dev
    state: present
  register: __pkgmgr_result
  until: __pkgmgr_result is succeeded
  retries: 3
  delay: 3

- name: We need latest git
  ansible.builtin.apt_repository:
    repo: ppa:git-core/ppa

- name: Install latest git
  ansible.builtin.apt:
    name: git
    state: present
  register: __pkgmgr_result
  until: __pkgmgr_result is succeeded
  retries: 3
  delay: 3

# Using git: url does not work as we do not have access to ssh auth sock.
# Module itself does not support setting owner of the checkout out files:
#   https://github.com/ansible/ansible/issues/51601
# So for now we use unauthed URL and pray that nothing breaks
- name: Checkout gitlab-development-kit
  ansible.builtin.git:
    repo: https://gitlab.com/gitlab-org/gitlab-development-kit.git
    dest: /home/dev/gitlab-development-kit/
    accept_hostkey: true
    version: main
  # ssh auth sock issues
  become: true
  become_flags: -SHE
  become_user: dev

- name: Certs
  block:
  - name: Create certs for gdk
    ansible.builtin.shell:
    args:
      cmd: |
        set -eou pipefail
        mkcert -install
        mkcert \
          -cert-file /home/dev/gitlab-development-kit/gdk.devvm.pem \
          -key-file /home/dev/gitlab-development-kit/gdk.devvm.key.pem \
            gdk.devvm {{ gdk_ip }}
      chdir: /home/dev/gitlab-development-kit/
      executable: /bin/bash
      creates: /home/dev/gitlab-development-kit/gdk.devvm.pem
    become: true
    become_user: dev

  - name: Ensure CA cert is available in gdk repo
    ansible.builtin.copy:
      remote_src: true
      src: /home/dev/.local/share/mkcert/rootCA.pem
      dest: /home/dev/gitlab-development-kit/localhost.crt
      owner: dev
      group: dev
      mode: 0644

  - name: Adjust permissions and ownership for gdk certs
    ansible.builtin.file:
      path: "{{ item }}"
      owner: root
      group: dev
      mode: 0640
    loop:
      - /home/dev/gitlab-development-kit/gdk.devvm.pem
      - /home/dev/gitlab-development-kit/gdk.devvm.key.pem


- name: Ensure /etc/hosts entries
  block:
  - name: ensure /etc/hosts entry for gdk.devvm exists
    ansible.builtin.lineinfile:
      path: /etc/hosts
      regexp: gdk\.devvm
      line: "{{ gdk_ip }} gdk.devvm"

  - name: ensure /etc/hosts entry for gob.devvm exists
    ansible.builtin.lineinfile:
      path: /etc/hosts
      regexp: gob\.devvm
      line: "{{ vip_start_ip }} gob.devvm"

- name: Config
  block:
  - name: Upload gdk config file
    ansible.builtin.copy:
      src: gdk.conf.yml
      dest: /home/dev/gitlab-development-kit/gdk.yml
      owner: dev
      group: dev
      mode: 0644
    notify: reconfigure gdk

  # Coloring of output breaks parsing it with the tooling due to controll chars
  - name: Disable coloring in GDKs output
    copy:
      content: Pry.color = false
      dest: /home/dev/.pryrc
      owner: dev
      group: dev
      mode: 0644

  # Coloring of output breaks parsing it with the tooling due to controll chars
  - name: Change URL used by observability tab
    copy:
      src: env.runit
      dest: /home/dev/gitlab-development-kit/env.runit
      owner: dev
      group: dev
      mode: 0644

- name: Bootstrap GDK packages
  ansible.builtin.shell:
  args:
    cmd: |
      set -eou pipefail
      . $HOME/.asdf/asdf.sh

      make bootstrap 2>&1 | ts '[%Y-%m-%d %H:%M:%S]' | tee -a make.bootstrap.log
    chdir: /home/dev/gitlab-development-kit/
    executable: /bin/bash
    creates: /home/dev/gitlab-development-kit/.cache/.gdk_platform_setup
  become: true
  become_user: dev

- name: Perform gdk install
  ansible.builtin.shell:
  args:
    cmd: |
      set -eou pipefail
      . $HOME/.asdf/asdf.sh

      # Another workaround to make GDK install:
      cp -v ./.tool-versions ../
      gdk install 2>&1 | ts '[%Y-%m-%d %H:%M:%S]' | tee -a gdk-install.log
      gdk stop
      rm -v ../.tool-versions
    chdir: /home/dev/gitlab-development-kit/
    executable: /bin/bash
  become: true
  become_user: dev
