#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo "devvm.sh script has been replaced by a CLI, please refer to README.md for more information"
echo "The TLDR is:"
echo
echo "cd ${SCRIPT_DIR}/go/"
echo "go build -o devvm ./cmd/devvm/main.go"
echo "./devvm create <options>"
echo
echo "Check out other subcommands of the devvm CLI too!"
