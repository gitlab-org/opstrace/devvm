# GitLab Observability Stack development environments

This repository contains the code used to set up virtual machines with fully configured GitLab Observability Stack.
The intention is to provide developers with a working system that they can use for development and experimentation, as well as a way for them to share their work with others for collaboration and/or review.

[[_TOC_]]

## Terminology

Across this document, we use the following terms:

- Booter: the program that runs on the `devvm` during its start that sets up all components of GOS. It is started automatically by systemd while the machine starts.
- `devvm`: to refer to the virtual machine running GitLab Observability Stack development environment.
- GOB: GitLab Observability Backend.
- GOS: GitLab Observability Stack

## Known limitations

- Deploying `devvm` requires one final manual step. Improving this requires [groups reconciliation to move out of Gatekeeper](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1942).

## Quickstart

This quickstart outlines the steps needed to set up a fully working development environment.

You can also watch these steps in the [video demo](https://youtu.be/kt7CVfIH_Zk).
Most of the bugs mentioned there are already fixed.

If you are a Mac user and require more detailed explanations of each step, you can use the [Mac quick start guide](mac-quickstart.md).

### Prerequisites

To successfully deploy `devvm`, you must have:

- A GcE account with sufficient privileges to create and delete virtual machines in the `opstrace-dev-bee41fca` project.
- The `gcloud` tool installed. For more details, see [`gcloud` CLI docs](https://cloud.google.com/sdk/docs/install).
- The `Wireguard` app installed. See https://www.wireguard.com/install/.

### Use `devvm` CLI tool

The `devvm` CLI binary is a tool that automates operations on devvms.
It is written in Go and requires to be compiled after the first check out of the repository and after every update.
This can be done by executing the following steps in the devvm repository checkout:

```shell
$ cd go/
$ go build -o devvm ./cmd/devvm/main.go
```

Run it the CLI tool without any arguments to print the help doc:

```shell
$ ./devvm
devvm is a tool to manage devvms

Usage:
  devvm [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  create      Launch a devvm
  delete      Delete a devvm
  help        Help about any command
  list        List devvms

Flags:
  -h, --help               help for devvm
      --log-level string   logging subsystem log level (default "info")

Use "devvm [command] --help" for more information about a command.
```

See the help of individual subcommands for information about the available options and their defaults.

The CLI uses Application Default Credentials in order to perform GCE API calls, please make sure that you have set them up correctly ([documentation](https://cloud.google.com/docs/authentication/provide-credentials-adc)).

### Creating a `devvm` VM.

`devvm` requires you to provide an ssh-key in order to be able to access your devvm.
In order to create an ssh-key suitable for devvm, try executing:

```shell
$ ssh-keygen -t ecdsa -f devvm_key.ssh_key
```

It is recommended, but not required, to set up a passphrase for the key.
Please note down the path to the private key generated above, we will use it in the later steps througout the document.

Launching devvm itself can be done with a single CLI `create` call.
Usually, you will simply use:

```shell
$ ./devvm create --demo-app --ssh-key-path ./devvm_key.ssh_key devvm-1
2023-10-09T14:49:07+02:00       info    SSH key read from /home/vespian/devvm_key.ssh_key
2023-10-09T14:49:07+02:00       info    Instance ensured        {"created": false}
2023-10-09T14:49:08+02:00       info    Devvm public IP: 35.187.62.241
2023-10-09T14:49:12+02:00       info    License file has been uploaded
2023-10-09T14:49:17+02:00       info    Booter finished initialization of the devvm
2023-10-09T14:49:17+02:00       info    Admin token: 9MRfHoe8CAZyHfh6o1q01ctZ30W6yu3V
2023-10-09T14:49:17+02:00       info    k8s client through SOCKs proxy is working
2023-10-09T14:49:26+02:00       info    http client through SOCKs proxy is working
2023-10-09T14:49:26+02:00       info    All done!
```

In case you launch devvm from non-defeault (`main`) branch:

1. The booter verifies if GOB Docker images are already present in their respective registries before `devvm` starts to boot.
   If images are missing, it reports an error.
1. After the booter determines the Docker image tags, new pushes or image versions for the given branch names are not taken into consideration.

To update the container references, the easiest way is to create a new `devvm` and destroy old one.

You should launch `devvm` in the zone close to your location for minimal latency.
By default, we launch it in Europe/Belgium (`europe-west1-b`).
For the list of all GCE zones, see [GCE documentation](https://cloud.google.com/compute/docs/regions-zones#available).

For the rest of this tutorial, we assume that we launched a `devvm` named `devvm-1` using the `main` GOB branch in the default zone `europe-west1-b`.

### Log into the `devvm`

In order to log into the devvm, you need its public IP.
You can find it in the output of the `create` subcommand, or using the `list` CLI:

```shell
$ ./devvm list
europe-west1-b
- devvm-1 RUNNING 1.2.3.4
```

Now you can log into the devvm:

```shell
$ ssh -i ./devvm_key.ssh_key dev@1.2.3.4
Enter passphrase for key '/home/vespian/devvm_key.ssh_key':
Welcome to devvm-1!

Please make sure to add these entries to your /etc/hosts file

10.15.17.1 gdk.devvm
10.15.16.129 gob.devvm

In order to determine if machine is healthy, please execute:

sudo -u dev /home/dev/devvm/go/booter -watch

Happy hacking!
Last login: Sun Oct  8 19:07:19 2023 from 188.98.142.224
-bash: warning: setlocale: LC_ALL: cannot change locale (en_GB.UTF-8)
dev@devvm-1:~$
```

Follow the instructions in the command output and add the required entries to your _local_ `/etc/hosts` file (not in the devvm filesystem).
We will need them later on.

### Start monitoring the progress of booter

> The `devvm` credentials used here were already deleted before publishing this text.

As mentioned in the welcome message after you logged in, you can monitor the setup of the `devvm`.

```shell
dev@devvm-1:~$ /home/dev/devvm/go/booter
```

The booter will continuously update the status as the setup process progresses.
At the end of the process, you should see something like this:

```plaintext
    | Booter status:
    | Reading Network addresation information:                 [OK]
    |         GDK IP: 10.15.17.1
    |         K8s VIP range start: 10.15.16.129
    |         K8s VIP range end: 10.15.16.160
    |         Stage retries count: 0
    |         Stage total time: 1.214521ms
(2) | Wireguard VPN:                                           [OK]
(3) |         ui is now reachable at: https://35.195.170.135:5000/
(1) |         username: condescending_mayer
    |         password: 27iKthPV6guHC1v36ioj54qee6KBipR2
    |         Stage retries count: 0
    |         Stage total time: 1.000094441s
    | GOB branch name and commit ID resolution/verification:   [OK]
    |         GOB commit ID: 76088dc2ef8907df59aa9ec905068e9ef941d905
    |         GOB images tag: 0.3.0-76088dc2
    |         Opentelemetry-demo enable: true
    |         ATTENTION: Subsequent pushes to GOB branches will be ignored.
    |         ATTENTION: Please re-create the dev vm if you would like to update the references.
    |         Stage retries count: 0
    |         Stage total time: 1.060682ms
    | GOB repo preparation:                                    [OK]
    |         Stage retries count: 0
    |         Stage total time: 14.012054156s
    | Ensure GDK is UP:                                        [OK]
    |         GitLab is now available at https://gdk.devvm:3443/
(4) |         Username: root
    |         Password: 5120Nms1tmw1AWv6n2brCQ2RmeLq5CQE
    |         Gitlab API Admin Token: 9aadsfgsghrvfasdfahgwredasfasdfs
    |         EE License uploaded: true
    |         ATTENTION: Please remember to connect through VPN to devvmm and update /etc/hosts file first!
    |         Stage retries count: 0
    |         Stage total time: 1m20.318345123s
(5) | Ensure Kind cluster is running:                          [OK]
    |         kubectl is available at /home/dev/kubeconfig
    |         try copying it to your home dir and using:
    |                 gcloud compute scp --zone <devvm-zone> <devvm-name>:~/kubeconfig ~/kubeconfig
    |         Stage retries count: 0
    |         Stage total time: 1.005831106s
    | Ensure GDK OAuth credentials and GOB OAuth data:         [OK]
    |         Application ID: 16191321438627058194990bc0a46d5b0c0cd3a8775df774cb226c3eda15318b
    |         Application secret:
    |         Error tracking shared secret: xXnN8sxS4oWpvPag_1M-
    |         Stage retries count: 0
    |         Stage total time: 629.26359ms
    | Ensure GOB cluster is deployed:                          [OK]
    |         Stage retries count: 0
    |         Stage total time: 3.102663036s
    | Ensure auxilary k8s manifests are deployed:              [OK]
    |         GOB is available at https://gob.devvm/
    |         Stage retries count: 0
    |         Stage total time: 824.29101ms
    | Ensure GOB tenant:                                       [OK]
(8) |         Gitlab Group ID: 22
    |         Gitlab Group Name: Toolbox
(7) |         Gitlab Group URL: https://gdk.devvm:3443/groups/toolbox
    |         Gitlab Project ID: 1
    |         Gitlab Project Name: Gitlab Smoke Tests
    |         Gitlab Project URL: https://gdk.devvm:3443/toolbox/gitlab-smoke-tests
(6) |         ATTENTION: Visit https://gob.devvm/v1/provision/22 to finalize provisioning tenant
    |         Stage retries count: 0
    |         Stage total time: 4.147283174s
    | Ensure otel-telemetry demo manifests are deployed:       [OK]
    |         Release status: failed
    |         Namespace: otel-demo-app
    |         Stage retries count: 0
    |         Stage total time: 84.216145ms
```

The column on the left with numbers in parentheses was added so we can refer to specific lines here.
In real output the numbers are not there.

Each paragraph is a separate stage that the booter progresses through.
In case of an error, a detailed error message is printed and the booter is restarted after few seconds.

You can also use the `-watch` flag to make the booter continuously print the status of the bootstrap process:

```shell
/home/dev/devvm/go/booter -watch
```

To terminate the program, press `Ctrl` + `C` or `Command` + `C`.

It is not necessary to wait for the booter to finish. You can execute steps below as soon as the booter marks the given stage as done (`[OK]`).

### Set up WireGuard tunneling

After the booter successfully completes the `WireGuard-UI services start` (2) stage, you can set up and download a WireGuard profile.
Enter the URL that is located on line (3) in your browser.
It is expected that certificate is not trusted, as `devvm` uses a self-signed one.

To create a WireGuard profile:

   1. Enter the username and password from line (1).

      ![wg-ui_login](doc/assets/wireguard-10_login.png)

   1. Select **New client**.

      ![wg-ui_new_client](doc/assets/wireguard-20_new_client.png)

   1. Enter a meaningful name for the new client.

      ![wg-ui_new_client_name](doc/assets/wireguard-30_new_client_name.png)

   1. Select **Submit**.
   1. Select **Apply Config**.

      ![wg-ui_apply_config](doc/assets/wireguard-40_apply_config.png)

   1. Download the WireGuard profile file for the newly created user.

      ![wg-ui_download_config](doc/assets/wireguard-50_save_profile_file.png)

   1. Import the profile on your workstation. These steps differ depending on your OS.

      - On an Ubuntu 22.04 LTS workstation running `NetworkManager`:

        ```shell
        nmcli connection import type wireguard file <path-to-downloaded-profile-file>
        ```

      - For MacOS:

        1. Follow the instructions in [Using WireGuard on macOS](https://mullvad.net/en/help/wireguard-macos-app/) but use the `WireGuard` app.
        1. Activate the profile once you imported it:

           ![wg-mac_activate](doc/assets/wireguard-55_profile_activate.png)

   1. Confirm connectivity by pinging a remote IP from your workstation:

      ```shell
      $ ping -c3 gdk.devvm

      PING gdk.devvm (10.15.17.1) 56(84) bytes of data.
      64 bytes from gdk.devvm (10.15.17.1): icmp_seq=1 ttl=64 time=28.4 ms
      64 bytes from gdk.devvm (10.15.17.1): icmp_seq=2 ttl=64 time=27.7 ms
      64 bytes from gdk.devvm (10.15.17.1): icmp_seq=3 ttl=64 time=27.8 ms

      --- gdk.devvm ping statistics ---
      3 packets transmitted, 3 received, 0% packet loss, time 2003ms
      rtt min/avg/max/mdev = 27.731/27.976/28.408/0.306 ms
      ```

### Set up Kubernetes configuration

After the booter successfully gets past the `Ensure Kind cluster is running` (5) state, you can download `kubeconfig` for the remote cluster:

1. Copy the `kubeconfig` onto your workstation:

   ```shell
   scp dev@<devvm-public-ip>:~/kubeconfig <destination dir on your workstation>
   ```

1. Make `kubectl` use the config file:

   ```shell
   export KUBECONFIG=<destination dir on your workstation>/kubeconfig
   ```

1. Verify that you can reach the remote cluster:

   ```shell
   $ kubectl cluster-info

   Kubernetes control plane is running at https://10.15.16.4:6443
   CoreDNS is running at https://10.15.16.4:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

   To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
   ```

   ```shell
   $ kubectl get pods
   NAME                                            READY   STATUS    RESTARTS        AGE
   cainjector-8bf8d856d-jm8tw                      1/1     Running   0             25h
   certmanager-545f6cf79d-nscfv                    1/1     Running   0             25h
   clickhouse-operator-7bcdbfb6df-f76pn            1/1     Running   0             25h
   cluster-0-0-0                                   3/3     Running   0             25h
   cluster-0-1-0                                   3/3     Running   1 (25h ago)   25h
   cluster-0-2-0                                   3/3     Running   1 (25h ago)   25h
   errortracking-api-7ddf8c4465-4494l              1/1     Running   0             25h
   errortracking-api-7ddf8c4465-hllx2              1/1     Running   0             25h
   errortracking-api-7ddf8c4465-lf4m8              1/1     Running   0             25h
   gatekeeper-589b599774-7p4wx                     1/1     Running   3 (25h ago)   25h
   gatekeeper-589b599774-m7mmp                     1/1     Running   3 (25h ago)   25h
   gatekeeper-589b599774-q9f77                     1/1     Running   4 (25h ago)   25h
   jaeger-operator-cc854bb65-q66lg                 1/1     Running   0             25h
   kube-state-metrics-5947c8fcdb-2xrkd             4/4     Running   0             25h
   nginx-ingress-64bf66c64c-t5dnn                  1/1     Running   0             25h
   node-exporter-6csdv                             2/2     Running   0             25h
   node-exporter-8lbbp                             2/2     Running   0             25h
   node-exporter-gh4ql                             2/2     Running   0             25h
   node-exporter-jkdh7                             2/2     Running   0             25h
   prometheus-operator-595bd7db6f-gd26x            1/1     Running   0             25h
   prometheus-prometheus-0                         2/2     Running   0             25h
   redis-operator-677bd9b876-5c5sj                 1/1     Running   0             25h
   rfr-redis-0                                     2/2     Running   0             25h
   rfr-redis-1                                     2/2     Running   0             25h
   rfr-redis-2                                     2/2     Running   0             25h
   rfs-redis-6549f5595b-79btt                      1/1     Running   0             25h
   rfs-redis-6549f5595b-88bbq                      1/1     Running   0             25h
   rfs-redis-6549f5595b-wlpxc                      1/1     Running   0             25h
   scheduler-controller-manager-6b95f65784-58w5d   1/1     Running   0             25h
   ```

### Finalize installation

After the booter finishes bootstraping the machine, a manual action is required to finish the installation.

You must visit the link printed on the line `ATTENTION: Visit ... to finalize provisioning tenant` (6).
When successful, Jaeger UI should appear.
GitLab user credentials are printed by the booter on line (4).

Automating this step is planned and tracked in [issue 1942](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1942).

GitLab credentials are also printed by the booter, in the "Ensure GDK is UP" stage's paragraph.

### Delete `devvm`

When you no longer need `devvm` you should delete it:

```shell
./devvm delete --zone <devvm-gce-zone> devvm-1
```

## Common tasks

### Update the GitLab source code

#### Create devvm with Git Remote Branch

By default, a checkout of GitLab source code is done when creating the VM image used to launch `devvm`. 

Unless specified, `master` is used, but this can be overriden by passing `-g` to the `create` command.

```
/devvm create -g my-gitlab-custom-branch
```

#### Push/Pull Git Remote Branch

If you want to change the branch of devvm that is already provisioned, without recreating one you can do as follows.

1. Create a branch on your workstation in the `gitlab` project, make some changes, and commit them.
1. Push your branch to the `gitlab` project from your workstation:

   ```shell
   git push --set-upstream origin $(git_current_branch)
   ```

1. Pull the branch in `devvm`'s GitLab Development Kit's checkout:

   ```shell
   sudo -i -u dev
   cd /home/dev/gitlab-development-kit/gitlab/
   git fetch origin <remote-branch-name>:<local-branch-name>
   ```

1. Now you could switch to the new branch (`git checkout <local-branch-name>`), but it might get messy as your GDK version might differ from devvm's and refreshing GDK on devvm does not always succeedes or can take quite long.

   ```shell
   gdk stop
   bundle install
   yarn install
   gdk start
   ```

   If you just need to test things quickly, it's usually easier to just cherry-pick the changes you want to test.  

   ```shell
   git log --oneline <local-branch-name>
   git cherry-pick <A-commit>..<B-commit>
   ```

1. Note that webpack incremental compilation is disabled by default on devvm, which could result in frontend changes to be picked up slowly. It's usually a good idea to re-enable it

```
gdk config set webpack.incremental true
gdk reconfigure
gdk restart rails-web webpack nginx
```



Any GDK commands must be executed from the `gitlab-development-kit` repository's directory.


#### Make GDK use Gitlab EE

By default, the devvm is using FOSS Gitlab.
In order to be able to launch EE version of gitlab, one needs to obtain the license:

1. Read about obtaining an EE license as described in [docs](https://handbook.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses) OR request one by filling up the form [here](https://support-super-form-gitlab-com-support-support-op-651f22e90ce6d7.gitlab.io/).
   Note that:
   * It does not make difference if you are using `Premium` or `Ultimate` license.
   * Multiple devvms can re-use the same license.
1. Move your license to a location default location `${HOME}/.gitlab_ee_license`.
   It is possible to use a custom location for your license, although this will require specifying `-l|--ee-license-path` on every run of the `devvm` CLI.
   Specifying path to different licenses can also be a way to use different license types during testing.

These steps need to be done only once during the validity period of the given license.

Launching devvm that will deploy EE Gitlab is easy: one needs to launch `./devvm create` subcommand with `-e|--enable-ee-license` option specified.
Once the booter finishes, you will be able to use GDK with your license pre-loaded.

Devvm CLI does not upload license by default as licenses are granted on per-access request and not everybody already has one.

See next section on how to enable the correct plan on your top-level group.

#### Enable the Premium or Ultimate plan on your top-level group in GitLab

GDK in Devvm runs in [SaaS mode](https://docs.gitlab.com/ee/development/ee_features.html#simulate-a-saas-instance) by default. That means we must also apply the correct plan to your top level group where you
want to access observability features.

1. Navigate to Admin area in GitLab UI
1. Navigate to groups and select the desired group
1. Click the "edit" button
1. Scroll down to "Plan", under the "Permissions and group features" section
1. Select either `Premium` or `Ultimate`
1. Scroll down and click "Save changes"
1. You can navigate out of the Admin area and to your group use `Premium` or `Ultimate` features

#### Use VSCode SSH Extension

A quick way of making changes to the GitLab source code is to use an SSH file explorer and make changes to remote files on the fly. 

VSCode supports this out of the box with the [Remote SSH Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh). 

You can follow the steps below.

1. Locate the external IP of the Devvm instance. This can be found in the `/home/dev/devvm/go/booter` output, under "Wireguard-UI services start" section.
1. Copy your SSH public key. It can be any key you own. For simplicity, here I've used the one created by `gcloud ssh`, i.e. `cat ~/.ssh/google_compute_engine.pub | pbcopy`
1. Log into devvm and add your key to the authorised list

   ```
   gcloud compute ssh <my-devvm>

   sudo -i -u dev
   mkdir ~/.ssh
   echo "<your public ssh key goes here>" > ~/.ssh/authorized_keys
   ```

1. Add an entry to the SSH config file (create it if it does not exist), in `$HOME/.ssh/config`

   ```
   Host my-devvm
      HostName <DEVVM-EXTERNAL-IP>
      UseKeychain yes
      AddKeysToAgent yes
      IdentityFile ~/.ssh/google_compute_engine #or whatever key you used
      User dev
   ```

1. Make sure you can now connect to devvm with `ssh` by running

   ```
   ssh my-devvm
   ls /home/dev/gitlab-development-kit
   ```

1. If that worked, you can now connect to devvm with VSCode, by opening the VSCode Command Palette and and select "Remote-SSH: Connect to Host..." . It will launch a new VSCode window with the SSH connection. Now, you will connect to any folder on your remote server as it was a local folder. You can check the extension [doc](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh) for more information on how to use it and customise it.

1. Now, it will be enough to make changes to files and save them, and GDK should automatically pick that up.

Note the above steps could easily be change to use any IDE or SSH File Explorer of your choice.


### Share your VM

After a VM has been deployed, to give access to it:

1. Create a new WireGuard profile, as described in [Set up WireGuard tunneling](#set-up-wireguard-tunneling).
1. Set up `/etc/hosts` entries, as described in [Log into the `devvm`](#log-into-the-devvm).

After the above is done, the new contributor has the same access as the person that created the VM.
So, they can access GDK, GOB, and other components using the same URLs.

They can also download the `kubeconfig` as described in [Set up Kubernetes configuration](#set-up-kubernetes-configuration).

### How to use already configured VM

As a reviewer, to configure devvm we need 3 things from the devvm owner.

1. IP addresses where GDK and GOB is running. We need to add those in our `/etc/host` file.
1. Wireguard configuration file.
1. Credentials for GDK.

[Here](https://www.loom.com/embed/dbd4fd6f66b54a7f8a4b2585bae05b96) is the demo video on how to use configured devvm.

### Fill devvm with sample data

#### Errors

Errors can be sent using one of the example programs in `test/sentry-sdk/testdata/supported-sdk-clients/` directory in the GOB repository.
Please follow the instructions in the example programs' comments and the GOB documentation.

#### Tracing

In order to generate traces we will use [opentelemetry demo](https://github.com/open-telemetry/opentelemetry-demo).
Please follow the steps below:
1. Launch devvm with `-m` option set:
   ```
   ./devvm -m <vm-name>
   ```
1. Visit the Tracing tab for the project in the GitLab UI once devvm boots.

Generation of traces can be stopped by scalling down the otel-collector that opentelemetry demo is using to send data:
```
dev@your-devvm:~$ cd ./opstrace/
dev@your-devvm:~/opstrace$ KUBECONFIG=/home/dev/kubeconfig kubectl scale deployment -n otel-demo-app otel-demo-otelcol --replicas 0
deployment.apps/otel-demo-otelcol scaled
```

In order to restore the generation of traces:
```
dev@your-devvm:~$ cd ./opstrace/
dev@your-devvm:~/opstrace$ KUBECONFIG=/home/dev/kubeconfig kubectl scale deployment -n otel-demo-app otel-demo-otelcol --replicas 1
deployment.apps/otel-demo-otelcol scaled
```

Purging all data can be done in a similar manner:
```
dev@your-devvm:~$ cd ./opstrace/

echo "truncate table tracing.gl_traces_main_local ON CLUSTER '{cluster}';" | KUBECONFIG=/home/dev/kubeconfig kubectl exec -ti cluster-0-0-0 -c clickhouse-server -- clickhouse-client

Unable to use a TTY - input is not a terminal or the right kind of file
cluster-0-0	9000	0		2	0
cluster-0-2	9000	0		1	0
cluster-0-1	9000	0		0	0


echo "truncate table tracing.gl_traces_rolled_local ON CLUSTER '{cluster}';" | KUBECONFIG=/home/dev/kubeconfig kubectl exec -ti cluster-0-0-0 -c clickhouse-server -- clickhouse-client

Unable to use a TTY - input is not a terminal or the right kind of file
cluster-0-2	9000	0		2	0
cluster-0-1	9000	0		1	0
cluster-0-0	9000	0		0	0
```

## Frequently asked questions

### If I want to re-create my machine, do I need to follow all the steps again?

Yes. For security reasons, the WireGuard keys and `kubeconfig` are unique for each machine.

### Can I change the region of the `devvm`?

No. This is a Google Cloud limitation.
You must destroy your current `devvm` and create a new one in the desired region.

### I fixed something in my branch or in `main` branch. When will `devvm` pick it up?

Every new `devvm` launched from the branch will pick this up.

### I need a change that landed recently in GitLab Development Kit or the GitLab project to be included in `devvm`

GDK and GitLab changes are not immediately picked up by `devvm`.
The problem is that bootstraping GDK is a very long process, so we pre-build VM images with GDK and GitLab setup to speed up `devvm` creation.
Updating GDK/GitLab requires re-baking the VM image by the CI.
[The CI pipeline](https://gitlab.com/gitlab-org/opstrace/devvm/-/pipelines) runs once a day, and can be run manually too.
Make sure that you run it on the `main` branch.

### I broke my Kubernetes cluster. What should I do?

The easiest way to fix things is to destroy the VM and create a new one.

### I want to access somebody else's VM. Do I really need to create a new WireGuard profile instead of reusing existing one?

Yes, absolutely.
Using somebody else's profile leads to IP conflicts and lots of weird bugs.
Always create a new profile for yourself when sharing a `devvm`.

### Under Chrome, I get "Connection is not private" while accessing `https://gob.devvm` or `https://gdk.devvm:3443` without option to proceed with untrusted certificate.

There are few options to workaround this:
* try opening the page in Incognito mode, maybe some extension is preventing you from accessing it
* while your window manger focues on the Chrome window with the warning page open, try typing one of the strings:
  * `badidea`
  * `thisisunsafe`
* check if Firefox/Safari has the same issue

### Something went wrong while uploading my GitLab license. What should I do?

You can upload a license file after your VM has been created by following these steps:

1. Ensure that you have a valid GitLab Ultimate license. See the [use Gitlab EE](#make-gdk-use-gitlab-ee) section on how to obtain a license.
1. Log into the remote GDK instance as admin and upload the license file at **Admin Area > Settings > General > Add License**. 
