package main

import (
	"flag"
	"log"
	"os/user"

	"github.com/go-logr/zapr"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/misc"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/systemops"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/userops"
	cr_log "sigs.k8s.io/controller-runtime/pkg/log"
)

var (
	systemMode bool
	logLevel   string
	logPretty  bool
	watch      bool
)

func main() {
	//FIXME(prozlach):  Rewrite command line args handling to spf13/cobra CLI framework.
	flag.BoolVar(&systemMode, "system-mode", false, "enable system-mode operation")
	flag.StringVar(&logLevel, "log-level", "info", "log-level")
	flag.BoolVar(&watch, "watch", false, "enable continuous printing of the status messages")
	flag.BoolVar(&logPretty, "log-pretty", false, "enables pretty-formatting of logs, be journald-friendly by default")
	flag.Parse()

	logger, syncF, err := misc.Logger(logLevel, logPretty)
	if err != nil {
		log.Fatalf("Unable to instantiate logger: %v", err)
	}
	defer syncF()

	// set client-runtime lib logger
	cr_log.SetLogger(zapr.NewLogger(logger.Desugar()))

	user, err := user.Current()
	if err != nil {
		logger.Fatalw("unable to get current user", "error", err)
	}

	if systemMode {
		if user.Username != "root" {
			log.Fatal("system-mode requires running as root user")
		}
		err = systemops.Do(logger)
	} else {
		if user.Username != "dev" && user.Username != "root" {
			log.Fatal("please run as either `dev` or `root` user")
		}
		err = userops.Do(logger, watch)
	}

	if err != nil {
		logger.Fatalf("Execution failed, error occured: %v", err)
	}
}
