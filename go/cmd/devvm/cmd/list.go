package cmd

import (
	"github.com/spf13/cobra"
	pkgcmd "gitlab.com/gitlab-org/opstrace/devvm/go/pkg/cmd"
)

func newListCmd(logLevel string) *cobra.Command {
	var zone string

	var listCmd = &cobra.Command{
		Use:     "list",
		Aliases: []string{"show", "ls"},
		Short:   "List devvms",
		Args:    cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			return pkgcmd.ListVMs(cmd.Context(), zone)
		},
	}

	listCmd.Flags().StringVarP(
		&zone, "zone", "z",
		"", "GCE zone to list devvms in, default is list from all zones")

	return listCmd
}
