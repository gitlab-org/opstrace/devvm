package cmd

import (
	pkgcmd "gitlab.com/gitlab-org/opstrace/devvm/go/pkg/cmd"
	"github.com/spf13/cobra"
)

func newDeleteCmd(logLevel string) *cobra.Command {
	var zone string

	var listCmd = &cobra.Command{
		Use:     "delete",
		Aliases: []string{"remove", "rm", "del"},
		Short:   "Delete a devvm",
		Args:    cobra.MinimumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			return pkgcmd.DeleteVM(cmd.Context(), zone, args)
		},
	}

	listCmd.Flags().StringVarP(&zone, "zone", "z", "", "GCE zone where the devvm resides")
	listCmd.MarkFlagRequired("zone")

	return listCmd
}
