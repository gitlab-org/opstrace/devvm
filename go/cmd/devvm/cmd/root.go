package cmd

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/spf13/cobra"
)

const shutdownTimeout = time.Minute

func Execute() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, os.Interrupt)

	// Signal handling code is based on
	// https://github.com/cosmos/relayer/blob/259b1278264180a2aefc2085f1b55753849c4815/cmd/root.go
	go func() {
		<-sigCh

		cancel()

		fmt.Fprintln(
			os.Stderr,
			"SIGINT received, attempting a clean shutdown."+
				"Send interrupt again to force hard shutdown.",
		)

		select {
		case <-time.After(shutdownTimeout):
			panic(fmt.Sprintf("timed after %s while waiting for clean shutdown, forcing shutdown", shutdownTimeout))
		case <-sigCh:
			panic("recived another SIGNT signal, focing shutdown")
		}
	}()

	var rootCmd = &cobra.Command{
		Use:   "devvm",
		Short: "devvm is a tool to manage devvms",
	}

	var logLevel string
	rootCmd.PersistentFlags().StringVar(
		&logLevel, "log-level", "info", "logging subsystem log level")

	rootCmd.AddCommand(newCreateCmd(logLevel))
	rootCmd.AddCommand(newListCmd(logLevel))
	rootCmd.AddCommand(newDeleteCmd(logLevel))

	if err := rootCmd.ExecuteContext(ctx); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
