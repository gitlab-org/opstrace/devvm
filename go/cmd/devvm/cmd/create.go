package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	pkgcmd "gitlab.com/gitlab-org/opstrace/devvm/go/pkg/cmd"
	"golang.org/x/term"
)

// Parts of the code were inspired/based on https://github.com/openclarity/vmclarity
const GCEProject = "opstrace-dev-bee41fca"

func newCreateCmd(logLevel string) *cobra.Command {
	var zone string
	var gobBranch string
	var gitLabBranch string
	var demoApp bool
	var customSSHKey string
	var keyPassphrase string
	var enableEELicense bool
	var eeLicensePath = ""
	var imageName string

	var createCmd = &cobra.Command{
		Use:     "create",
		Aliases: []string{"launch", "start"},
		Short:   "Launch a devvm",
		Args:    cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			if demoApp {
				// NOTE(prozlach): EE license is required if you want to see
				// the generated traces
				enableEELicense = true
			}
			return pkgcmd.CreateVM(
				cmd.Context(),
				args[0],
				zone,
				gobBranch,
				gitLabBranch,
				demoApp,
				logLevel,
				term.IsTerminal(int(os.Stdout.Fd())),
				customSSHKey,
				[]byte(keyPassphrase),
				enableEELicense,
				eeLicensePath,
				imageName,
			)
		},
	}

	createCmd.Flags().StringVarP(
		&imageName, "image-name", "i",
		"devvm-current", "devvm base image to use for launching the VM")
	createCmd.Flags().StringVarP(
		&zone, "zone", "z",
		"europe-west1-b", "GCE zone to launch VM in")
	createCmd.Flags().StringVarP(
		&gobBranch, "gob-branch", "b",
		"main", "GOB branch to use to launch VM",
	)
	createCmd.Flags().StringVarP(
		&gitLabBranch, "gitlab-branch", "g",
		"master", "GitLab branch to use to launch VM",
	)
	createCmd.Flags().BoolVarP(
		&demoApp, "demo-app", "m",
		false, "launch opentelemetry-demo on the devvm after creating",
	)
	createCmd.Flags().StringVarP(
		&customSSHKey, "ssh-key-path", "k",
		"", "ssh-key to use to launch devvm",
	)
	createCmd.MarkFlagRequired("ssh-key-passphrase")
	createCmd.Flags().StringVarP(
		&keyPassphrase, "key-passphrase", "p",
		"", "ssh-key passphrase, command will prompt for one if key is pass-protected and passphrase was not provided",
	)
	createCmd.Flags().BoolVarP(
		&enableEELicense, "enable-ee-license", "e",
		false, "enable EE Gitlab on devvm, requires providing an EE license",
	)
	userHomeDir, err := os.UserHomeDir()
	if err != nil {
		panic(fmt.Sprintf("unable to determine home dir of the user: %v", err))
	}
	createCmd.Flags().StringVarP(
		&eeLicensePath, "ee-license-path", "l",
		filepath.Join(userHomeDir, ".gitlab_ee_license"), "path to a file that contains EE license",
	)

	return createCmd
}
