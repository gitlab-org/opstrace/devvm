package main

import (
	"gitlab.com/gitlab-org/opstrace/devvm/go/cmd/devvm/cmd"
)

func main() {
	cmd.Execute()
}
