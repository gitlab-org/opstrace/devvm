package provisioning

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	mrand "math/rand"
	"net"
	"os"
	"sync"
	"time"

	"github.com/armon/go-socks5"
	"github.com/pkg/sftp"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"golang.org/x/crypto/ssh"
	"golang.org/x/term"
)

func StartSOCKSProxy(listenIP string, sshConn *ssh.Client) (net.Listener, error) {
	conf := &socks5.Config{
		Dial: func(ctx context.Context, network, addr string) (net.Conn, error) {
			return sshConn.Dial(network, addr)
		},
		Logger: log.New(os.Stdout, "", log.LstdFlags),
	}

	serverSocks, err := socks5.New(conf)
	if err != nil {
		return nil, fmt.Errorf("unable to create SOCKS5 proxy instance")
	}

	l, err := net.Listen("tcp", listenIP+":0")
	if err != nil {
		return nil, fmt.Errorf("unable to start SOCK5 proxy listener: %w", err)
	}

	go func() {
		if err := serverSocks.Serve(l); err != nil {
			// TODO(prozlach): canceling context will produce misleading error to the user
			// fmt.Printf("failed to create socks5 server: %v", err)
		}
	}()

	return l, nil
}

// port-forwarding is based upon https://gist.github.com/iamralch/5d695dcc4cc6ad5dd275

func StartPortForward(
	localIPPort, remoteHostPort string,
	sshConn *ssh.Client,
) (net.Listener, error) {
	localListener, err := net.Listen("tcp", localIPPort)
	if err != nil {
		return nil, fmt.Errorf("unable to create local port-forward listener: %w", err)
	}

	go func() {
		for {
			localConn, err := localListener.Accept()
			if err != nil {
				// no need to report on closed connections
				if !errors.Is(err, net.ErrClosed) {
					fmt.Printf("error occurred while accepting the conn: %v\n", err)
				}
				return
			}

			go func() {
				localConn := localConn
				wg := new(sync.WaitGroup)

				remoteConn, err := sshConn.Dial("tcp", remoteHostPort)
				if err != nil {
					fmt.Printf("remote dial error: %v\n", err)
					return
				}

				copyConn := func(direction string, writer, reader net.Conn) {
					defer wg.Done()

					defer writer.Close()
					defer reader.Close()

					_, err := io.Copy(writer, reader)
					if err != nil {
						fmt.Printf("io.Copy port-forward error (direction %s): %v\n", direction, err)
						return
					}
				}

				wg.Add(1)
				go copyConn("to-remote", localConn, remoteConn)
				wg.Add(1)
				go copyConn("from-remote", remoteConn, localConn)

				wg.Wait()
			}()
		}
	}()

	return localListener, nil
}

func FetchStatusData(sftpClient *sftp.Client) (*status.StatusData, error) {
	f, err := sftpClient.Open(constants.StatusFilePath)
	if err != nil {
		return nil, fmt.Errorf("unable to open booter status file: %w", err)
	}
	defer f.Close()

	res := new(status.StatusData)
	err = json.NewDecoder(f).Decode(res)
	if err != nil {
		return nil, fmt.Errorf("unable to decode status file: %w", err)
	}

	return res, nil
}

func UploadLicenseFile(sftpClient *sftp.Client, srcPath string) error {
	fDest, err := sftpClient.OpenFile(constants.EELicensePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC)
	if err != nil {
		return fmt.Errorf("unable to open license file on devvm for writing: %w", err)
	}
	defer fDest.Close()

	license, err := os.ReadFile(srcPath)
	if err != nil {
		return fmt.Errorf("unable to read local license file %s: %w", srcPath, err)
	}

	_, err = fDest.Write(license)
	if err != nil {
		return fmt.Errorf("unable to write license file to remote destination: %w", err)
	}

	return nil
}

func EstablishSSHConn(
	ctx context.Context,
	signer ssh.Signer,
	host string,
) (*ssh.Client, error) {
	// Create the Signer for this private key.
	config := &ssh.ClientConfig{
		User: "dev",
		Auth: []ssh.AuthMethod{
			// Use the PublicKeys method for remote authentication.
			ssh.PublicKeys(signer),
		},
		// TODO(prozlach): how to extract hostkey from devvm?
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// It takes some time for VM to start, hence we retry with a time limit:
	ticker := time.NewTicker(3 * time.Second)
	defer ticker.Stop()

	timeout := 60 * time.Second
	to := time.NewTimer(timeout)
	defer to.Stop()

	var lastErr error
	for {
		select {
		case <-ctx.Done():
			return nil, fmt.Errorf("context canceled")
		case <-to.C:
			return nil, fmt.Errorf("timed out after %s while waiting for devvm to boot, last err: %w", timeout.String(), lastErr)
		case <-ticker.C:
			var client *ssh.Client
			client, lastErr = ssh.Dial("tcp", fmt.Sprintf("%s:22", host), config)
			if lastErr != nil {
				continue
			}
			return client, nil
		}
	}
}

func ReadPasswordFromTerm() ([]byte, error) {
	state, err := term.MakeRaw(int(os.Stdin.Fd()))
	if err != nil {
		return nil, fmt.Errorf("unable to switch terminal to raw mode: %w", err)
	}

	fmt.Printf("Enter the passphrase: ")
	passphrase, err := term.ReadPassword(int(os.Stdin.Fd()))
	if err != nil {
		return nil, fmt.Errorf("unable to read password from stdin: %w", err)
	}

	err = term.Restore(int(os.Stdin.Fd()), state)
	fmt.Print("\n\r")
	if err != nil {
		return nil, fmt.Errorf("unable to restore terminal state: %w", err)
	}

	return passphrase, nil
}

func ReadSSHKeys(keyPath string, passphrase []byte) (ssh.Signer, string, error) {
	pemBytes, err := os.ReadFile(keyPath)
	if err != nil {
		return nil, "", fmt.Errorf("reading private key file failed %w", err)
	}

	signer, err := ssh.ParsePrivateKey(pemBytes)
	if err != nil {
		var ppErr *ssh.PassphraseMissingError
		if errors.As(err, &ppErr) {
			if len(passphrase) > 0 {
				signer, err = ssh.ParsePrivateKeyWithPassphrase(pemBytes, passphrase)
				if err != nil {
					return nil, "", fmt.Errorf("unable to parse %s: %w", keyPath, err)
				}
			} else {
				return nil, "", fmt.Errorf("unable to parse %s: %w", keyPath, err)
			}
		}
	}

	pubBytes := ssh.MarshalAuthorizedKey(signer.PublicKey())

	return signer, string(pubBytes), nil
}

// GenerateECDSAKeys generates ECDSA public and private key pair with given size for SSH.
func GenerateECDSAKeys(bitSize int) (ssh.Signer, string, error) {
	mrand.Seed(time.Now().UnixNano())

	privateKey, err := ecdsa.GenerateKey(curveFromLength(bitSize), rand.Reader)
	if err != nil {
		return nil, "", fmt.Errorf("unable to generate ssh key: %w", err)
	}

	publicKey, err := ssh.NewPublicKey(privateKey.Public())
	if err != nil {
		return nil, "", fmt.Errorf("unable to derrive public key from private key: %w", err)
	}
	pubBytes := ssh.MarshalAuthorizedKey(publicKey)

	sshSigner, err := ssh.NewSignerFromSigner(privateKey)
	if err != nil {
		return nil, "", fmt.Errorf("unable to convert generated private key to ssh.Signer: %w", err)
	}
	return sshSigner, string(pubBytes), nil
}

func curveFromLength(l int) elliptic.Curve {
	switch l {
	case 224:
		return elliptic.P224()
	case 256:
		return elliptic.P256()
	case 348:
		return elliptic.P384()
	case 521:
		return elliptic.P521()
	}
	return elliptic.P384()
}
