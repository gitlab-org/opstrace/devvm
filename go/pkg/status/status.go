package status

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"

	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"go.uber.org/zap"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

const (
	colorReset  = "\033[0m"
	colorRed    = "\033[31m"
	colorGreen  = "\033[32m"
	colorYellow = "\033[33m"
)

type StatusData struct {
	PartialStatuses []PartialStatus
	AllDone         bool
}

type Status struct {
	StatusData

	lock           sync.RWMutex
	logger         *zap.SugaredLogger
	statusFilePath string
}

type StatusStage string

const (
	StatusStageFailed     StatusStage = "failed"
	StatusStageInProgress StatusStage = "inProgress"
	StatusStageOK         StatusStage = "ok"
)

type PartialStatus struct {
	Name            string
	Succeded        StatusStage
	DetailedMessage []string
	RetriesCount    int
	RunDuration     time.Duration
	Metadata        map[string]string
}

func (sd *StatusData) StatusSummary() (bool, []string) {
	if len(sd.PartialStatuses) == 0 {
		panic("partial statuses list is empty")
	}

	if sd.AllDone {
		return true, []string{}
	}

	res := make([]string, 0)
	for _, ps := range sd.PartialStatuses {
		if ps.Succeded != StatusStageOK {
			msg := fmt.Sprintf(
				"stage: %s, status: %s, run duration: %s, retries count: %d",
				ps.Name, ps.Succeded, ps.RunDuration.String(), ps.RetriesCount,
			)
			res = append(res, msg)
		}
	}

	// New task has not started yet, all other tasks succeded. Let's wait a
	// bit.
	return false, res
}

func (sd *StatusData) RestConfig(proxyAddr string) (*restclient.Config, error) {
	for _, ps := range sd.PartialStatuses {
		if ps.Name != constants.GobK8sBaseName {
			continue
		}
		if len(ps.Metadata) == 0 {
			return nil, fmt.Errorf("Metadata for stage %s is empty", constants.GobK8sBaseName)
		}

		restConfig, err := clientcmd.RESTConfigFromKubeConfig([]byte(ps.Metadata["kubeconfig"]))
		if err != nil {
			return nil, fmt.Errorf("unable to parse metadata to rest config: %w", err)
		}

		if proxyAddr != "" {
			rawURL := fmt.Sprintf("socks5://%s", proxyAddr)
			u, err := url.Parse(rawURL)
			if err != nil {
				return nil, fmt.Errorf("unable to parse url %s: %w", rawURL, err)
			}
			restConfig.Proxy = http.ProxyURL(u)
		}

		return restConfig, nil
	}

	return nil, fmt.Errorf("Partial status for stage %s has not been found", constants.GdkStatusName)
}

func (sd *StatusData) AdminToken() (string, error) {
	for _, ps := range sd.PartialStatuses {
		if ps.Name != constants.GdkStatusName {
			continue
		}
		if len(ps.Metadata) == 0 {
			return "", fmt.Errorf("Metadata for stage %s is empty", constants.GdkStatusName)
		}
		return ps.Metadata["token"], nil
	}

	return "", fmt.Errorf("Partial status for stage %s has not been found", constants.GdkStatusName)
}

func (sd *StatusData) GDKIP() (string, error) {
	for _, ps := range sd.PartialStatuses {
		if ps.Name != constants.NetworkInfoBaseName {
			continue
		}
		if len(ps.Metadata) == 0 {
			return "", fmt.Errorf("Metadata for stage %s is empty", constants.NetworkInfoBaseName)
		}
		return ps.Metadata["gdk_ip"], nil
	}

	return "", fmt.Errorf("Partial status for stage %s has not been found", constants.NetworkInfoBaseName)
}

func (s *Status) MarkAllDone() {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.AllDone = true

	s.save()
}

func (s *Status) Upsert(ps PartialStatus) {
	s.lock.Lock()
	defer s.lock.Unlock()

	found := false
	for i := range s.PartialStatuses {
		if s.PartialStatuses[i].Name == ps.Name {
			s.PartialStatuses[i] = ps
			found = true
		}
	}
	if !found {
		s.PartialStatuses = append(s.PartialStatuses, ps)
	}

	// Keep the status up-to-date to make sure the user always knows at what
	// state we are.
	s.save()
}

func (s *Status) save() {
	// NOTE(prozlach): Not sure if we shouldn't propagate error to the caller,
	// instead of just logging them with ERROR severity. OTOH this would
	// require lots of boilerplate in the code to handle these errors.
	b, err := json.Marshal(s.StatusData)
	if err != nil {
		s.logger.Errorf("unable to marschal status: %w", err)
	}
	err = os.WriteFile(s.statusFilePath+".tmp", b, 0644)
	if err != nil {
		s.logger.Errorf("unable to write marschaled status to %s: %w", s.statusFilePath+".tmp", err)
	}
	err = os.Rename(s.statusFilePath+".tmp", s.statusFilePath)
	if err != nil {
		s.logger.Errorf("unable to move status file to its final destination: %w", err)
	}
	s.logger.Debugf("status saved successfully to %s", s.statusFilePath)
}

func (s *Status) Print(printfF func(string, ...any) (int, error)) {
	s.lock.RLock()
	defer s.lock.RUnlock()

	_, _ = printfF("Booter status:\n")
	maxLen := 0
	for _, ps := range s.PartialStatuses {
		if maxLen < len(ps.Name) {
			maxLen = len(ps.Name)
		}
	}

	allDone := true

	for _, ps := range s.PartialStatuses {
		var color string
		var txt string
		switch ps.Succeded {
		case StatusStageFailed:
			color = colorRed
			txt = "FAILED"
			allDone = false
		case StatusStageOK:
			color = colorGreen
			txt = "OK"
		case StatusStageInProgress:
			color = colorYellow
			txt = "IN PROGRESS"
			allDone = false
		}
		// TODO(prozlach): make it nicer using github.com/buger/goterm
		_, _ = printfF("%s:\t\r\x1b[%dC%s[%s]%s\n", ps.Name, maxLen+4, color, txt, colorReset)
		for _, ln := range ps.DetailedMessage {
			_, _ = printfF("\t%s\n", ln)
		}
		_, _ = printfF("\tStage retries count: %d\n", ps.RetriesCount)
		_, _ = printfF("\tStage total time: %s\n", ps.RunDuration.String())
	}

	_, _ = printfF("\n")

	if allDone {
		_, _ = printfF("All tasks were successfully completed!\n")
		return
	}

	_, _ = printfF("Devvm is still being boostrapped.\n")
	_, _ = printfF("More information on what is happening can be obtained by issuing `journalctl -fu booter.service` as root.\n")
}

func New(logger *zap.SugaredLogger, statusFilePath string) *Status {
	return &Status{
		StatusData: StatusData{
			PartialStatuses: make([]PartialStatus, 0),
			AllDone:         false,
		},
		lock:           sync.RWMutex{},
		logger:         logger,
		statusFilePath: statusFilePath,
	}
}

func (s *Status) Load() error {
	s.lock.Lock()
	defer s.lock.Unlock()

	b, err := os.ReadFile(s.statusFilePath)
	if err != nil {
		return fmt.Errorf("unable to read status file %s: %w", s.statusFilePath, err)
	}
	err = json.Unmarshal(b, &s.StatusData)
	if err != nil {
		return fmt.Errorf("unable to unmarschal status: %w", err)
	}
	return nil
}
