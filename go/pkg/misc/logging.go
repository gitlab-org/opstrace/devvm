package misc

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func Logger(logLevel string, logPretty bool) (*zap.SugaredLogger, func(), error) {
	level, err := zap.ParseAtomicLevel(logLevel)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to parse loglevel %q: %w", logLevel, err)
	}

	config := &zap.Config{
		Level:            level,
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"},
	}

	if logPretty {
		config.Encoding = "console"
		config.EncoderConfig = zapcore.EncoderConfig{
			MessageKey:  "message",
			LevelKey:    "level",
			TimeKey:     "time",
			EncodeLevel: zapcore.LowercaseColorLevelEncoder,
			EncodeTime:  zapcore.RFC3339TimeEncoder,
		}
	} else {
		config.Encoding = "json"
		config.EncoderConfig = zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "lvl",
			MessageKey:     "msg",
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.EpochTimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}
	}

	logger, err := config.Build()
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create logger: %w", err)
	}

	sync := func() {
		_ = logger.Sync()
	}

	return logger.Sugar(), sync, nil
}
