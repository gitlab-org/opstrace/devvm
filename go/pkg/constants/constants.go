package constants

const (
	GCEProject = "opstrace-dev-bee41fca"

	DefaultApppasswordCacheFilePath = "/home/dev/.booter-apppassword_cache.json"
	DefaultLabelsCacheFilePath      = "/home/dev/.booter-vmlabels_cache.json"
	DefaultNetworkInfoFilePath      = "/home/dev/.booter-network_info.json"
	StatusFilePath                  = "/home/dev/.booter-status.json"

	GDKPasswordFilePath  = "/home/dev/.booter-gdk_password.json"
	GDKRepositoryPath    = "/home/dev/gitlab-development-kit/"
	GitLabRepositoryPath = GDKRepositoryPath + "gitlab/"
	GDKRootUser          = "root"

	GOBOauthSecretName          = "oauth-credentials"
	GOBTenantOverrideSecretName = "local-tenant-data"
	GOBRepositoryPath           = "/home/dev/opstrace/"
	GOBSchedulerNamespace       = "default"
	GOBCACertSecretName         = "self-signed-ca-secret"
	GOBCACertPath               = "/usr/local/share/ca-certificates/k8s-CA.crt"

	ClickhouseConfPath            = "/etc/clickhouse-server/config.d/devvm_conf.xml"
	ClickhouseDefaultUserConfPath = "/etc/clickhouse-server/users.d/default-password.xml"

	KindClusterName   = "opstrace"
	KindK8sConfigPath = "/home/dev/kubeconfig"

	WireguardUIConfPath = "/etc/wireguard-ui/wireguard-ui.conf"

	EELicensePath = "/home/dev/.gitlab-license"

	FieldManagerIDString = "booter"

	//Status names:
	NetworkInfoBaseName   = "Reading Network addresation information"
	WireguardVPNStageName = "Wireguard VPN"
	VMlabelsStatusName    = "GOB & GitLab branch name and commit ID resolution/verification"
	ClickhouseStatusName  = "Clickhouse DB"
	GobprepStatusName     = "GOB repo preparation"
	GobK8sBaseName        = "Ensure Kind cluster is running"
	GobClusterBaseName    = "Ensure GOB cluster is deployed"
	AuxManifestsBaseName  = "Ensure auxilary k8s manifests are deployed"
	GobTenantBaseName     = "Ensure GOB tenant"
	DemoManifestsBaseName = "Ensure otel-telemetry demo manifests are deployed"
	GdkOAuthBaseName      = "Ensure GDK OAuth credentials and GOB OAuth data"
	GdkStatusName         = "Ensure GDK is UP"
)
