package cmd

import (
	"context"
	"fmt"
	"strings"
	"time"

	compute "cloud.google.com/go/compute/apiv1"
	"cloud.google.com/go/compute/apiv1/computepb"
	"github.com/googleapis/gax-go/v2"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/provisioning"
	"google.golang.org/api/iterator"
	"google.golang.org/protobuf/proto"
)

func getVMInfoString(instance *computepb.Instance) (string, error) {
	ip, err := provisioning.ExtractPublicIPFromVMInfo(instance)
	if err != nil {
		return "", fmt.Errorf("unable to determine the public IP of the instance: %w", err)
	}

	var uptime string
	startTime, err := time.Parse("2006-01-02T15:04:05.999-07:00", instance.GetLastStartTimestamp())
	if err != nil {
		uptime = fmt.Sprintf("invalid start timestamp: %s", err)
	} else {
		uptime = time.Since(startTime).String()
	}

	return fmt.Sprintf("%s %s %s %s", instance.GetName(), instance.GetStatus(), ip, uptime), nil
}

func ListVMs(ctx context.Context, zone string) error {
	instancesClient, err := compute.NewInstancesRESTClient(ctx)
	if err != nil {
		return fmt.Errorf("failed to create instance client: %w", err)
	}

	//TODO(prozlach): DRY this code, quick version for now
	if zone != "" {
		it := instancesClient.List(
			ctx,
			&computepb.ListInstancesRequest{
				Project: constants.GCEProject,
				Zone:    zone,
				Filter:  proto.String("labels.devvm:true"),
			},
			gax.WithRetry(
				func() gax.Retryer {
					return gax.OnErrorFunc(
						gax.Backoff{
							Initial:    1 * time.Second,
							Max:        32 * time.Second,
							Multiplier: 2,
						},
						// Wait ~1 minutes max in total
						// 1, 2, 4, 8, 16, 32
						// 63
						provisioning.ShouldRetryFunc(5, true),
					)
				},
			),
		)
		for {
			instance, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return fmt.Errorf("iterator returned error: %w", err)
			}

			infoString, err := getVMInfoString(instance)
			if err != nil {
				return fmt.Errorf("unable to obtain instance info text: %w", err)
			}

			fmt.Printf("\t- %s\n", infoString)
		}
	} else {
		it := instancesClient.AggregatedList(ctx, &computepb.AggregatedListInstancesRequest{
			Project: constants.GCEProject,
			Filter:  proto.String("labels.devvm:true"),
		})
		for {
			pair, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return fmt.Errorf("iterator returned error: %w", err)
			}
			instances := pair.Value.Instances
			if len(instances) > 0 {
				fmt.Printf("%s\n", strings.Split(pair.Key, "/")[1])
				for _, instance := range instances {
					infoString, err := getVMInfoString(instance)
					if err != nil {
						return fmt.Errorf("unable to obtain instance info text: %w", err)
					}

					fmt.Printf("\t- %s\n", infoString)
				}
			}
		}

	}

	return nil
}
