package cmd

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	compute "cloud.google.com/go/compute/apiv1"
	"github.com/go-logr/zapr"
	"github.com/pkg/sftp"
	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/misc"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/provisioning"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"golang.org/x/crypto/ssh"
	"golang.org/x/net/proxy"
	v1 "k8s.io/api/apps/v1"
	"k8s.io/kubectl/pkg/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	cr_log "sigs.k8s.io/controller-runtime/pkg/log"
)

func CreateVM(
	ctx context.Context,
	instanceName string,
	zone string,
	gobBranch string,
	gitLabBranch string,
	demoApp bool,
	logLevel string,
	logPretty bool,
	customSSHKey string,
	keyPassphrase []byte,
	enableEELicense bool,
	eeLicensePath string,
	imageName string,
) error {
	logger, syncF, err := misc.Logger(logLevel, logPretty)
	if err != nil {
		log.Fatalf("Unable to instantiate logger: %v", err)
	}
	defer syncF()

	// set client-runtime lib logger
	cr_log.SetLogger(zapr.NewLogger(logger.Desugar()))

	region, err := provisioning.GetGCERegion(zone)
	if err != nil {
		return fmt.Errorf("unable to determine region basing on gce zone: %w", err)
	}

	var signer ssh.Signer
	var pubKey string
	if customSSHKey == "" {
		signer, pubKey, err = provisioning.GenerateECDSAKeys(256)
		if err != nil {
			return fmt.Errorf("unable to generate ssh key: %w", err)
		}
		logger.Infow("ephemeral SSH key created")
	} else {
		signer, pubKey, err = provisioning.ReadSSHKeys(customSSHKey, []byte(keyPassphrase))
		if err != nil {
			var ppErr *ssh.PassphraseMissingError
			if errors.As(err, &ppErr) {
				if len(keyPassphrase) == 0 {
					keyPassphrase, err = provisioning.ReadPasswordFromTerm()
					if err != nil {
						return fmt.Errorf("unable to read passphrase from stdin: %w", err)
					}
				}

				signer, pubKey, err = provisioning.ReadSSHKeys(customSSHKey, []byte(keyPassphrase))
				if err != nil {
					return fmt.Errorf("unable to read ssh key from %s: %w", customSSHKey, err)
				}
			} else {
				return fmt.Errorf("unable to read ssh key from %s: %w", customSSHKey, err)
			}
		}
		logger.Infof("SSH key read from %s", customSSHKey)
	}

	instancesClient, err := compute.NewInstancesRESTClient(ctx)
	if err != nil {
		return fmt.Errorf("failed to create instance client: %w", err)
	}

	created, err := provisioning.EnsureVM(
		ctx, instanceName, constants.GCEProject, zone, pubKey,
		region, gobBranch, gitLabBranch, demoApp, instancesClient,
		imageName,
	)
	if err != nil {
		return fmt.Errorf("ensuring VM is running failed: %w", err)
	}
	logger.Infow("Instance ensured", "created", created)

	instanceInfo, _, err := provisioning.GetVMInfo(ctx, instanceName, constants.GCEProject, zone, instancesClient)
	if err != nil {
		return fmt.Errorf(
			"unable to fetch information about instance %s: %w", instanceName, err,
		)
	}

	publicIP, err := provisioning.ExtractPublicIPFromVMInfo(instanceInfo)
	if err != nil {
		return fmt.Errorf(
			"unable to determine public IP of the instance %s: %w", instanceName, err,
		)
	}

	logger.Infof("Devvm public IP: %s", publicIP)

	sshConn, err := provisioning.EstablishSSHConn(ctx, signer, publicIP)
	if err != nil {
		return fmt.Errorf("unable to connect via SSH: %w", err)
	}
	defer sshConn.Close()

	// open an SFTP session over an existing ssh connection.
	sftpClient, err := sftp.NewClient(sshConn)
	if err != nil {
		return fmt.Errorf("unable to establish sftp client: %w", err)
	}
	defer sftpClient.Close()

	if enableEELicense {
		if err := provisioning.UploadLicenseFile(sftpClient, eeLicensePath); err != nil {
			return fmt.Errorf("unable to upload license file: %w", err)
		}
		logger.Info("License file has been uploaded")
	}

	// It takes some time for booter to configure all operations, hence we
	// retry with a time limit:
	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	timeout := 25 * time.Minute
	to := time.NewTimer(timeout)
	defer to.Stop()

	var sd *status.StatusData
	var lastMsgs []string
LOOP:
	for {
		select {
		case <-ctx.Done():
			return errors.New("context canceled while waiting for booter to set up VM")
		case <-to.C:
			return fmt.Errorf("timed out after %s while waiting for booter to set up devvm", timeout.String())
		case <-ticker.C:
			sd, err = provisioning.FetchStatusData(sftpClient)
			if err != nil {
				if errors.Is(err, os.ErrNotExist) {
					logger.Infof("Booter has not started yet, status files does not exist")
					continue
				}
				return fmt.Errorf("unable to fetch status data from devvm: %w", err)
			}
			allDone, msgs := sd.StatusSummary()
			if allDone {
				break LOOP
			}
			if !misc.StringSlicesEqual(lastMsgs, msgs) {
				logger.Info("Booter is executing:")
				for _, msg := range msgs {
					logger.Infof(" - %s", msg)
				}
				lastMsgs = msgs
			}
		}
	}

	logger.Info("Booter finished initialization of the devvm")

	token, err := sd.AdminToken()
	if err != nil {
		return fmt.Errorf("unable to find token data: %w", err)
	}
	logger.Infof("Admin token: %s", token)

	listener, err := provisioning.StartSOCKSProxy("127.0.0.1", sshConn)
	if err != nil {
		return fmt.Errorf("failed to start SOCKS5 proxy server: %w", err)
	}
	defer listener.Close()

	restConfig, err := sd.RestConfig(listener.Addr().String())
	if err != nil {
		return fmt.Errorf("unable to determine k8s rest config: %w", err)
	}

	k8sClient, err := client.New(restConfig, client.Options{
		Scheme: scheme.Scheme,
	})
	if err != nil {
		return fmt.Errorf("unable to determine k8s rest config: %w", err)
	}

	deployments := &v1.DeploymentList{}
	err = k8sClient.List(ctx, deployments, client.InNamespace("default"))
	if err != nil {
		return fmt.Errorf("unable to list deployments in default NS: %w", err)
	}
	if len(deployments.Items) == 0 {
		return errors.New("deployments list in default NS is empty")
	}
	found := false
	for _, d := range deployments.Items {
		if d.Name == "scheduler-controller-manager" {
			found = true
		}
	}
	if !found {
		return errors.New("scheduler-controller-manager deployment is not present")
	}
	logger.Info("k8s client through SOCKs proxy is working")

	dialSocksProxy, err := proxy.SOCKS5(
		"tcp", listener.Addr().String(), nil, proxy.Direct,
	)
	if err != nil {
		return fmt.Errorf("unable to create socks5 proxy dialer: %w", err)
	}

	httpClient := &http.Client{
		Timeout: 60 * time.Second,
		Transport: &http.Transport{
			Dial: dialSocksProxy.Dial,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true, //nolint:gosec
			},
		},
	}

	glClient, err := gogitlab.NewClient(
		token,
		gogitlab.WithBaseURL("https://gdk.devvm:3443"),
		gogitlab.WithHTTPClient(httpClient),
	)
	if err != nil {
		return fmt.Errorf("unable to create go-gitlab Http client: %w", err)
	}

	req, err := glClient.NewRequest(
		http.MethodGet, "personal_access_tokens/self", nil,
		[]gogitlab.RequestOptionFunc{
			gogitlab.WithContext(ctx),
		},
	)
	if err != nil {
		return fmt.Errorf("unable to create API token information req: %w", err)
	}

	tokenInfo := &struct {
		Id     int
		Name   string
		Scopes []string
	}{}
	resp, err := glClient.Do(req, tokenInfo)
	if err != nil {
		return fmt.Errorf("go-gitlab request failed: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK || tokenInfo.Scopes[0] != "api" {
		return fmt.Errorf("http request through SOCKs proxy failed (%s, %s)", resp.Status, strings.Join(tokenInfo.Scopes, ","))
	}
	logger.Info("http client through SOCKs proxy is working")

	logger.Info("All done!")
	return nil
}
