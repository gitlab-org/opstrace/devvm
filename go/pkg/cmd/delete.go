package cmd

import (
	"context"
	"fmt"
	"time"

	compute "cloud.google.com/go/compute/apiv1"
	"cloud.google.com/go/compute/apiv1/computepb"
	"github.com/googleapis/gax-go/v2"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/provisioning"
)

func DeleteVM(ctx context.Context, zone string, instancesNames []string) error {
	instancesClient, err := compute.NewInstancesRESTClient(ctx)
	if err != nil {
		return fmt.Errorf("failed to create instance client: %w", err)
	}

	for _, instanceName := range instancesNames {
		op, err := instancesClient.Delete(
			ctx,
			&computepb.DeleteInstanceRequest{
				Project:  constants.GCEProject,
				Zone:     zone,
				Instance: instanceName,
			},
			gax.WithRetry(
				func() gax.Retryer {
					return gax.OnErrorFunc(
						gax.Backoff{
							Initial:    1 * time.Second,
							Max:        32 * time.Second,
							Multiplier: 2,
						},
						// Wait ~1 minutes max in total
						// 1, 2, 4, 8, 16, 32
						// 63
						provisioning.ShouldRetryFunc(5, true),
					)
				},
			),
		)
		if err != nil {
			return fmt.Errorf("unable to delete VM %s: %w", instanceName, err)
		}
		//TODO(prozlach) - add a pretty spinner
		fmt.Printf("Deleting instance %s...", instanceName)
		if err = op.Wait(ctx); err != nil {
			return fmt.Errorf("unable to wait for the operation: %w", err)
		}
		fmt.Printf(" - OK!\n")

	}

	return nil
}
