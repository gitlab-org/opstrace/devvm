package systemops

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/moby/moby/pkg/namesgenerator"
	"github.com/sethvargo/go-password/password"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
)

const wireguardUIUsernamePlaceholder = "USERNAME_PLACEHOLDER"
const wireguardUIPasswordPlaceholder = "PASSWORD_PLACEHOLDER"

func ensureWireguardUI(logger *zap.SugaredLogger) status.PartialStatus {
	user, pass, err := ensureWireguardUICreds(logger)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf("unable to ensure wireguard-ui credentials: %v", err),
			},
		}
	}
	err = ensureWireguardUIServices(logger)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf("unable to ensure wireguard-ui service: %v", err),
			},
		}
	}

	publicIP, err := getPublicIP(logger)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf("unable to determine instane's public IP: %v", err),
			},
		}
	}

	logger.Info("wireguard-ui has been set-up")
	return status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			fmt.Sprintf("ui is now reachable at: https://%s:5000/", publicIP),
			fmt.Sprintf("username: %s", user),
			fmt.Sprintf("password: %s", pass),
		},
	}
}

func getPublicIP(
	logger *zap.SugaredLogger,
) (string, error) {
	gceHeaders := map[string]string{
		"Metadata-Flavor": "Google",
	}

	// TODO(prozlach): make it cmdline param
	// Value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	logger.Debug("fetching instance's public IP")
	url := "http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip"
	resp, status, err := httpGetReq(ctx, logger, url, gceHeaders, false)
	if err != nil {
		return "", fmt.Errorf("http GET request failed: %w", err)
	}
	if status != http.StatusOK {
		return "", fmt.Errorf("http GET request returned status %d", status)
	}
	return string(resp), nil
}

func ensureWireguardUIServices(logger *zap.SugaredLogger) error {
	for _, svc := range []string{"wgui.service", "wgui.path", "wireguard-ui.service"} {
		enabled, err := isServiceEnabled(logger, svc)
		if err != nil {
			return err
		}
		if !enabled {
			logger.Infof("enabling service %q", svc)
			err = enableService(logger, svc)
			if err != nil {
				return err
			}
		}
		isRunning, err := isServiceRunning(logger, svc)
		if err != nil {
			return err
		}
		if !isRunning {
			logger.Infof("starting service %q", svc)
			err = startService(logger, svc)
			if err != nil {
				return err
			}
		}

		// NOTE(prozlach): shortly after we start wireguard-ui, we need to
		// restart it again, as there is a weird issue with re-creating the
		// contents of the state directory (i.e. `/db`) which in turn leads to
		// redirection loops in the login screen. This is just a workaround,
		// the bug is most likelly in wireguard ui itself.
		//
		// Upstream issue: https://github.com/ngoduykhanh/wireguard-ui/issues/530

		if svc == "wireguard-ui.service" {
			// Give wireguard-ui some time to create directories and stabilize.
			// Wait time was chosen arbitrarly.
			time.Sleep(3 * time.Second)
			logger.Infof("restarting service %q", svc)
			err = restartService(logger, svc)
			if err != nil {
				return err
			}
		}
	}

	logger.Debug("wireguard-ui services have been ensured")
	return nil
}

func ensureWireguardUICreds(logger *zap.SugaredLogger) (string, string, error) {
	confBytes, err := os.ReadFile(constants.WireguardUIConfPath)
	if err != nil {
		return "", "", fmt.Errorf("unable to read conf file %s: %w", constants.WireguardUIConfPath, err)
	}
	conf := string(confBytes)

	configUpdated := false

	var name string
	re := regexp.MustCompile(`WGUI_USERNAME=(\w+)`)
	submatches := re.FindStringSubmatch(conf)
	if submatches == nil || len(submatches) != 2 {
		err = fmt.Errorf("wireguard-ui config %q file is malformed, username submatches: %v", constants.WireguardUIConfPath, submatches)
		return "", "", err
	}
	if submatches[1] == wireguardUIUsernamePlaceholder {
		rand.Seed(time.Now().UnixNano())
		name = namesgenerator.GetRandomName(0)
		conf = strings.Replace(conf, wireguardUIUsernamePlaceholder, name, 1)
		configUpdated = true
		logger.Info("wireguard-ui username has been updated")
	} else {
		name = submatches[1]
	}

	var pass string
	re = regexp.MustCompile(`WGUI_PASSWORD=(\w+)`)
	submatches = re.FindStringSubmatch(conf)
	if submatches == nil || len(submatches) != 2 {
		err = fmt.Errorf("wireguard-ui config %q file is malformed, password submatches: %v", constants.WireguardUIConfPath, submatches)
		return "", "", err
	}
	if submatches[1] == wireguardUIPasswordPlaceholder {
		pass, err = password.Generate(32, 10, 0, false, true)
		if err != nil {
			return "", "", fmt.Errorf("unable to generate password: %w", err)
		}
		conf = strings.Replace(conf, wireguardUIPasswordPlaceholder, pass, 1)
		configUpdated = true
		logger.Info("wireguard-ui password has been updated")
	} else {
		pass = submatches[1]
	}

	if configUpdated {
		err = os.WriteFile(constants.WireguardUIConfPath, []byte(conf), 0640)
		if err != nil {
			return "", "", fmt.Errorf("unable to write conf file %s: %w", constants.WireguardUIConfPath, err)
		}
	}

	logger.Debug("wireguard-ui credentials have been ensured")
	return name, pass, nil
}

func isServiceEnabled(logger *zap.SugaredLogger, name string) (bool, error) {
	out, _, err := executeCmd(logger, "", "systemctl", "is-enabled", name)
	if err != nil {
		return false, fmt.Errorf("unable to check if service %q is running: %w", name, err)
	}
	return strings.TrimSpace(out) == "enabled", nil
}

func enableService(logger *zap.SugaredLogger, name string) error {
	out, exitCode, err := executeCmd(logger, "", "systemctl", "enable", name)
	if err != nil {
		return fmt.Errorf("unable to enable service %q: %w", name, err)
	}
	if exitCode != 0 {
		return fmt.Errorf("`systemctl enabled %q` returned non-zero exit code: %d, output: %s", name, exitCode, out)
	}
	return nil
}

func isServiceRunning(logger *zap.SugaredLogger, name string) (bool, error) {
	out, _, err := executeCmd(logger, "", "systemctl", "is-active", name)
	if err != nil {
		return false, fmt.Errorf("unable to check if service %q is running: %w", name, err)
	}
	return strings.TrimSpace(out) == "active", nil
}

func startService(logger *zap.SugaredLogger, name string) error {
	out, exitCode, err := executeCmd(logger, "", "systemctl", "start", name)
	if err != nil {
		return fmt.Errorf("unable to start service %q: %w", name, err)
	}
	if exitCode != 0 {
		return fmt.Errorf("`systemctl start %q` returned non-zero exit code: %d, output: %s", name, exitCode, out)
	}
	return nil
}

func restartService(logger *zap.SugaredLogger, name string) error {
	out, exitCode, err := executeCmd(logger, "", "systemctl", "restart", name)
	if err != nil {
		return fmt.Errorf("unable to restart service %q: %w", name, err)
	}
	if exitCode != 0 {
		return fmt.Errorf("`systemctl restart %q` returned non-zero exit code: %d, output: %s", name, exitCode, out)
	}
	return nil
}
