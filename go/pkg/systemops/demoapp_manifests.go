package systemops

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/helm"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
)

const (
	DemoManifestsBaseName = "Ensure otel-telemetry demo manifests are deployed"
)

const demoAppValuesYamlFmt = `opentelemetry-collector:
  config:
    processors:
      attributes:
        actions:
          - action: insert
            key: gitlab.target_project_id
            value: "%d"
          - action: insert
            key: gitlab.target_namespace_id
            value: "%d"
    exporters:
      otlphttp/example:
        endpoint: "http://otel-collector.tenant-%d.svc.cluster.local:4318/"
    service:
      pipelines:
        traces:
          receivers: [otlp]
          processors: [attributes,batch]
          exporters: [otlphttp/example]
        metrics:
          receivers: [otlp]
          processors: [attributes,batch]
          exporters: [otlphttp/example]
        logs:
          receivers: [otlp]
          processors: [attributes,batch]
          exporters: [otlphttp/example]
  enabled: true
  resources:
    limits:
      memory: 400Mi`

const otelDemoCHartVersion = "0.30.5"

func ensureDemoManifests(logger *zap.SugaredLogger, groupID int, projectID int) status.PartialStatus {
	// helm repo add open-telemetry https://open-telemetry.github.io/opentelemetry-helm-charts
	// helm repo update
	// helm upgrade  my-otel-demo open-telemetry/opentelemetry-demo --install  --force -f ~/values-ot_demo.yaml
	otelDemoHelmChart := helm.Helm{
		Repo:             "https://open-telemetry.github.io/opentelemetry-helm-charts",
		ReleaseName:      "otel-demo",
		ReleaseNamespace: "otel-demo-app",
		ChartName:        "opentelemetry-demo",

		Logger: logger,
	}
	valuesData := fmt.Sprintf(demoAppValuesYamlFmt, projectID, groupID, groupID)

	logger.Infof("rendered otel-demo values: %s", valuesData)

	release, err := otelDemoHelmChart.Install(valuesData, otelDemoCHartVersion)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to deploy otel-demo via Helm: %v", err,
				),
			},
		}
	}

	return status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			fmt.Sprintf("Release status: %s", release.Info.Status),
			fmt.Sprintf("Namespace: %s", release.Namespace),
		},
	}
}
