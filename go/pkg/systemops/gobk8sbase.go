package systemops

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"time"

	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
	v1 "k8s.io/api/apps/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	k8s_scheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	clientcmdv1 "k8s.io/client-go/tools/clientcmd/api/v1"
	cr_client "sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"
)

type kindClusterState string

const (
	clusterRunning kindClusterState = "running"
	clusterBroken  kindClusterState = "broken"
	clusterAbsent  kindClusterState = "absent"
	clusterUnknown kindClusterState = "unknown"

	controlPlaneNodeName            = "opstrace-control-plane"
	dockerNetworkName               = "wghub"
	schedulerDeploymentName         = "scheduler-controller-manager"
	schedulerDeploymentReadyTimeout = 4 * time.Minute
)

func ensureGOBK8sBase(logger *zap.SugaredLogger) (cr_client.Client, status.PartialStatus) {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"failed to create docker client: %v", err,
				),
			},
		}
	}

	kindState, err := checkKindRunning(logger, cli)
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"checking Kind cluster status failed: %v", err,
				),
			},
		}
	}

	switch kindState {
	case clusterBroken:
		err = destroyKindCluster(logger)
		if err != nil {
			return nil, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"destroying Kind cluster failed: %v", err,
					),
				},
			}
		}
		fallthrough
	case clusterAbsent:
		err = startKindCluster(logger)
		if err != nil {
			return nil, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"starting Kind cluster failed: %v", err,
					),
				},
			}
		}
	}

	restConfig, kubeconfigBytes, err := ensureKindK8sConfig(logger, cli)
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure k8s kubeconfig file: %v", err,
				),
			},
		}
	}

	scheme := runtime.NewScheme()
	utilruntime.Must(k8s_scheme.AddToScheme(scheme))
	k8sClient, err := cr_client.New(restConfig, cr_client.Options{Scheme: scheme})
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to instantiate k8s client: %v", err,
				),
			},
		}
	}

	schedulerUp, err := checkSchedulerRunning(logger, k8sClient)
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"failed to check scheduler status: %v", err,
				),
			},
		}
	}
	if !schedulerUp {
		err := deployScheduler(logger)
		if err != nil {
			return nil, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"failed to deploy scheduler: %v", err,
					),
				},
			}
		}
	}

	err = waitDeploymentReady(
		logger, k8sClient,
		constants.GOBSchedulerNamespace, schedulerDeploymentName,
		2*time.Second, schedulerDeploymentReadyTimeout,
	)
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"scheduler deployment did not get ready in time: %v", err,
				),
			},
		}
	}

	return k8sClient, status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			"kubectl is available at /home/dev/kubeconfig",
			"try copying it to your home dir and using:",
			"\tgcloud compute scp --zone <devvm-zone> <devvm-name>:~/kubeconfig ~/kubeconfig",
		},
		Metadata: map[string]string{
			"kubeconfig": string(kubeconfigBytes),
		},
	}
}

func checkKindRunning(logger *zap.SugaredLogger, cli *client.Client) (kindClusterState, error) {
	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	filterArgs := filters.NewArgs()
	filterArgs.Add("name", "opstrace") // The name filter matches also part of a container’s name
	filterArgs.Add("status", "running")
	containers, err := cli.ContainerList(
		ctx,
		container.ListOptions{
			Filters: filterArgs,
		},
	)
	if err != nil {
		return clusterUnknown, fmt.Errorf("unable to list containers using docker API client: %w", err)
	}

	allContainers := len(containers)
	logger.Debugw(
		"GOB Kind cluster",
		"allContainers", allContainers,
	)
	if allContainers == 0 {
		return clusterAbsent, nil
	}
	if allContainers < 4 {
		return clusterBroken, nil
	}
	return clusterRunning, nil
}

func startKindCluster(logger *zap.SugaredLogger) error {
	msg := fmt.Sprintf("deploying Kind cluster %s", constants.KindClusterName)
	_, err := executeCmdWithAsdf(
		logger,
		constants.GOBRepositoryPath,
		fmt.Sprintf("KIND_EXPERIMENTAL_DOCKER_NETWORK=%s make kind kind-load-docker-images", dockerNetworkName),
		msg,
	)
	return err
}

func destroyKindCluster(logger *zap.SugaredLogger) error {
	msg := fmt.Sprintf("destroying Kind cluster %s", constants.KindClusterName)
	_, err := executeCmdWithAsdf(logger, constants.GOBRepositoryPath, "make destroy", msg)
	if err != nil {
		return err
	}

	err = os.Remove(constants.KindK8sConfigPath)
	if err != nil {
		if !errors.Is(err, fs.ErrNotExist) {
			return fmt.Errorf("unable to unlink stale kubeconfig file %s: %w", constants.KindK8sConfigPath, err)
		}
	}

	return nil
}

func ensureKindK8sConfig(
	logger *zap.SugaredLogger,
	cli *client.Client,
) (
	*rest.Config, []byte, error,
) {
	// * move to dest location
	cmd := fmt.Sprintf("kind get kubeconfig --name %s", constants.KindClusterName)
	msg := "fetching kubeconfig of the kind cluster"
	// asdf is not really needed here, just trying to reuse the helpers we have
	// instead of creating new ones.
	out, err := executeCmdWithAsdf(logger, constants.GOBRepositoryPath, cmd, msg)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to fetch kubeconfig from kind: %w", err)
	}

	restConfig, err := clientcmd.RESTConfigFromKubeConfig([]byte(out))
	if err != nil {
		return nil, nil, fmt.Errorf("unable to parse kubeconfig returned by kind: %w", err)
	}

	controllNodeIP, err := getControllNodeIP(logger, cli)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to determine control node's ip: %w", err)
	}

	// We need to adjust the IP in the kubeconfig used by the user, as they
	// need to connect to the controll node directly as they come through VPN
	// hence localhost entry will not work.
	hostname, err := os.Hostname()
	if err != nil {
		return nil, nil, fmt.Errorf("unable to determine node's hostname: %w", err)
	}
	userRestConfig := rest.CopyConfig(restConfig)
	userRestConfig.Host = fmt.Sprintf("https://%s:6443", controllNodeIP)
	userRestConfigBytes, err := newKubeConfigForRESTConfig(
		userRestConfig,
		fmt.Sprintf("devvm-%s", hostname),
		"default",
	)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to serialize restConfig: %w", err)
	}

	err = os.WriteFile(constants.KindK8sConfigPath, userRestConfigBytes, 0666)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to write serialized kubeconfig: %w", err)
	}

	return restConfig, userRestConfigBytes, nil
}

func getControllNodeIP(logger *zap.SugaredLogger, cli *client.Client) (string, error) {
	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	container, err := cli.ContainerInspect(ctx, controlPlaneNodeName)
	if err != nil {
		return "", fmt.Errorf("failed to get information about controll plane container: %w", err)
	}

	// TODO(prozlach): add verification of fields and handling nulls.
	// YOLO! for now.
	return container.NetworkSettings.Networks[dockerNetworkName].IPAddress, nil
}

// NewKubeConfigForRESTConfig returns a kubeConfig bytes for the given REST Config.
func newKubeConfigForRESTConfig(config *rest.Config, clusterName, namespace string) ([]byte, error) {
	kubeConfig := &clientcmdv1.Config{
		APIVersion: "v1",
		Kind:       "Config",
		Clusters: []clientcmdv1.NamedCluster{
			{
				Name: clusterName,
				Cluster: clientcmdv1.Cluster{
					Server:                   config.Host,
					InsecureSkipTLSVerify:    config.Insecure,
					CertificateAuthorityData: config.TLSClientConfig.CAData,
				},
			},
		},
		AuthInfos: []clientcmdv1.NamedAuthInfo{
			{
				Name: "auth",
				AuthInfo: clientcmdv1.AuthInfo{
					ClientKeyData:         config.TLSClientConfig.KeyData,
					ClientCertificateData: config.TLSClientConfig.CertData,
				},
			},
		},
		Contexts: []clientcmdv1.NamedContext{
			{
				Name: clusterName,
				Context: clientcmdv1.Context{
					Cluster:   clusterName,
					Namespace: namespace,
					AuthInfo:  "auth",
				},
			},
		},
		CurrentContext: clusterName,
	}

	bytes, err := yaml.Marshal(kubeConfig)
	if err != nil {
		return nil, fmt.Errorf("unable to marschal kubeconfig: %w", err)
	}
	return bytes, nil
}

func checkSchedulerRunning(logger *zap.SugaredLogger, k8sClient cr_client.Client) (bool, error) {
	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	key := cr_client.ObjectKey{
		Name:      schedulerDeploymentName,
		Namespace: constants.GOBSchedulerNamespace,
	}
	tmp := new(v1.Deployment)

	err := k8sClient.Get(ctx, key, tmp)
	if err != nil {
		if kerrors.IsNotFound(err) {
			return false, nil
		}
		return false, fmt.Errorf("unable to fetch scheduler deployment: %w", err)
	}

	return true, nil
}

func deployScheduler(logger *zap.SugaredLogger) error {
	msg := fmt.Sprintf("deploying Kind cluster %s", constants.KindClusterName)
	_, err := executeCmdWithAsdf(logger, constants.GOBRepositoryPath, "make deploy", msg)
	return err
}
