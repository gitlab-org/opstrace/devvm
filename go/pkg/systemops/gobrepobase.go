package systemops

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
)

func prepareGOBRepo(logger *zap.SugaredLogger, commitID string) status.PartialStatus {
	err := checkoutRepo(logger, commitID, constants.GOBRepositoryPath)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"checkint out GOB repo failed: %v", err,
				),
			},
		}
	}

	err = ensureASDFTooling(logger)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"installation of ASDF-managed tooling failed: %v", err,
				),
			},
		}
	}

	err = ensureDockerContainers(logger)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"ensuring all GOB containers are available locally failed: %v", err,
				),
			},
		}
	}

	return status.PartialStatus{
		Succeded: status.StatusStageOK,
	}
}

func ensureASDFTooling(logger *zap.SugaredLogger) error {
	msg := "installing ASDF-manager tooling"
	_, err := executeCmdWithAsdf(logger, constants.GOBRepositoryPath, "make bootstrap", msg)
	return err
}

func ensureDockerContainers(logger *zap.SugaredLogger) error {
	msg := "ensuring GOB docker containers are available locally"
	_, err := executeCmdWithAsdf(logger, constants.GOBRepositoryPath, "make docker-ensure", msg)
	return err
}
