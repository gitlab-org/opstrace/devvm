In order to update Metallb, simply do:

export VERSION=v0.13.7

curl -q \
    https://raw.githubusercontent.com/metallb/metallb/${VERSION}/config/manifests/metallb-native.yaml \
    -o 10-metallb.yaml
