package systemops

import (
	"fmt"
	"math/rand"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/moby/moby/pkg/namesgenerator"
	"github.com/sethvargo/go-password/password"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
)

const clickhouseReplicationSecretPlaceholder = "CH_SECRET_PLACEHOLDER"
const clickhouseDefaultUsernamePlaceholder = "CH_DEFAULTUSER_NAME_PLACEHOLDER"
const clickhouseDefaultUserPasswordPlaceholder = "CH_DEFAULTUSER_SHA256PASS_PLACEHOLDER"

func ensureClickhouse(logger *zap.SugaredLogger, gdkIP string) (string, string, status.PartialStatus) {
	user, pass, err := ensureClickhouseDefaultUserCreds(logger)
	if err != nil {
		return "", "", status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf("unable to ensure clickhouse default-user credentials: %v", err),
			},
		}
	}
	err = ensureClickhouseReplicationSecret(logger)
	if err != nil {
		return "", "", status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf("unable to ensure clickhouse replication secret: %v", err),
			},
		}
	}
	err = ensureClickhouseServices(logger)
	if err != nil {
		return "", "", status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf("unable to ensure clickhouse service: %v", err),
			},
		}
	}

	logger.Info("clickhouse has been set-up")
	return user, pass, status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			fmt.Sprintf("username: %s", user),
			fmt.Sprintf("password: %s", pass),
			fmt.Sprintf("cli cmd: clickhouse-client -h %s --port 9000 -u %s --password %s", gdkIP, user, pass),
		},
	}
}

func ensureClickhouseServices(logger *zap.SugaredLogger) error {
	for _, svc := range []string{"clickhouse-keeper", "clickhouse-server"} {
		enabled, err := isServiceEnabled(logger, svc)
		if err != nil {
			return err
		}
		if !enabled {
			logger.Infof("enabling service %q", svc)
			err = enableService(logger, svc)
			if err != nil {
				return err
			}
		}
		isRunning, err := isServiceRunning(logger, svc)
		if err != nil {
			return err
		}
		if !isRunning {
			logger.Infof("starting service %q", svc)
			err = startService(logger, svc)
			if err != nil {
				return err
			}
		}
	}

	logger.Debug("clickhouse services have been ensured")
	return nil
}

// No need to store the repliation secret, we just want to make it uniqe - just
// in case
func ensureClickhouseReplicationSecret(logger *zap.SugaredLogger) error {
	confBytes, err := os.ReadFile(constants.ClickhouseConfPath)
	if err != nil {
		return fmt.Errorf("unable to read conf file %s: %w", constants.ClickhouseConfPath, err)
	}
	conf := string(confBytes)

	configUpdated := false

	re := regexp.MustCompile(`<secret>(\w+)</secret>`)
	submatches := re.FindStringSubmatch(conf)
	switch {
	case submatches == nil || len(submatches) != 2:
		err = fmt.Errorf(
			"clickhouse config file %q is malformed, secret submatches: %v",
			constants.ClickhouseConfPath, submatches,
		)
		return err
	case submatches[1] == clickhouseReplicationSecretPlaceholder:
		pass, err := password.Generate(32, 10, 0, false, true)
		if err != nil {
			return fmt.Errorf("unable to generate secret: %w", err)
		}
		conf = strings.Replace(conf, clickhouseReplicationSecretPlaceholder, pass, 1)
		configUpdated = true
		logger.Info("clickhouse replication secret has been updated")
	}

	if configUpdated {
		err = os.WriteFile(constants.ClickhouseConfPath, []byte(conf), 0640)
		if err != nil {
			return fmt.Errorf("unable to write conf file %s: %w", constants.ClickhouseConfPath, err)
		}
	}

	logger.Debug("clickhouse replication secret has been ensured")
	return nil
}

func ensureClickhouseDefaultUserCreds(logger *zap.SugaredLogger) (string, string, error) {
	confBytes, err := os.ReadFile(constants.ClickhouseDefaultUserConfPath)
	if err != nil {
		return "", "", fmt.Errorf("unable to read conf file %s: %w", constants.ClickhouseDefaultUserConfPath, err)
	}
	conf := string(confBytes)

	configUpdated := false

	var pass string
	re := regexp.MustCompile(`<password>(\w+)</password>`)
	submatches := re.FindStringSubmatch(conf)
	switch {
	case submatches == nil || len(submatches) != 2:
		err = fmt.Errorf(
			"clickhouse default user config %q file is malformed, password submatches: %v",
			constants.ClickhouseDefaultUserConfPath, submatches,
		)
		return "", "", err
	case submatches[1] == clickhouseDefaultUserPasswordPlaceholder:
		pass, err = password.Generate(32, 10, 0, false, true)
		if err != nil {
			return "", "", fmt.Errorf("unable to generate password: %w", err)
		}
		conf = strings.Replace(conf, clickhouseDefaultUserPasswordPlaceholder, pass, 1)
		configUpdated = true
		logger.Info("clickhouse password has been updated")
	default:
		pass = submatches[1]
	}

	var name string
	re = regexp.MustCompile(`<users><(\w+)>`)
	submatches = re.FindStringSubmatch(conf)
	switch {
	case submatches == nil || len(submatches) != 2:
		err = fmt.Errorf(
			"clickhouse default user config file %q is malformed, user submatches: %v",
			constants.ClickhouseDefaultUserConfPath, submatches,
		)
		return "", "", err
	case submatches[1] == clickhouseDefaultUsernamePlaceholder:
		rand.Seed(time.Now().UnixNano())
		name = namesgenerator.GetRandomName(0)
		conf = strings.Replace(conf, clickhouseDefaultUsernamePlaceholder, name, 2)
		configUpdated = true
		logger.Info("clickhouse default username has been updated")
	default:
		name = submatches[1]
	}

	if configUpdated {
		err = os.WriteFile(constants.ClickhouseDefaultUserConfPath, []byte(conf), 0640)
		if err != nil {
			return "", "", fmt.Errorf("unable to write conf file %s: %w", constants.ClickhouseDefaultUserConfPath, err)
		}
	}

	logger.Debug("clickhouse default user credentials have been ensured")
	return name, pass, nil
}
