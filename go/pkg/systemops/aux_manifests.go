package systemops

import (
	"context"
	"embed"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer/yaml"
	"k8s.io/apimachinery/pkg/util/wait"
	yamlutil "k8s.io/apimachinery/pkg/util/yaml"
	"sigs.k8s.io/controller-runtime/pkg/client"
	k8s_yaml "sigs.k8s.io/yaml"
)

const (
	metallbNamespace = "metallb-system"
)

// NOTE(prozlach): We must not import here types from metallb repo as this
// will lead to dependency hell. Shelling out to kubectl seems ugly and has its
// own issues, dynamic client plus untyped objects is the best approach.
const ingressLBManifestTmpl = `apiVersion: v1
kind: Service
metadata:
  labels:
    opstrace.com/name: dev-cluster
    opstrace.com/namespace: ""
  name: ingress-controller-lb
  namespace: %s
spec:
  ports:
  - name: http
    appProtocol: http
    port: 80
    protocol: TCP
    targetPort: web
  - name: https
    appProtocol: https
    port: 443
    protocol: TCP
    targetPort: websecure
  selector:
    app.kubernetes.io/instance: traefik-default
    app.kubernetes.io/name: traefik
  type: LoadBalancer
  loadBalancerIP: %s`

const metallbIPRangesManifestTmpl = `apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: gob-ippool
  namespace: metallb-system
spec:
  addresses:
  - %s-%s`

const l2AdvManifest = `apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: empty
  namespace: metallb-system`

//go:embed k8s_manifests/*.yml
var auxManifests embed.FS

func ensureAuxManifests(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	vipMinAddress, vipMaxAddress string,
) status.PartialStatus {
	//TODO(prozlach): consider using helm and simply waiting for release to get ready
	err := applyManifests(logger, k8sClient)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure auxilary manifests: %v", err,
				),
			},
		}
	}

	err = waitDeploymentReady(
		logger, k8sClient,
		metallbNamespace, "controller",
		2*time.Second, 45*time.Second,
	)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"metall-lb controller did not get ready in time: %v", err,
				),
			},
		}
	}

	err = waitDaemonsetReady(
		logger, k8sClient,
		metallbNamespace, "speaker",
		2*time.Second, 45*time.Second,
	)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"metall-lb's `speaker` daemonset did not get ready in time: %v", err,
				),
			},
		}
	}

	err = ensurel2AdvObject(logger, k8sClient)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure IPAddressPool object: %v", err,
				),
			},
		}
	}

	err = ensureIpPoolObject(logger, k8sClient, vipMinAddress, vipMaxAddress)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure IPAddressPool object: %v", err,
				),
			},
		}
	}

	err = ensureNginxLBObject(logger, k8sClient, vipMinAddress)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure Service of type loadBalancer: %v", err,
				),
			},
		}
	}

	err = waitLBReady(
		logger, k8sClient,
		constants.GOBSchedulerNamespace, "ingress-controller-lb", vipMinAddress,
		2*time.Second, 45*time.Second,
	)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"LB service was not assigned IP %s in time: %v", vipMinAddress, err,
				),
			},
		}
	}

	// Values selected arbitrarly
	err = waitGatekeeperReady(logger, 2*time.Second, 45*time.Second)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"Gatekeeper has not become ready in time: %v", err,
				),
			},
		}
	}

	return status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			"GOB is available at https://gob.devvm/",
		},
	}
}

func applyManifests(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
) error {
	manifests, err := parseManifests(logger)
	if err != nil {
		return fmt.Errorf("unable to parse embedded manifests: %w", err)
	}

	for _, manifest := range manifests {
		err = applyUnstructured(logger, k8sClient, manifest)
		if err != nil {
			return fmt.Errorf("unable to apply manifest: %w", err)
		}
	}

	return nil
}

func parseManifests(logger *zap.SugaredLogger) ([]*unstructured.Unstructured, error) {
	res := make([]*unstructured.Unstructured, 0)

	dirEntries, err := auxManifests.ReadDir("k8s_manifests")
	if err != nil {
		return nil, fmt.Errorf("unable to open embedded manifests dir: %w", err)
	}

	for _, dirEntry := range dirEntries {
		if !strings.HasSuffix(dirEntry.Name(), ".yml") {
			logger.Debugf("skipping file %q from manifests directory", dirEntry.Name())
			continue
		}

		path := "k8s_manifests/" + dirEntry.Name()
		fh, err := auxManifests.Open(path)
		if err != nil {
			return nil, fmt.Errorf("unable to open embedded file %s: %w", path, err)
		}

		decoder := yamlutil.NewYAMLOrJSONDecoder(fh, 1024)
		for {
			obj, err := getResource(decoder)
			if err != nil {
				if errors.Is(err, io.EOF) {
					break
				}

				return nil, fmt.Errorf("unable to decode manifests from embbed FS file %s: %w", path, err)
			}
			res = append(res, obj)
		}
	}

	return res, nil
}

func getResource(decoder *yamlutil.YAMLOrJSONDecoder) (*unstructured.Unstructured, error) {
	var rawObj runtime.RawExtension
	if err := decoder.Decode(&rawObj); err != nil {
		return nil, fmt.Errorf("unable to decode object: %w", err)
	}

	// NOTE(prozlach): rawObj.Object attribute is not set by decoder.Decode, we
	// need an extra decoding step:
	unstructuredObj := &unstructured.Unstructured{}
	_, _, err := yaml.NewDecodingSerializer(unstructured.UnstructuredJSONScheme).Decode(rawObj.Raw, nil, unstructuredObj)
	if err != nil {
		return nil, fmt.Errorf("unable to decode object to unstructured: %w", err)
	}

	return unstructuredObj, nil
}

func ensureIpPoolObject(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	vipMinAddress, vipMaxAddress string,
) error {
	manifest := fmt.Sprintf(metallbIPRangesManifestTmpl, vipMinAddress, vipMaxAddress)

	obj := new(unstructured.Unstructured)
	err := k8s_yaml.Unmarshal([]byte(manifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	return applyUnstructured(logger, k8sClient, obj)
}

func ensurel2AdvObject(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
) error {
	obj := new(unstructured.Unstructured)
	err := k8s_yaml.Unmarshal([]byte(l2AdvManifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	return applyUnstructured(logger, k8sClient, obj)
}

func ensureNginxLBObject(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	vipMinAddress string,
) error {
	manifest := fmt.Sprintf(ingressLBManifestTmpl, constants.GOBSchedulerNamespace, vipMinAddress)

	obj := new(unstructured.Unstructured)
	err := k8s_yaml.Unmarshal([]byte(manifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	return applyUnstructured(logger, k8sClient, obj)
}

func waitGatekeeperReady(logger *zap.SugaredLogger, interval, timeout time.Duration) error {
	var waitF wait.ConditionFunc = func() (bool, error) {
		// TODO(prozlach): make it cmdline param, value chosen arbitrary.
		ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelF()

		url := "https://gob.devvm/readyz"
		resp, status, err := httpGetReq(ctx, logger, url, nil, true)
		if err != nil {
			return false, fmt.Errorf("http GET request failed: %w", err)
		}
		if status != http.StatusOK {
			logger.Infof("http GET request returned status %d", status)
			return false, nil
		}

		if !strings.Contains(string(resp), "Success") {
			logger.Infof("http does not contain `Success` keyword")
			return false, nil
		}

		logger.Infof("Gatekeeper responded!")
		return true, nil

	}

	return wait.PollImmediate(interval, timeout, waitF)
}
