package systemops

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/sethvargo/go-password/password"
	"github.com/xanzy/go-gitlab"
	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/railsconsole"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/util/wait"
)

const (
	// Yeah, it can take a lot of time. Webpack compilation  of all the assets
	// takes around 10 minutes.
	gdkWarmUpDelayTimeout = 13 * time.Minute
)

func ensureGDKRunning(
	logger *zap.SugaredLogger, commitID string,
) (
	*gogitlab.Client, string, *railsconsole.RailsConsole, status.PartialStatus,
) {
	isRunning, err := checkGDKStatus(logger)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"checking GDK status failed: %v", err,
				),
			},
		}
	}

	if !isRunning {
		err = checkoutRepo(logger, commitID, constants.GitLabRepositoryPath)
		if err != nil {
			return nil, "", nil, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"checking out GitLab repo failed: %v", err,
					),
				},
			}
		}

		err = updateGitlabDeps(logger)
		if err != nil {
			return nil, "", nil, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"updating GDK dependencies failed: %v", err,
					),
				},
			}
		}

		// restarting GDK also starts it if it was not already running
		err = restartGDK(logger)
		if err != nil {
			return nil, "", nil, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"starting GDK failed: %v", err,
					),
				},
			}
		}
	}

	err = runMigrations(logger)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"GDK migrations: %v", err,
				),
			},
		}
	}

	// After running migrations, we need to restart GDK again, otherwise weird
	// things happen like e.g. DB deadlocks
	err = restartGDK(logger)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"restarting GDK failed: %v", err,
				),
			},
		}
	}

	err = warmUpGDK(logger)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"warm-up request to GDK failed: %v", err,
				),
			},
		}
	}

	rc, err := railsconsole.New(logger)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to launch rails console session: %v", err,
				),
			},
		}
	}

	gdkPassword, gdkToken, err := setGDKCredentials(logger, rc)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to confiure GDK credentials: %v", err,
				),
			},
		}
	}

	err = enableIntegratedErrorTracking(logger, rc)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to enable integrated error tracking tab: %v", err,
				),
			},
		}
	}

	err = enableGitlabErrorTracking(logger, rc)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to enable Gitlab-based error tracking: %v", err,
				),
			},
		}
	}

	err = enableObservabilityFeatures(logger, rc)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to enable Gitlab-based tracing: %v", err,
				),
			},
		}
	}

	err = enableCHErrorTracking(logger, rc)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to enable ch backend for observability: %v", err,
				),
			},
		}
	}

	err = updateAllowList(logger, rc)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to update allow list for observability: %v", err,
				),
			},
		}

	}

	_, err = os.Stat(constants.EELicensePath)
	licenseProvided := err == nil

	if licenseProvided {
		err = uploadEELicense(logger, rc)
		if err != nil {
			return nil, "", nil, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"unable to upload EE license to Gitlab: %v", err,
					),
				},
			}
		}
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}
	httpClient := &http.Client{Transport: tr}

	gitlabClient, err := gogitlab.NewBasicAuthClient(
		constants.GDKRootUser,
		gdkPassword,
		gitlab.WithBaseURL("https://gdk.devvm:3443/api/v4"),
		gitlab.WithHTTPClient(httpClient),
	)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"failed to create Gitlab client: %v", err,
				),
			},
		}
	}

	err = setCustomErrorTrackingURL(logger, gitlabClient)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"failed to set custom error-tracking URL: %v", err,
				),
			},
		}
	}

	etToken, err := fetchErrorTrackingToken(logger, rc)
	if err != nil {
		return nil, "", nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"failed to fetch error tracking auth token: %v", err,
				),
			},
		}
	}

	// https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2652#note_1755071627
	// err = ensureSidekiqIsRunning(logger, rc)
	// if err != nil {
	// 	return nil, "", nil, status.PartialStatus{
	// 		Succeded: status.StatusStageFailed,
	// 		DetailedMessage: []string{
	// 			fmt.Sprintf(
	// 				"failed workaround sidekiq borkage: %v", err,
	// 			),
	// 		},
	// 	}
	// }

	return gitlabClient, etToken, rc, status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			"GitLab is now available at https://gdk.devvm:3443/",
			fmt.Sprintf("Username: %s", constants.GDKRootUser),
			fmt.Sprintf("Password: %s", gdkPassword),
			fmt.Sprintf("Gitlab API Admin Token: %s", gdkToken),
			fmt.Sprintf("EE License uploaded: %t", licenseProvided),
			fmt.Sprintf("Checked Out GitLab Commit: %s", commitID),
			"ATTENTION: Please remember to connect through VPN to devvmm and update /etc/hosts file first!",
		},
		Metadata: map[string]string{
			"token": gdkToken,
		},
	}
}

func setCustomErrorTrackingURL(logger *zap.SugaredLogger, client *gogitlab.Client) error {
	logger.Infof("setting custom error tracking URL")

	// Official client library does not support these settings yet
	req, err := client.NewRequest(http.MethodPut, "application/settings", nil, nil)
	if err != nil {
		return err
	}

	q := req.URL.Query()
	q.Add("error_tracking_enabled", "true")
	q.Add("error_tracking_api_url", "https://gob.devvm:443/")
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req, nil)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("non-200 status code returned from Gitlab: %s", resp.Status)
	}

	return nil
}

// warmUpGDK issues a request to gdk to trigger compilation of Ruby stuff (TM)
// so that users can access it later on without delay. We need to do it in a
// loop as GDK can return 502 and friends during this process...
func warmUpGDK(logger *zap.SugaredLogger) error {
	var gdkReadyF = func() (bool, error) {
		ctx, cancelF := context.WithTimeout(context.Background(), 60*time.Second)
		defer cancelF()

		logger.Debugf("performing warm-up request to GDK")
		url := "https://gdk.devvm:3443/users/sign_in"
		resp, status, err := httpGetReq(ctx, logger, url, nil, true)
		if err != nil {
			logger.Debugf("http GET request failed: %v", err)
			return false, nil
		}
		if status != http.StatusOK {
			logger.Debugf("http GET request returned status %d", status)
			return false, nil
		}

		if !strings.Contains(string(resp), "Username or primary email") {
			logger.Debugf("malformed content received from GDK, please open https://gdk.devvm:3443/ and verify")
			return false, nil
		}

		logger.Info("GDK has warmed up!")
		return true, nil
	}

	// Values selected arbitrarly
	logger.Info("Warming GDK up")
	return wait.PollImmediate(2*time.Second, gdkWarmUpDelayTimeout, gdkReadyF)
}

func updateGitlabDeps(logger *zap.SugaredLogger) error {
	_, err := executeCmdWithAsdf(
		logger,
		constants.GitLabRepositoryPath,
		"awk '{ system(\"asdf plugin-add \" $1) }' < .tool-versions",
		"installing asdf plugins from gitlab directory",
	)
	if err != nil {
		return fmt.Errorf("installing gitlab asdf plugins failed: %w", err)
	}

	_, err = executeCmdWithAsdf(
		logger,
		constants.GitLabRepositoryPath,
		"asdf install",
		"install gitlab asdf tool versions",
	)
	if err != nil {
		return fmt.Errorf("installing gitlab asdf tool versions failed: %w", err)
	}

	msg := "running bundle install"
	_, err = executeCmdWithAsdf(logger, constants.GitLabRepositoryPath, "bundle install", msg)
	if err != nil {
		return fmt.Errorf("bundle install failed: %w", err)
	}

	_, err = executeCmdWithAsdf(
		logger,
		constants.GitLabRepositoryPath,
		"yarn install",
		"running yarn install",
	)
	if err != nil {
		return fmt.Errorf("yarn install failed: %w", err)
	}

	return nil
}

func runMigrations(logger *zap.SugaredLogger) error {
	_, err := executeCmdWithAsdf(
		logger,
		constants.GitLabRepositoryPath,
		constants.GDKRepositoryPath+"support/bundle-exec rails db:migrate",
		"running db:migrations",
	)
	if err != nil {
		return fmt.Errorf("db:migrations failed: %w", err)
	}

	return nil
}

func checkGDKStatus(logger *zap.SugaredLogger) (bool, error) {
	msg := "checking GDK status"
	out, err := executeCmdWithAsdf(logger, constants.GDKRepositoryPath, "gdk status", msg)
	if err != nil {
		return false, err
	}

	lines := strings.Split(out, "\n")
	isUp := true
	re := regexp.MustCompile(`^((run|down): |=> GitLab available|\s*)`)
	for _, line := range lines {
		submatches := re.FindStringSubmatch(line)
		if len(submatches) < 3 {
			err = fmt.Errorf("malformed line found in output of `gdk status`: %s", line)
			return false, err
		}

		if submatches[0] == "" || submatches[0] == "=> GitLab available" {
			continue
		}

		if submatches[2] == "down" {
			isUp = false
		}
	}

	logger.Debugf("GDK up==%v", isUp)
	return isUp, nil
}

func enableObservabilityFeatures(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) error {
	output, err := rc.PushCommand("Feature.enable(:observability_features)")
	if err != nil {
		return err
	}
	if !strings.Contains(output, "=> true") {
		return errors.New("gdk command was not successfull, please check booter logs for more details")
	}
	return nil
}

func uploadEELicense(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) error {
	cmd := fmt.Sprintf(
		`License.new(data: File.open("%s").read.gsub("\r\n", "\n").gsub(/\n+$/, "") + "\n").save`,
		constants.EELicensePath,
	)
	output, err := rc.PushCommand(cmd)
	if err != nil {
		return err
	}
	if !strings.Contains(output, "=> true") {
		return errors.New("gdk command was not successfull, please check booter logs for more details")
	}
	return nil
}

func enableCHErrorTracking(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) error {
	output, err := rc.PushCommand("Feature.enable(:use_click_house_database_for_error_tracking)")
	if err != nil {
		return err
	}
	if !strings.Contains(output, "=> true") {
		return errors.New("gdk command was not successfull, please check booter logs for more details")
	}
	return nil
}

func enableIntegratedErrorTracking(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) error {
	output, err := rc.PushCommand("Feature.enable(:integrated_error_tracking)")
	if err != nil {
		return err
	}
	if !strings.Contains(output, "=> true") {
		return errors.New("gdk command was not successfull, please check booter logs for more details")
	}
	return nil
}

func fetchErrorTrackingToken(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) (string, error) {
	output, err := rc.PushCommand("Gitlab::CurrentSettings.error_tracking_access_token")
	if err != nil {
		return "", err
	}

	lines := strings.Split(output, "\n")
	re := regexp.MustCompile(`^=> "([\w-_]+)"`)
	for _, line := range lines {
		submatches := re.FindStringSubmatch(line)
		if len(submatches) < 2 {
			continue
		}
		return submatches[1], nil
	}

	return "", fmt.Errorf("error tracking token not found in gdk rails console output")
}

func enableGitlabErrorTracking(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) error {
	output, err := rc.PushCommand("Feature.enable(:gitlab_error_tracking)")
	if err != nil {
		return err
	}
	if !strings.Contains(output, "=> true") {
		return errors.New("gdk command was not successfull, please check booter logs for more details")
	}
	return nil
}

func updateAllowList(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) error {
	output, err := rc.PushCommand(`Gitlab::CurrentSettings.update(outbound_local_requests_whitelist: ['gob.devvm'])`)
	if err != nil {
		return err
	}
	if !strings.Contains(output, "=> true") {
		return errors.New("gdk command for allowlist was not successfull, please check booter logs for more details")
	}

	return nil
}

func ensureSidekiqIsRunning(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) error {
	isSidekiqAlive := func() (bool, error) {
		output, err := rc.PushCommand("require 'sidekiq/monitor'; Sidekiq::Monitor::Status.new.display('processes');")
		if err != nil {
			return false, err
		}

		lines := strings.Split(output, "\n")
		correctReply := false
		sidekiqDead := false
		re := regexp.MustCompile(`^---- Processes \((\d+)\) ----`)
		for _, line := range lines {
			submatches := re.FindStringSubmatch(line)
			if len(submatches) < 2 {
				continue
			}
			correctReply = true
			if submatches[1] == "0" {
				sidekiqDead = true
			}
		}
		if !correctReply {
			return false, fmt.Errorf("malformed reply received from GDK console, unable to determine if sidekiq died or nor")
		}

		return !sidekiqDead, nil
	}
	alive, err := isSidekiqAlive()
	if err != nil {
		return err
	}
	if alive {
		return nil
	}

	msg := "restarting GDK sidekiq to workaround its borkage during GDK start"
	out, err := executeCmdWithAsdf(logger, constants.GDKRepositoryPath, "gdk restart rails-background-jobs", msg)
	if err != nil {
		return err
	}

	lines := strings.Split(out, "\n")
	started := false
	for _, line := range lines {
		if strings.Contains(line, "ok: run: /home/dev/gitlab-development-kit/services/rails-background-jobs") {
			started = true
		}
	}
	if !started {
		return fmt.Errorf("restarting sidekiq failed")
	}

	deadlineTimer := time.NewTimer(2 * time.Minute)
	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-deadlineTimer.C:
			return fmt.Errorf("despite restart, sidekick continously reports no processes alive, giving up")
		case <-ticker.C:
			alive, err = isSidekiqAlive()
			if err != nil {
				return err
			}
			if alive {
				return nil
			}
		}
	}
}

// restartGDK restarts GDK but also starts it if it is not already running
func restartGDK(logger *zap.SugaredLogger) error {
	msg := "stopping GDK"
	_, err := executeCmdWithAsdf(logger, constants.GDKRepositoryPath, "gdk stop", msg)
	if err != nil {
		return err
	}

	msg = "starting GDK"
	out, err := executeCmdWithAsdf(logger, constants.GDKRepositoryPath, "gdk start", msg)
	if err != nil {
		return err
	}

	lines := strings.Split(out, "\n")
	re := regexp.MustCompile(`^(ok: (\w+): |=> GitLab available|\s*)`)
	for _, line := range lines {
		submatches := re.FindStringSubmatch(line)
		if len(submatches) < 3 {
			err = fmt.Errorf("malformed line found in output of `gdk start`: %s", line)
			return err
		}

		if submatches[0] == "" || submatches[0] == "=> GitLab available" {
			continue
		}

		if submatches[2] != "run" {
			err = fmt.Errorf("failed to restart GDK, unexpected subcomponents status %s, line: %s", submatches[1], line)
			return err
		}
	}

	// TODO(prozlach): Do something smarter here to verify that GDK has
	// actually started, for now we just wait few seconds to see if
	// services keep being up.
	time.Sleep(3 * time.Second)

	isRunning, err := checkGDKStatus(logger)
	if err != nil {
		return fmt.Errorf("re-checking GDK status failed: %w", err)
	}
	if !isRunning {
		return errors.New("GDK failed to start")
	}

	return nil
}

func setGDKCredentials(
	logger *zap.SugaredLogger,
	rc *railsconsole.RailsConsole,
) (string, string, error) {
	type PasswordsStore struct {
		Password string
		Token    string
	}
	tmp := new(PasswordsStore)

	b, err := os.ReadFile(constants.GDKPasswordFilePath)
	if err == nil {
		logger.Infof("retrieved cached gdk credentials from %s", constants.GDKPasswordFilePath)

		err = json.Unmarshal(b, tmp)
		if err != nil {
			return "", "", fmt.Errorf("unable to unmarschal GDK passwords file: %w", err)
		}

		logger.Infof("GDK credentials retrieved from file")
	} else {
		logger.Info("generating new GDK password")
		tmp.Password, err = password.Generate(32, 10, 0, false, true)
		if err != nil {
			return "", "", fmt.Errorf("unable to generate password: %w", err)
		}

		tmp.Token, err = password.Generate(32, 10, 0, false, true)
		if err != nil {
			return "", "", fmt.Errorf("unable to generate token: %w", err)
		}

		logger.Info("setting new GDK password")
		cmd := fmt.Sprintf(
			"user=User.find_by_username('root');"+
				"user.password='%s';user.password_confirmation='%s';"+
				"user.password_expires_at=356.day.from_now;"+
				"user.save!", tmp.Password, tmp.Password,
		)
		output, err := rc.PushCommand(cmd)
		if err != nil {
			return "", "", err
		}
		if !strings.Contains(output, "=> true") {
			return "", "", errors.New("gdk command was not successfull, please check booter logs for more details")
		}

		logger.Info("generating new GDK token")
		cmd = fmt.Sprintf(
			"user = User.find_by_username('root');"+
				"token = user.personal_access_tokens.create(scopes: [:api, :admin_mode], name: 'devvm-token', expires_at: 31.day.from_now);"+
				"token.set_token('%s');"+
				"token.save!", tmp.Token,
		)
		output, err = rc.PushCommand(cmd)
		if err != nil {
			return "", "", err
		}
		if !strings.Contains(output, "=> true") {
			return "", "", errors.New("gdk command was not successfull, please check booter logs for more details")
		}

		b, err = json.Marshal(tmp)
		if err != nil {
			return "", "", fmt.Errorf("unable to marshal GDK password: %w", err)
		}
		// NOTE(prozlach): Not sure about grainting 'o+r' here. OTOH it is just a
		// dev machine.
		err = os.WriteFile(constants.GDKPasswordFilePath, b, 0640)
		if err != nil {
			return "", "", fmt.Errorf("unable to write GDK credentials file %s: %w", constants.GDKPasswordFilePath, err)
		}

		logger.Infof("GDK credentials were generated and are stored for future use")
	}

	return tmp.Password, tmp.Token, nil
}
