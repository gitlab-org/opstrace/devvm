package systemops

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/xanzy/go-gitlab"
	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/misc"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/util/wait"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"
	k8s_yaml "sigs.k8s.io/yaml"
)

const (
	gobTenantTimeout = 5 * time.Minute
)

const gobGitlabNamespaceTmpl = `apiVersion: opstrace.com/v1alpha1
kind: GitLabNamespace
metadata:
  name: "%d"
spec:
  id: %d
  top_level_namespace_id: %d
  name: %s
  path: %s
  full_path: %s
  avatar_url: ""
  web_url: %s
  overrides:
    operator:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}`

const gobObservabilityTenantTmpl = `
apiVersion: opstrace.com/v1alpha1
kind: GitLabObservabilityTenant
metadata:
  name: tenant-%d
spec:
  topLevelNamespaceID: %d
`

const gobLocalTenantData = `
apiVersion: v1
kind: Secret
metadata:
  name: local-tenant-data
  namespace: default
type: Opaque
stringData:
  gitlab_otel_namespaceid: "%d"
  gitlab_otel_project_id: "%d"
`

func ensureGOBTenant(
	logger *zap.SugaredLogger,
	gitlabClient *gogitlab.Client,
	k8sClient client.Client,
	gitlabNS *gogitlab.Namespace,
) (int, status.PartialStatus) {
	gitlabProject, err := getGitlabProject(logger, gitlabClient, gitlabNS.ID)
	if err != nil {
		return -1, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to get information about Gitlab Namespace/Group to use: %v", err,
				),
			},
		}
	}

	err = ensureGOBNs(logger, k8sClient, gitlabNS)
	if err != nil {
		return -1, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure GitlabNamespace k8s object: %v", err,
				),
			},
		}
	}

	err = ensureGOBObservabilityTenant(logger, k8sClient, gitlabNS.ID)
	if err != nil {
		return -1, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure GitlabObservabilityTenant k8s object: %v", err,
				),
			},
		}
	}

	err = ensureTenantOverrideSecret(logger, k8sClient, gitlabNS.ID, gitlabProject.ID)
	if err != nil {
		return -1, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure tenant override secret: %v", err,
				),
			},
		}
	}

	return gitlabProject.ID, status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			fmt.Sprintf("Gitlab Group ID: %d", gitlabNS.ID),
			fmt.Sprintf("Gitlab Group Name: %s", gitlabNS.Name),
			fmt.Sprintf("Gitlab Group URL: %s", gitlabNS.WebURL),
			fmt.Sprintf("Gitlab Project ID: %d", gitlabProject.ID),
			fmt.Sprintf("Gitlab Project Name: %s", gitlabProject.Name),
			fmt.Sprintf("Gitlab Project URL: %s", gitlabProject.WebURL),
			// FIXME(prozlach): Once we create and wait for GitlabNamespace object
			// to get ready, there is still Argus Group tasks that are not
			// currently automated and require user to do manual action.
			//
			// Infra that required it is going away soonish, no point in
			// automating it as this step is going away with it.
			fmt.Sprintf("ATTENTION: Visit https://gob.devvm/v1/provision/%d to finalize provisioning tenant", gitlabNS.ID),
		},
	}
}

func getGitlabNs(
	logger *zap.SugaredLogger,
	gitlabClient *gogitlab.Client,
) (*gogitlab.Namespace, error) {
	curUser, _, err := gitlabClient.Users.CurrentUser()
	if err != nil {
		return nil, fmt.Errorf("unable to get information about the current user from Gitlab: %w", err)
	}
	logger.Debugw("current user info fetched", "Name", curUser.Name, "ID", curUser.ID)

	memberships, _, err := gitlabClient.Users.GetUserMemberships(
		curUser.ID, &gitlab.GetUserMembershipOptions{},
	)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch information about current user's memberships: %w", err)
	}

	// In order to always pick the same NS, we simply return the one with
	// lowest ID.
	var nsWithLowestID *gogitlab.Namespace
	for _, membership := range memberships {
		if membership.AccessLevel < gitlab.OwnerPermission {
			logger.Debugf(
				"candidate NS rejected due to inssuficient AccessLevel, %d < %d",
				membership.AccessLevel, gitlab.OwnerPermission,
			)
			continue
		}
		if membership.SourceType != "Namespace" {
			logger.Debugf(
				"candidate NS rejected due to wrong type, %s != 'Namespace'",
				membership.SourceType,
			)
			continue
		}
		ns, _, err := gitlabClient.Namespaces.GetNamespace(membership.SourceID)
		if err != nil {
			return nil, fmt.Errorf("unable to fetch gitlab namespace %d: %w", membership.SourceID, err)
		}
		if ns.Kind != "group" {
			logger.Debugf(
				"candidate NS rejected due to wrong kind, %s != 'group'",
				ns.Kind,
			)
			continue
		}
		if ns.ParentID != 0 {
			logger.Debugf(
				"candidate NS rejected due to not being root NS, ParentID=%d",
				ns.ParentID,
			)
			continue
		}
		if nsWithLowestID == nil || nsWithLowestID.ID > ns.ID {
			nsWithLowestID = ns
		}
	}

	if nsWithLowestID == nil {
		return nil, fmt.Errorf("no suitable namespaces to deploy GOB to, found for user %s", curUser.Name)
	}

	logger.Infow(
		"Gitlab namespace suitable for GOB found",
		"ID", nsWithLowestID.ID, "Name", nsWithLowestID.Name,
	)
	return nsWithLowestID, nil
}

func getGitlabProject(
	logger *zap.SugaredLogger,
	gitlabClient *gogitlab.Client,
	groupID int,
) (*gogitlab.Project, error) {
	opts := &gitlab.ListGroupProjectsOptions{
		IncludeSubGroups: misc.Ptr(true),
	}
	projects, _, err := gitlabClient.Groups.ListGroupProjects(groupID, opts)
	if err != nil {
		return nil, fmt.Errorf("unable to get fetch projects list for group %d: %w", groupID, err)
	}
	if len(projects) == 0 {
		return nil, fmt.Errorf("group with ID %d does not have any projects", groupID)
	}

	// In order to always pick the same project, we simply return the one with
	// the lowest ID.
	var res *gogitlab.Project
	for _, project := range projects {
		if res == nil || res.ID > project.ID {
			res = project
		}
	}

	logger.Infow(
		"Gitlab project suitable for GOB observability tenant found",
		"ID", res.ID, "Name", res.Name,
	)
	return res, nil
}

func ensureGOBNs(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	group *gitlab.Namespace,
) error {
	nsExists, err := checkGOBGitlabNSExists(logger, k8sClient, group.ID)
	if err != nil {
		return fmt.Errorf(
			"unable to check if GitlabNamespace k8s object exists: %w", err)
	}

	if !nsExists {
		err := createGOBGitlabNs(logger, k8sClient, group)
		if err != nil {
			return fmt.Errorf("unable to create GitlabNamespace k8s object: %w", err)
		}
	}

	err = waitGOBGitlabNSReady(logger, k8sClient, group.ID)
	if err != nil {
		return fmt.Errorf("error occured while waiting for GitlabNamespace to get ready: %w", err)
	}

	return nil
}

func checkGOBGitlabNSExists(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	nsID int,
) (bool, error) {
	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	key := client.ObjectKey{
		Name: strconv.Itoa(nsID),
	}

	obj := new(unstructured.Unstructured)
	obj.SetAPIVersion("opstrace.com/v1alpha1")
	obj.SetKind("GitLabNamespace")

	err := k8sClient.Get(ctx, key, obj)
	if err != nil {
		if kerrors.IsNotFound(err) {
			return false, nil
		}
		return false, fmt.Errorf("unable to fetch GitLabNamespace object: %w", err)
	}

	return true, nil
}

func createGOBGitlabNs(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	gitlabNS *gogitlab.Namespace,
) error {
	manifest := fmt.Sprintf(
		gobGitlabNamespaceTmpl,
		gitlabNS.ID, gitlabNS.ID, gitlabNS.ID, gitlabNS.Name, gitlabNS.Path, gitlabNS.FullPath, gitlabNS.WebURL,
	)

	obj := new(unstructured.Unstructured)
	err := yaml.Unmarshal([]byte(manifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	return applyUnstructured(logger, k8sClient, obj)
}

func waitGOBGitlabNSReady(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	nsID int,
) error {
	var nsReadyF = func(obj *unstructured.Unstructured) (bool, error) {
		conditions, err := conditionsFromUnstructured(obj, "status", "conditions")
		if err != nil {
			return false, err
		}

		ready, err := isReady(conditions)
		if err != nil {
			if errors.Is(err, ErrNoStatusCondition) {
				logger.Info("GitlabNameSpace object does not have any conditions yet")
				return false, nil
			}
			return false, err
		}

		if !ready {
			logger.Info("waiting for GitlabNSObject object to become ready")
			return false, nil
		}
		logger.Info("GitLabNameSpace object is has Ready status")

		logger.Info("GitlabNameSpace became ready!")
		return true, nil
	}

	var waitF wait.ConditionFunc = func() (bool, error) {
		// TODO(prozlach): make it cmdline param, value chosen arbitrary.
		ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelF()

		key := client.ObjectKey{
			Name: strconv.Itoa(nsID),
		}

		obj := new(unstructured.Unstructured)
		obj.SetAPIVersion("opstrace.com/v1alpha1")
		obj.SetKind("GitLabNamespace")

		err := k8sClient.Get(ctx, key, obj)
		if err != nil {
			return false, fmt.Errorf("unable to fetch GitLabNamespace object: %w", err)
		}

		return nsReadyF(obj)
	}

	// Values selected arbitrarly
	return wait.PollImmediate(2*time.Second, gobTenantTimeout, waitF)
}

func ensureGOBObservabilityTenant(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	nsID int,
) error {
	exists, err := checkGOBObservabilityTenantExists(logger, k8sClient, nsID)
	if err != nil {
		return fmt.Errorf(
			"unable to check if GitlabObservabilityTenant k8s object exists: %w", err)
	}

	if !exists {
		err := createGOBObservabilityTenant(logger, k8sClient, nsID)
		if err != nil {
			return fmt.Errorf("unable to create GitlabObservabilityTenant k8s object: %w", err)
		}
	}

	err = waitGOBObservabilityTenant(logger, k8sClient, nsID)
	if err != nil {
		return fmt.Errorf("error occured while waiting for GitlabObservabilityTenant to get ready: %w", err)
	}

	return nil
}

func checkGOBObservabilityTenantExists(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	nsID int,
) (bool, error) {
	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	key := client.ObjectKey{
		Name: fmt.Sprintf("tenant-%d", nsID),
	}

	obj := new(unstructured.Unstructured)
	obj.SetAPIVersion("opstrace.com/v1alpha1")
	obj.SetKind("GitLabObservabilityTenant")

	err := k8sClient.Get(ctx, key, obj)
	if err != nil {
		if kerrors.IsNotFound(err) {
			return false, nil
		}
		return false, fmt.Errorf("unable to fetch GitLabObservabilityTenant object: %w", err)
	}

	return true, nil
}

func createGOBObservabilityTenant(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	nsID int,
) error {
	manifest := fmt.Sprintf(gobObservabilityTenantTmpl, nsID, nsID)

	obj := new(unstructured.Unstructured)
	err := yaml.Unmarshal([]byte(manifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	return applyUnstructured(logger, k8sClient, obj)
}

func waitGOBObservabilityTenant(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	nsID int,
) error {
	var readyF = func(obj *unstructured.Unstructured) (bool, error) {
		conditions, err := conditionsFromUnstructured(obj, "status", "conditions")
		if err != nil {
			return false, err
		}

		ready, err := isReady(conditions)
		if err != nil {
			if errors.Is(err, ErrNoStatusCondition) {
				logger.Info("GitLabObservabilityTenant object does not have any conditions yet")
				return false, nil
			}
			return false, err
		}

		if !ready {
			logger.Info("waiting for GitLabObservabilityTenant object to become ready")
			return false, nil
		}
		logger.Info("GitLabObservabilityTenant object has Ready status")

		logger.Info("GitLabObservabilityTenant became ready!")
		return true, nil
	}

	var waitF wait.ConditionFunc = func() (bool, error) {
		// TODO(prozlach): make it cmdline param, value chosen arbitrary.
		ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelF()

		key := client.ObjectKey{
			Name: fmt.Sprintf("tenant-%d", nsID),
		}

		obj := new(unstructured.Unstructured)
		obj.SetAPIVersion("opstrace.com/v1alpha1")
		obj.SetKind("GitLabObservabilityTenant")

		err := k8sClient.Get(ctx, key, obj)
		if err != nil {
			return false, fmt.Errorf("unable to fetch GitLabObservabilityTenant object: %w", err)
		}

		return readyF(obj)
	}

	// Values selected arbitrarly
	return wait.PollImmediate(2*time.Second, gobTenantTimeout, waitF)
}

func ensureTenantOverrideSecret(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	gitlabNSID, gitlabProjectID int,
) error {
	manifest := fmt.Sprintf(gobLocalTenantData, gitlabNSID, gitlabProjectID)

	obj := new(unstructured.Unstructured)
	err := k8s_yaml.Unmarshal([]byte(manifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	return applyUnstructured(logger, k8sClient, obj)
}
