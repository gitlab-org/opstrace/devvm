package systemops

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
)

type NetworkInfo struct {
	GDKIP         string `json:"gdk_ip"`
	VIPRangeEnd   string `json:"vip_range_end"`
	VIPRangeStart string `json:"vip_range_start"`
}

func readNetworkInfo(
	logger *zap.SugaredLogger,
) (
	string, string, string, status.PartialStatus,
) {
	logger.Infof("retrieving network information from %s", constants.DefaultNetworkInfoFilePath)
	b, err := os.ReadFile(constants.DefaultNetworkInfoFilePath)
	if err != nil {
		res := status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to read networking info file %s: %v",
					constants.DefaultNetworkInfoFilePath, err,
				),
			},
		}
		return "", "", "", res
	}

	tmp := new(NetworkInfo)

	err = json.Unmarshal(b, tmp)
	if err != nil {
		res := status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to unmarschal network info: %v", err,
				),
			},
		}
		return "", "", "", res
	}

	res := status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			fmt.Sprintf("GDK IP: %s", tmp.GDKIP),
			fmt.Sprintf("K8s VIP range start: %s", tmp.VIPRangeStart),
			fmt.Sprintf("K8s VIP range end: %s", tmp.VIPRangeEnd),
		},
		Metadata: map[string]string{
			"gdk_ip": tmp.GDKIP,
		},
	}
	return tmp.GDKIP, tmp.VIPRangeStart, tmp.VIPRangeEnd, res
}
