package systemops

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"os"
	"time"

	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
	v1_core "k8s.io/api/core/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/util/wait"
	"sigs.k8s.io/controller-runtime/pkg/client"
	cr_client "sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"
)

const (
	gobClusterName = "dev-cluster"

	gobClusterReadyTimeout = 8 * time.Minute
)

const dbSecretTmpl = `apiVersion: v1
kind: Secret
metadata:
  name: opstrace-scheduler-clickhouse-credentials
  namespace: default
type: Opaque
data:
  http-endpoint: %s
  native-endpoint: %s
  password: %s
`

// NOTE(prozlach): We must not import here types from opstrace repo as this
// will lead to dependency hell. Shelling out to kubectl seems ugly and has its
// own issues, dynamic client plus untyped objects is the best approach.
const clusterManifestTmpl = `apiVersion: opstrace.com/v1alpha1
kind: Cluster
metadata:
  name: %s
spec:
  target: kind
  dns:
    domain: gob.devvm
    acmeEmail: ""
    dns01Challenge: {}
    externalDNSProvider: {}
  gitlab:
    instanceUrl: https://gdk.devvm:3443/
    oidcProviders:
      - https://gdk.devvm:3443
    authSecret:
      name: %s
  overrides:
    gatekeeper:
      patches:
        - target:
            kind: Deployment
            name: gatekeeper
          patch: '[{ "op": "add", "path": "/spec/template/spec/hostAliases", "value": [{"ip": "%s", "hostnames": ["gdk.devvm"]}]}]'
      replicas:
        - count: 1
          name: gatekeeper
    errorTrackingAPI:
      replicas:
        - count: 1
          name: errortracking-api
    provisioningAPI:
      replicas:
        - count: 1
          name: provisioning-api
    traceQueryAPI:
      patches:
        - patch: '[{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/value", "value": "debug" }, { "op": "add", "path": "/spec/template/spec/hostAliases", "value": [{"ip": "%s", "hostnames": ["gdk.devvm"]}]}]'
          target:
            kind: Deployment
            name: trace-query-api
      replicas:
        - count: 1
          name: trace-query-api
    queryAPI:
      patches:
        - patch: '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-log-level=debug" }, { "op": "add", "path": "/spec/template/spec/hostAliases", "value": [{"ip": "%s", "hostnames": ["gdk.devvm"]}]}]'
          target:
            kind: Deployment
            name: query-api
      replicas:
        - count: 1
          name: query-api
    otelCollector:
      patches:
        - target:
            kind: Deployment
            name: otel-collector
          patch: '[{ "op": "add", "path": "/spec/template/spec/hostAliases", "value": [{"ip": "%s", "hostnames": ["gdk.devvm"]}]}]'
      replicas:
        - count: 1
          name: otel-collector
`

// We have a chicken and egg problem because if the CRD changes in an opstrace/opstrace MR, we need to utilize the new
// CR here for the e2e tests to pass on that MR, while also supporting the old CR for all other devvms not created
// from that MR/branch.
const clusterManifestOldTmpl = `apiVersion: opstrace.com/v1alpha1
kind: Cluster
metadata:
  name: %s
spec:
  target: kind
  dns:
    domain: gob.devvm
    acmeEmail: ""
    dns01Challenge: {}
    externalDNSProvider: {}
  gitlab:
    instanceUrl: https://gdk.devvm:3443/
    authSecret:
      name: %s
  overrides:
    gatekeeper:
      patches:
        - target:
            kind: Deployment
            name: gatekeeper
          patch: '[{ "op": "add", "path": "/spec/template/spec/hostAliases", "value": [{"ip": "%s", "hostnames": ["gdk.devvm"]}]}]'
      replicas:
        - count: 1
          name: gatekeeper
    errorTrackingAPI:
      replicas:
        - count: 1
          name: errortracking-api
    provisioningAPI:
      replicas:
        - count: 1
          name: provisioning-api
    traceQueryAPI:
      patches:
        - patch: '[{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/value", "value": "debug" }]'
          target:
            kind: Deployment
            name: trace-query-api
      replicas:
        - count: 1
          name: trace-query-api
    queryAPI:
      patches:
        - patch: '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-log-level=debug" }]'
          target:
            kind: Deployment
            name: query-api
      replicas:
        - count: 1
          name: query-api
`

func ensureGOBCluster(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	gdkIP string,
	clickhouseUsername, clickhousePass string,
) status.PartialStatus {
	err := createDBSecret(logger, k8sClient, clickhouseUsername, clickhousePass, gdkIP)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure DB secret: %v", err,
				),
			},
		}
	}

	err = createClusterObject(logger, k8sClient, gdkIP)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to deploy GOB cluster: %v", err,
				),
			},
		}
	}

	err = waitClusterObjectReady(logger, k8sClient)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"GOB cluster has not became ready: %v", err,
				),
			},
		}
	}

	err = ensureK8sCertIsTrusted(logger, k8sClient)
	if err != nil {
		return status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to make K8s CA cert as trusted: %v", err,
				),
			},
		}
	}

	return status.PartialStatus{
		Succeded: status.StatusStageOK,
	}
}

func ensureK8sCertIsTrusted(
	logger *zap.SugaredLogger,
	k8sClient cr_client.Client,
) error {
	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	secret := new(v1_core.Secret)
	key := cr_client.ObjectKey{
		Name:      constants.GOBCACertSecretName,
		Namespace: "cert-manager",
	}

	err := k8sClient.Get(ctx, key, secret)
	if err != nil && !kerrors.IsNotFound(err) {
		return fmt.Errorf("unable to fetch GOB CA cert secret %s: %w", constants.GOBCACertSecretName, err)
	}

	caCert, ok := secret.Data["ca.crt"]
	if !ok || len(caCert) < 10 {
		return fmt.Errorf("GOB CA cert secret %s does not contain valid CA: %w", constants.GOBCACertSecretName, err)
	}

	err = os.WriteFile(constants.GOBCACertPath, caCert, 0644)
	if err != nil {
		return fmt.Errorf("failed to write CA cert to path %s: %w", constants.GOBCACertPath, err)
	}

	logger.Infof("updating system's trusted CA certificates store")
	_, exitStatus, err := executeCmd(
		logger, constants.GOBRepositoryPath,
		"update-ca-certificates", "--verbose", "--fresh",
	)
	if err != nil {
		return fmt.Errorf("failed to execute `update-ca-certificates` command: %w", err)
	}
	if exitStatus != 0 {
		return fmt.Errorf("`update-ca-certificates` returned non-zero exit status %d", exitStatus)
	}

	return nil
}

func waitClusterObjectReady(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
) error {
	var clusterReadyF = func(obj *unstructured.Unstructured) (bool, error) {
		conditions, err := conditionsFromUnstructured(obj, "status", "conditions")
		if err != nil {
			return false, err
		}

		ready, err := isReady(conditions)
		if err != nil {
			if errors.Is(err, ErrNoStatusCondition) {
				logger.Info("Cluster object does not have any conditions yet")
				return false, nil
			}
			return false, err
		}
		if ready {
			logger.Info("Cluster object became ready")
		} else {
			logger.Info("waiting for Cluster object to become ready")
		}

		return ready, nil
	}

	var waitF wait.ConditionFunc = func() (bool, error) {
		// TODO(prozlach): make it cmdline param, value chosen arbitrary.
		ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelF()

		key := client.ObjectKey{
			Name: gobClusterName,
		}

		obj := new(unstructured.Unstructured)
		obj.SetAPIVersion("opstrace.com/v1alpha1")
		obj.SetKind("Cluster")

		err := k8sClient.Get(ctx, key, obj)
		if err != nil {
			return false, fmt.Errorf("unable to fetch cluster object: %w", err)
		}

		return clusterReadyF(obj)
	}

	// Values selected arbitrarly
	return wait.PollImmediate(2*time.Second, gobClusterReadyTimeout, waitF)
}

func ClusterYAML(gdkIP string) string {
	return fmt.Sprintf(clusterManifestTmpl, gobClusterName, constants.GOBOauthSecretName, gdkIP, gdkIP, gdkIP, gdkIP)
}

func ClusterYAMLOld(gdkIP string) string {
	return fmt.Sprintf(clusterManifestOldTmpl, gobClusterName, constants.GOBOauthSecretName, gdkIP)
}

func createClusterObject(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	gdkIP string,
) error {
	manifest := ClusterYAML(gdkIP)

	obj := new(unstructured.Unstructured)
	err := yaml.Unmarshal([]byte(manifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	err = applyUnstructured(logger, k8sClient, obj)
	if err == nil {
		return nil
	}
	// We can land here if the new CR has fields that the old CRD does not support. I.e. CRD changes
	// have not landed in opstrace main yet.
	manifest = ClusterYAMLOld(gdkIP)
	obj = new(unstructured.Unstructured)
	err = yaml.Unmarshal([]byte(manifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	return applyUnstructured(logger, k8sClient, obj)
}

func createDBSecret(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	chUser, chPass, ip string,
) error {
	manifest := fmt.Sprintf(
		dbSecretTmpl,
		base64.StdEncoding.EncodeToString(
			[]byte(
				fmt.Sprintf("http://%s:%s@%s:8123", chUser, chPass, ip),
			),
		),
		base64.StdEncoding.EncodeToString(
			[]byte(
				fmt.Sprintf("tcp://%s:%s@%s:9000", chUser, chPass, ip),
			),
		),
		base64.StdEncoding.EncodeToString(
			[]byte(chPass),
		),
	)

	obj := new(unstructured.Unstructured)
	err := yaml.Unmarshal([]byte(manifest), &obj)
	if err != nil {
		return fmt.Errorf("failed to unmarshal object into unstructured.Unstructured: %w", err)
	}

	return applyUnstructured(logger, k8sClient, obj)
}
