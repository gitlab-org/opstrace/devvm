package systemops

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/railsconsole"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
	v1 "k8s.io/api/core/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ApppasswordCache struct {
	AppPassword string
}

const (
	gdkOAuthAppName     = "gitlab-observability-backend"
	gdkOAuthRedirectURL = ""
)

func ensureGDKOAuthCredentials(
	logger *zap.SugaredLogger,
	gitlabClient *gogitlab.Client,
	k8sClient client.Client,
	errortrackingToken string,
	rc *railsconsole.RailsConsole,
) (*gogitlab.Namespace, status.PartialStatus) {
	gitlabNS, err := getGitlabNs(logger, gitlabClient)
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to get information about Gitlab Namespace/Group to use: %v", err,
				),
			},
		}
	}

	appID, appSecret, err := ensureGLApp(logger, gitlabClient, rc)
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure Application in GDK: %v", err,
				),
			},
		}
	}

	pkcs1Key, err := ensureSecret(logger, k8sClient, appID, appSecret, errortrackingToken, gitlabNS.ID)
	if err != nil {
		return nil, status.PartialStatus{
			Succeded: status.StatusStageFailed,
			DetailedMessage: []string{
				fmt.Sprintf(
					"unable to ensure GOB auth credentials secret: %v", err,
				),
			},
		}
	}

	return gitlabNS, status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			fmt.Sprintf("Application ID: %s", appID),
			fmt.Sprintf("Application secret: %s", appSecret),
			fmt.Sprintf("Error tracking shared secret: %s", errortrackingToken),
			fmt.Sprintf(`RSA private key. Copy this into your GDK config/secrets.yml to connect to this GOB instance from a local GDK/GCK instance:
			
development:
	openid_connect_signing_key: |
		%s
`, pkcs1Key),
		},
	}
}

func ensureSecret(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	appID, appSecret, errortrackingToken string,
	gitlabNS int,
) (string, error) {
	// The check-then-update approach is intentional. We could in theory just
	// issue Patch() and just rely on k8s to either create or update the
	// secret, but:
	// * GOB does not really support reloading secrets gracefully yet
	// * once an object is created, we do not want to mess with it in case user
	//   changed it for some reason.

	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	// check if secret is there
	secret := new(v1.Secret)
	key := client.ObjectKey{
		Name:      constants.GOBOauthSecretName,
		Namespace: constants.GOBSchedulerNamespace,
	}

	err := k8sClient.Get(ctx, key, secret)
	if err != nil && !kerrors.IsNotFound(err) {
		return "", fmt.Errorf(
			"unable to fetch GOB oauth secret %s/%s: %w",
			constants.GOBSchedulerNamespace, constants.GOBOauthSecretName, err,
		)
	}
	if err == nil {
		logger.Info("Application secret already exists, skipping its creation")
		// In theory we should check for its presence, in practice it will be
		// always there.
		return secret.StringData["oidc_private_key_pem"], nil
	}
	cancelF()

	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	// Create RSA private key
	pkey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return "", fmt.Errorf("unable to generate RSA private key: %w", err)
	}
	pkcs1Key := string(pem.EncodeToMemory(&pem.Block{
		Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(pkey),
	}))

	secret = &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      constants.GOBOauthSecretName,
			Namespace: constants.GOBSchedulerNamespace,
		},
		StringData: map[string]string{
			"gitlab_oauth_client_id":     appID,
			"gitlab_oauth_client_secret": appSecret,
			"internal_endpoint_token":    errortrackingToken,
			"oidc_private_key_pem":       pkcs1Key,
			"gitlab_otel_endpoint":       fmt.Sprintf("http://otel-collector.tenant-%d.svc.cluster.local:4318/", gitlabNS),
			// Legacy entry, replaced by the one above. Once
			// https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/2436
			// gets merged, it can be savelly removed:
			"gitlab_traces_endpoint": fmt.Sprintf("http://otel-collector.tenant%d.svc.cluster.local:4318/v1/traces", gitlabNS),
		},
	}
	err = k8sClient.Create(ctx, secret)
	if err != nil {
		logger.Info("unable to create GOB oauth secret: %w", err)
		return "", err
	}

	return pkcs1Key, nil
}

func ensureGLApp(
	logger *zap.SugaredLogger,
	client *gogitlab.Client,
	rc *railsconsole.RailsConsole,
) (string, string, error) {
	apps, _, err := client.Applications.ListApplications(&gitlab.ListApplicationsOptions{})
	if err != nil {
		return "", "", fmt.Errorf("unable to list instance-wide Applications in GDK: %w", err)
	}

	foundAppsCount := 0
	var id int
	var appID string
	var appSecret string
	for _, app := range apps {
		if app.ApplicationName == gdkOAuthAppName {
			foundAppsCount++
			id = app.ID
			appID = app.ApplicationID
			// Secret is unavailable through API
		}
	}

	if foundAppsCount > 1 {
		return "", "", fmt.Errorf("found more than one Application with name %s", gdkOAuthAppName)
	}

	if foundAppsCount == 0 {
		logger.Infof("creating instance-wide Application %q", gdkOAuthAppName)
		// TODO(prozlach): There is probably a simpler way to do it using
		// Rails-console only, but this requires some more research. I have not
		// found any good example on the net with how to indepontently create
		// APP using console.
		app, _, err := client.Applications.CreateApplication(
			&gitlab.CreateApplicationOptions{
				Name:         strPtr(gdkOAuthAppName),
				Scopes:       strPtr("api"),
				Confidential: boolPtr(true),
				RedirectURI:  strPtr("https://gob.devvm/v1/auth/callback"),
			},
		)
		if err != nil {
			return "", "", fmt.Errorf("failed to create instance-wide oauth application: %w", err)
		}
		id = app.ID
		appID = app.ApplicationID
		appSecret = app.Secret

		err = saveCachedAppPassword(logger, constants.DefaultApppasswordCacheFilePath, appSecret)
		if err != nil {
			return "", "", fmt.Errorf("unable to save apppassword in cache: %w", err)
		}
	} else {
		appSecret, err = loadCachedAppPassword(logger, constants.DefaultApppasswordCacheFilePath)
		if err != nil {
			return "", "", fmt.Errorf("unable to load apppassword from cache: %w", err)
		}
	}

	// Another thing that we can't do using API - set the app as trusted:
	cmd := fmt.Sprintf("app = ApplicationsFinder.new(id: %d).execute; app.trusted=true; app.save!", id)
	output, err := rc.PushCommand(cmd)
	if err != nil {
		return "", "", fmt.Errorf("unable to mark Application %s as trusted: %w", gdkOAuthAppName, err)
	}
	if !strings.Contains(output, "=> true") {
		return "", "", errors.New("gdk command was not successfull, please check booter logs for more details")
	}

	return appID, appSecret, nil
}

func loadCachedAppPassword(logger *zap.SugaredLogger, path string) (string, error) {
	logger.Infof("retrieving cached apppassword from %s", path)
	b, err := os.ReadFile(path)
	if err != nil {
		return "", fmt.Errorf("unable to read cache file %s: %w", path, err)
	}

	tmp := new(ApppasswordCache)

	err = json.Unmarshal(b, tmp)
	if err != nil {
		return "", fmt.Errorf("unable to unmarschal cache: %w", err)
	}
	return tmp.AppPassword, nil
}

func saveCachedAppPassword(logger *zap.SugaredLogger, path, appPassword string) error {
	logger.Infof("storing apppassword data in %s", path)

	tmp := ApppasswordCache{
		AppPassword: appPassword,
	}

	b, err := json.Marshal(tmp)
	if err != nil {
		return fmt.Errorf("unable to marschal cache: %w", err)
	}

	err = os.WriteFile(path, b, 0644)
	if err != nil {
		return fmt.Errorf("unable to write marschaled cache to %s: %w", path, err)
	}
	return nil
}
