package systemops

import (
	"bytes"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os/exec"
	"strings"
	"sync"
	"time"

	"github.com/egymgmbh/go-prefix-writer/prefixer"
	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/railsconsole"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
	"go.uber.org/zap/zapio"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	deploymentutil "k8s.io/kubectl/pkg/util/deployment"
	"sigs.k8s.io/controller-runtime/pkg/client"
	cr_client "sigs.k8s.io/controller-runtime/pkg/client"
)

type taskT struct {
	name string
	f    func() status.PartialStatus
}

func Do(logger *zap.SugaredLogger) error {
	logger.Info("Ensuring this VM is bootstraped.")
	st := status.New(logger, constants.StatusFilePath)

	rand.Seed(time.Now().UnixNano())

	var res status.PartialStatus

	var gdkIP string
	var vipMinAddress string
	var vipMaxAddress string
	var gobCommitID string
	var gitLabCommitID string
	var demoAppEnabled bool
	var gitlabClient *gogitlab.Client
	var errortrackingToken string
	var rc *railsconsole.RailsConsole
	var k8sClient cr_client.Client
	var gitlabNS *gogitlab.Namespace
	var clickhouseUsername string
	var clickhousePass string

	runConcurently(
		logger, st,
		taskT{
			constants.NetworkInfoBaseName,
			func() status.PartialStatus {
				gdkIP, vipMinAddress, vipMaxAddress, res = readNetworkInfo(logger)
				return res
			},
		},
		taskT{
			constants.WireguardVPNStageName,
			func() status.PartialStatus {
				return ensureWireguardUI(logger)
			},
		},
		taskT{
			constants.VMlabelsStatusName,
			func() status.PartialStatus {
				gobCommitID, gitLabCommitID, demoAppEnabled, res = inspectVMLabels(logger)
				return res
			},
		},
	)

	retryUntillSuccesful(logger, constants.GobprepStatusName, st, func() status.PartialStatus {
		return prepareGOBRepo(logger, gobCommitID)
	})

	runConcurently(
		logger, st,
		taskT{
			constants.GdkStatusName,
			func() status.PartialStatus {
				gitlabClient, errortrackingToken, rc, res = ensureGDKRunning(logger, gitLabCommitID)
				return res
			},
		},
		taskT{
			constants.GobK8sBaseName,
			func() status.PartialStatus {
				k8sClient, res = ensureGOBK8sBase(logger)
				return res
			},
		},
		taskT{
			constants.ClickhouseStatusName,
			func() status.PartialStatus {
				clickhouseUsername, clickhousePass, res = ensureClickhouse(logger, gdkIP)
				return res
			},
		},
	)

	retryUntillSuccesful(logger, constants.GdkOAuthBaseName, st, func() status.PartialStatus {
		gitlabNS, res = ensureGDKOAuthCredentials(
			logger, gitlabClient, k8sClient, errortrackingToken, rc)
		return res
	})

	runConcurently(
		logger, st,
		taskT{
			constants.GobClusterBaseName,
			func() status.PartialStatus {
				return ensureGOBCluster(logger, k8sClient, gdkIP, clickhouseUsername, clickhousePass)
			},
		},
		taskT{
			constants.AuxManifestsBaseName,
			func() status.PartialStatus {
				return ensureAuxManifests(logger, k8sClient, vipMinAddress, vipMaxAddress)
			},
		},
	)

	var glProject int
	retryUntillSuccesful(logger, constants.GobTenantBaseName, st, func() status.PartialStatus {
		glProject, res = ensureGOBTenant(logger, gitlabClient, k8sClient, gitlabNS)
		return res
	})

	if demoAppEnabled {
		retryUntillSuccesful(logger, constants.DemoManifestsBaseName, st, func() status.PartialStatus {
			return ensureDemoManifests(logger, gitlabNS.ID, glProject)
		})
	} else {
		logger.Infof("Demo app is not enabled, skipping deployment")
	}

	st.MarkAllDone()

	return nil
}

func runConcurently(logger *zap.SugaredLogger, st *status.Status, tasks ...taskT) {
	wg := new(sync.WaitGroup)

	for _, task := range tasks {
		task := task

		wg.Add(1)

		logger.Infow("starting task", "name", task.name)
		go func() {
			defer wg.Done()

			retryUntillSuccesful(logger, task.name, st, task.f)
			logger.Infow("task done", "name", task.name)
		}()
	}

	wg.Wait()
}

func retryUntillSuccesful(
	logger *zap.SugaredLogger,
	name string,
	st *status.Status,
	f func() status.PartialStatus,
) {
	tStart := time.Now()
	nTries := -1

	initialRes := status.PartialStatus{
		Name:     name,
		Succeded: status.StatusStageInProgress,
		Metadata: make(map[string]string),
	}
	st.Upsert(initialRes)

	for {
		res := f()

		nTries++
		tDuration := time.Now().Sub(tStart)

		res.Name = name
		res.RetriesCount = nTries
		res.RunDuration = tDuration
		st.Upsert(res)

		if res.Succeded == status.StatusStageOK {
			break
		}

		logger.Errorf("stage %q in state %q: %s", name, res.Succeded, strings.Join(res.DetailedMessage, ";"))

		// NOTE(prozlach): dumb sleep to prevent tight retry-loops. NOt sure if
		// something smarter is really needed.
		time.Sleep(time.Second)
	}
}

func executeCmd(logger *zap.SugaredLogger, cwd string, cmdStr ...string) (string, int, error) {
	debugwriter := &zapio.Writer{
		Log:   logger.Desugar(),
		Level: zap.DebugLevel,
	}
	defer debugwriter.Close()

	prefixedWriter := prefixer.New(debugwriter, func() string { return "CMD_OUT | " })

	buf := new(bytes.Buffer)
	outWriter := io.MultiWriter(buf, prefixedWriter)

	cmd := exec.Command(cmdStr[0], cmdStr[1:]...)
	cmd.Stdout = outWriter
	cmd.Stderr = outWriter
	if cwd != "" {
		cmd.Dir = cwd
	}

	err := cmd.Run()

	if err != nil {
		if exitErr, ok := err.(*exec.ExitError); ok {
			return buf.String(), exitErr.ExitCode(), nil
		} else {
			return "", -1, fmt.Errorf("failed to run command %q: %w\n", cmdStr, err)
		}
	}
	return buf.String(), 0, nil
}

// executeCmdWithAsdf is an opinionated version of executeCmd()
func executeCmdWithAsdf(
	logger *zap.SugaredLogger,
	cwd string,
	cmd string,
	msg string,
) (string, error) {
	cmd = fmt.Sprintf(". /home/dev/.asdf/asdf.sh; %s", cmd)
	return executeCmdAsDevWithBash(logger, cwd, cmd, msg)
}

// executeCmdAsDevWithBash is an opinionated version of executeCmd()
func executeCmdAsDevWithBash(
	logger *zap.SugaredLogger,
	cwd string,
	cmd string,
	msg string,
) (string, error) {
	logger.Infof(msg)

	out, exitStatus, err := executeCmd(
		logger, cwd,
		"sudo", "-u", "dev", "bash", "-c", cmd,
	)
	if err != nil {
		return "", fmt.Errorf("failed to execute `%s`: %w", cmd, err)
	}
	if exitStatus != 0 {
		return "", fmt.Errorf("`%s` returned non-zero exit status %d", cmd, exitStatus)
	}

	return out, nil
}

func httpGetReq(
	ctx context.Context,
	logger *zap.SugaredLogger,
	url string,
	headers map[string]string,
	insecureSkipVerify bool,
) ([]byte, int, error) {
	logger.Debugf("fetching url %s", url)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, 0, fmt.Errorf("failed to create new http req for URL %q: %w", url, err)
	}

	for k, v := range headers {
		req.Header.Set(k, v)
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: insecureSkipVerify,
		},
	}
	client := &http.Client{Transport: tr}

	res, err := client.Do(req)
	if err != nil {
		return nil, 0, fmt.Errorf("failed to execute http req for URL %q: %w", url, err)
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, 0, fmt.Errorf("unable to read response body when fetching URL %q: %w", url, err)
	}
	res.Body.Close()

	return body, res.StatusCode, nil
}

func strPtr(in string) *string {
	return &in
}

func boolPtr(in bool) *bool {
	return &in
}

func conditionsFromUnstructured(from *unstructured.Unstructured, fields ...string) ([]metav1.Condition, error) {
	rawConditions, _, err := unstructured.NestedSlice(from.Object, fields...)
	if err != nil {
		return nil, fmt.Errorf("failed to extract conditions from unstructured object: %w", err)
	}

	conditions := make([]metav1.Condition, len(rawConditions))

	for i := range rawConditions {
		c := &metav1.Condition{}
		err = runtime.DefaultUnstructuredConverter.FromUnstructured(rawConditions[i].(map[string]interface{}), c)
		if err != nil {
			return nil, fmt.Errorf("unable to convert condition from unstructured: %w", err)
		}
		conditions[i] = *c
	}

	return conditions, nil
}

var ErrNoStatusCondition error = errors.New("`Ready` condition has not been found")

func isReady(conditions []metav1.Condition) (bool, error) {
	hasReadyStatus := false
	for _, condition := range conditions {
		if condition.Type == "Ready" {
			hasReadyStatus = true
			if condition.Status == "True" {
				return true, nil
			}
		}
	}
	if hasReadyStatus {
		return false, nil
	}
	return false, ErrNoStatusCondition
}

func applyUnstructured(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	obj *unstructured.Unstructured,
) error {
	logger.Debugw("Ensuring k8s object",
		"apiVersion", obj.GetAPIVersion(),
		"kind", obj.GetKind(),
		"name", obj.GetName(),
		"namespace", obj.GetNamespace(),
	)

	// TODO(prozlach): make it cmdline param, value chosen arbitrary.
	ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelF()

	err := k8sClient.Patch(
		ctx,
		obj,
		client.Apply,
		client.ForceOwnership,
		client.FieldOwner(constants.FieldManagerIDString),
	)
	if err != nil {
		return fmt.Errorf("failed to ensure object: %w", err)
	}

	logger.Infow("K8s object ensured",
		"apiVersion", obj.GetAPIVersion(),
		"kind", obj.GetKind(),
		"name", obj.GetName(),
		"namespace", obj.GetNamespace(),
	)

	return nil
}

func waitDeploymentReady(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	namespace, name string,
	interval, timeout time.Duration,
) error {
	deploymentReadyF := func(deployment *appsv1.Deployment) bool {
		if deployment.Status.ObservedGeneration < deployment.Generation {
			logger.Infof(
				"Deployment's %q observed generation has not caught up yet",
				deployment.Name,
			)
			return false
		}

		cond := deploymentutil.GetDeploymentCondition(deployment.Status, appsv1.DeploymentProgressing)
		if cond != nil && cond.Reason == deploymentutil.TimedOutReason {
			logger.Infof("deployment %q exceeded its progress deadline", deployment.Name)
			return false
		}
		if deployment.Spec.Replicas != nil && deployment.Status.UpdatedReplicas < *deployment.Spec.Replicas {
			logger.Infof(
				"Waiting for deployment %q: %d out of %d new replicas have been updated",
				deployment.Name, deployment.Status.UpdatedReplicas, *deployment.Spec.Replicas,
			)
			return false
		}
		if deployment.Status.Replicas > deployment.Status.UpdatedReplicas {
			logger.Infof(
				"Waiting for deployment %q: %d old replicas are pending termination",
				deployment.Name, deployment.Status.Replicas-deployment.Status.UpdatedReplicas,
			)
			return false
		}
		if deployment.Status.AvailableReplicas < deployment.Status.UpdatedReplicas {
			logger.Infof(
				"Waiting for deployment %q: %d of %d updated replicas are available",
				deployment.Name, deployment.Status.AvailableReplicas, deployment.Status.UpdatedReplicas,
			)
			return false
		}

		logger.Infof("deployment %q is up!", deployment.Name)
		return true
	}

	var waitF wait.ConditionFunc = func() (bool, error) {
		// TODO(prozlach): make it cmdline param, value chosen arbitrary.
		ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelF()

		deployment := new(appsv1.Deployment)
		key := client.ObjectKey{
			Name:      name,
			Namespace: namespace,
		}

		err := k8sClient.Get(ctx, key, deployment)
		if err != nil {
			return false, fmt.Errorf("unable to fetch deployment: %w", err)
		}

		return deploymentReadyF(deployment), nil
	}

	// Values selected arbitrarly
	return wait.PollImmediate(interval, timeout, waitF)
}

func waitDaemonsetReady(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	namespace, name string,
	interval, timeout time.Duration,
) error {
	daemonsetReadyF := func(daemonset *appsv1.DaemonSet) bool {
		if daemonset.Status.ObservedGeneration < daemonset.Generation {
			logger.Infof(
				"daemonset's %q observed generation has not caught up yet",
				daemonset.Name,
			)
			return false
		}

		if daemonset.Status.DesiredNumberScheduled == 0 ||
			daemonset.Status.DesiredNumberScheduled != daemonset.Status.NumberReady {
			logger.Infof("DaemonSet %v not ready yet", daemonset.Name)
			return false
		}

		logger.Infof("daemonset %q is up!", daemonset.Name)
		return true
	}

	var waitF wait.ConditionFunc = func() (bool, error) {
		// TODO(prozlach): make it cmdline param, value chosen arbitrary.
		ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelF()

		daemonset := new(appsv1.DaemonSet)
		key := client.ObjectKey{
			Name:      name,
			Namespace: namespace,
		}

		err := k8sClient.Get(ctx, key, daemonset)
		if err != nil {
			return false, fmt.Errorf("unable to fetch daemonset: %w", err)
		}

		return daemonsetReadyF(daemonset), nil
	}

	// Values selected arbitrarly
	return wait.PollImmediate(interval, timeout, waitF)
}

func waitLBReady(
	logger *zap.SugaredLogger,
	k8sClient client.Client,
	namespace, name, ip string,
	interval, timeout time.Duration,
) error {
	lbReadyF := func(lb *v1.Service) bool {
		if len(lb.Status.LoadBalancer.Ingress) == 0 {
			logger.Infof("lb %v does not have VIPs assigned yet", lb.Name)
			return false
		}
		for _, ingress := range lb.Status.LoadBalancer.Ingress {
			if ingress.IP == ip {
				logger.Infof("lb %q has IP assigned!", lb.Name)
				return true
			}
		}

		logger.Infof("lb %q does not have IP assigned yet", lb.Name)
		return false
	}

	var waitF wait.ConditionFunc = func() (bool, error) {
		// TODO(prozlach): make it cmdline param, value chosen arbitrary.
		ctx, cancelF := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelF()

		lb := new(v1.Service)
		key := client.ObjectKey{
			Name:      name,
			Namespace: namespace,
		}

		err := k8sClient.Get(ctx, key, lb)
		if err != nil {
			return false, fmt.Errorf("unable to fetch lb: %w", err)
		}

		return lbReadyF(lb), nil
	}

	// Values selected arbitrarly
	return wait.PollImmediate(interval, timeout, waitF)
}

func checkoutRepo(logger *zap.SugaredLogger, commitID string, repoPath string) error {
	logger.Infof("checking out %s at revision %s", repoPath, commitID)

	_, exitStatus, err := executeCmd(
		logger, repoPath,
		"sudo", "-u", "dev", "git", "fetch",
	)
	if err != nil {
		return fmt.Errorf("failed to execute `git fetch`: %w", err)
	}
	if exitStatus != 0 {
		return fmt.Errorf("`git fetch` returned non-zero exit status %d", exitStatus)
	}

	_, exitStatus, err = executeCmd(
		logger, repoPath,
		"sudo", "-u", "dev", "git", "checkout", commitID,
	)
	if err != nil {
		return fmt.Errorf("failed to execute `git checkout`: %w", err)
	}
	if exitStatus != 0 {
		return fmt.Errorf("`git checkout` returned non-zero exit status %d", exitStatus)
	}
	return nil
}
