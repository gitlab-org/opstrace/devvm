package systemops

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/status"
	"go.uber.org/zap"
)

const (
	// Maximum amount of time all operations in this stage may take
	// TODO(prozlach): add CLI param that defines HTTP req. timeout, use it
	// instead
	// Value chosen arbitrary.
	defaultInspectionTimeout = 15 * time.Second

	vmLabelGOBBranchName    = "gob_branch"
	vmLabelGitLabBranchName = "gitlab_branch"
	vmLabelDemoAppName      = "demo_app"

	// Go to project page of the GOB/GitLab repos on gitlab.com to verify/update these:
	GOBProjectID    = 32149347
	GitLabProjectID = 278964
)

// GOBContainerIDs is a poor man's way to have a map-constant
func GOBContainerIDs() map[string]int {
	// Updated/obtained via:
	// curl https://gitlab.com/api/v4/projects/32149347/registry/repositories | jq '.[] | {name,id}'
	return map[string]int{
		"clickhouse-operator": 3001665,
		"scheduler":           3096456,
		"tenant-operator":     3096593,
		"gatekeeper":          3105190,
		"tracing-api":         3108163,
		"errortracking-api":   3143760,
	}
}

var ErrDockerImageMissing error = errors.New("Docker image with given tag has not been found")

func toGOBImageTag(commitID string) string {
	return fmt.Sprintf("0.3.0-%s", commitID[:8])
}

type VMLabelsCache struct {
	GOBCommitID    string
	GOBImageTag    string
	GitLabCommitID string
	DemoAppEnable  bool
}

// inspectVMLabels inspects VM labels to determine:
// * GOB commit to checkout
// * GitLab commit to checkout
// It enforces that the aforementioned commits already have docker images built (GOB only)
// and caches the commits in a local file so that if the booter is re-run, it
// always uses the same checkout version even if user pushed new commits in the
// meantime. Handling the update of branch for already bootstraped devvm is
// doable, but not trivial so we do not support it for the moment.
func inspectVMLabels(logger *zap.SugaredLogger) (string, string, bool, status.PartialStatus) {
	gobCommitID, gobImageTag, gitLabCommitID, demoAppEnabled, err := loadCachedValues(logger, constants.DefaultLabelsCacheFilePath)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return "", "", false, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"unable to retrive cache file: %v", err,
					),
				},
			}
		}

		// cache file does not exist, gather the data

		ctx, cancelF := context.WithTimeout(context.Background(), defaultInspectionTimeout)
		defer cancelF()

		gobBranch, err := getGOBBranchName(ctx, logger)
		if err != nil {
			return "", "", false, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"unable to fetch GOB branch name using GCE metadata service: %v", err,
					),
				},
			}
		}

		gitLabBranch, err := getGitLabBranchName(ctx, logger)
		if err != nil {
			return "", "", false, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"unable to fetch GitLab branch name using GCE metadata service: %v", err,
					),
				},
			}
		}

		demoAppEnabled, err = getDemoAppEnable(ctx, logger)
		if err != nil {
			return "", "", false, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"unable to fetch demoApp enablement status using GCE metadata service: %v", err,
					),
				},
			}
		}

		gobParts := strings.Split(gobBranch, ":")
		switch len(gobParts) {
		case 1:
			// plain name of the git branch:
			gobCommitID, err = getGOBCommitIDs(ctx, logger, gobBranch)
			if err != nil {
				return "", "", false, status.PartialStatus{
					Succeded: status.StatusStageFailed,
					DetailedMessage: []string{
						fmt.Sprintf(
							"unable to translate branch names to respective commit IDs: %v", err,
						),
					},
				}
			}
			gobImageTag = toGOBImageTag(gobCommitID)
		case 2:
			// git tag/released version, the tag is also used as a commit
			// ID
			gobImageTag = gobParts[1]
			gobCommitID = gobParts[1]
		case 4:
			// plain commit
			gobImageTag = gobParts[1]
			gobCommitID = gobParts[3]
		default:
			return "", "", false, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"the branch name %q is malformed and can't be decoded", gobBranch,
					),
				},
			}
		}

		gitLabParts := strings.Split(gitLabBranch, ":")
		switch len(gitLabParts) {
		case 1:
			// plain name of the git branch:
			gitLabCommitID, err = getGitLabCommitIDs(ctx, logger, gitLabBranch)
			if err != nil {
				return "", "", false, status.PartialStatus{
					Succeded: status.StatusStageFailed,
					DetailedMessage: []string{
						fmt.Sprintf(
							"unable to translate branch names to respective commit IDs: %v", err,
						),
					},
				}
			}
		case 2:
			gitLabCommitID = gitLabParts[1]
		case 4:
			// plain commit
			gitLabCommitID = gitLabParts[3]
		default:
			return "", "", false, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"the branch name %q is malformed and can't be decoded", gobBranch,
					),
				},
			}
		}

		err = verifyDockerImages(ctx, logger, gobImageTag)
		if err != nil {
			return "", "", false, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"docker images verification failed: %v", err,
					),
				},
			}
		}

		err = saveCachedValues(
			logger, constants.DefaultLabelsCacheFilePath, gobCommitID, gobImageTag, gitLabCommitID, demoAppEnabled)
		if err != nil {
			return "", "", false, status.PartialStatus{
				Succeded: status.StatusStageFailed,
				DetailedMessage: []string{
					fmt.Sprintf(
						"unable to save vm labels cache: %v", err,
					),
				},
			}
		}
	}

	return gobCommitID, gitLabCommitID, demoAppEnabled, status.PartialStatus{
		Succeded: status.StatusStageOK,
		DetailedMessage: []string{
			fmt.Sprintf("GitLab commit ID: %s", gitLabCommitID),
			fmt.Sprintf("GOB commit ID: %s", gobCommitID),
			fmt.Sprintf("GOB images tag: %s", gobImageTag),
			fmt.Sprintf("Opentelemetry-demo enable: %v", demoAppEnabled),
			"ATTENTION: Subsequent pushes to GOB or GitLab branches will be ignored.",
			// NOTE(prozlach): There are ways around it but they require some
			// k8s-foo, we want things super-simple instead. Those who know how
			// to update image references will not be using the devvm in the
			// first place.
			"ATTENTION: Please re-create the dev vm if you would like to update the references.",
		},
	}
}

func loadCachedValues(logger *zap.SugaredLogger, path string) (string, string, string, bool, error) {
	logger.Infof("retrieving cached vm labels data from %s", path)
	b, err := os.ReadFile(path)
	if err != nil {
		return "", "", "", false, fmt.Errorf("unable to read cache file %s: %w", path, err)
	}

	tmp := new(VMLabelsCache)

	err = json.Unmarshal(b, tmp)
	if err != nil {
		return "", "", "", false, fmt.Errorf("unable to unmarschal cache: %w", err)
	}
	return tmp.GOBCommitID, tmp.GOBImageTag, tmp.GitLabCommitID, tmp.DemoAppEnable, nil
}

func saveCachedValues(
	logger *zap.SugaredLogger,
	path, gobCommitID, gobImageTag, gitLabCommitID string,
	demoAppEnabled bool,
) error {
	logger.Infof("storing vm labels data in %s", path)

	tmp := VMLabelsCache{
		GOBImageTag:    gobImageTag,
		GOBCommitID:    gobCommitID,
		GitLabCommitID: gitLabCommitID,
		DemoAppEnable:  demoAppEnabled,
	}

	b, err := json.Marshal(tmp)
	if err != nil {
		return fmt.Errorf("unable to marschal cache: %w", err)
	}

	err = os.WriteFile(path, b, 0644)
	if err != nil {
		return fmt.Errorf("unable to write marschaled cache to %s: %w", path, err)
	}
	return nil
}

func verifyDockerImages(ctx context.Context, logger *zap.SugaredLogger, gobImageTag string) error {
	for k, v := range GOBContainerIDs() {
		logger.Debugf("checking GOB %s docker image with tag %s", k, gobImageTag)
		err := verifyDockerImage(ctx, logger, GOBProjectID, v, gobImageTag)
		if err != nil {
			if errors.Is(err, ErrDockerImageMissing) {
				return fmt.Errorf("GOB %s docker image with tag %s does not exist in the repo", k, gobImageTag)
			}
			return fmt.Errorf("failed to fetch information about GOB %s docker image: %w", k, err)
		}
		logger.Infof("GOB %s docker image with tag %s is present in the registry", k, gobImageTag)
	}

	return nil
}

func verifyDockerImage(
	ctx context.Context,
	logger *zap.SugaredLogger,
	projectID, containerID int,
	tag string,
) error {
	url := fmt.Sprintf(
		"https://gitlab.com/api/v4/projects/%d/registry/repositories/%d/tags/%s",
		projectID, containerID, tag,
	)
	_, status, err := httpGetReq(ctx, logger, url, nil, false)
	if err != nil {
		return fmt.Errorf("http GET request failed: %w", err)
	}
	if status == http.StatusOK {
		return nil
	}
	if status != http.StatusNotFound {
		return fmt.Errorf("http GET request returned unexpected status %d", status)
	}
	return ErrDockerImageMissing
}

func getGOBBranchName(ctx context.Context, logger *zap.SugaredLogger) (string, error) {
	gobBranchName, err := getLabelByName(ctx, logger, vmLabelGOBBranchName)
	if err != nil {
		return "", fmt.Errorf("unable to fetch GCE metadata for GOB branch name: %w", err)
	}
	logger.Infof("gob branch name: %s", gobBranchName)

	return gobBranchName, nil
}

func getGitLabBranchName(ctx context.Context, logger *zap.SugaredLogger) (string, error) {
	gitLabBranchName, err := getLabelByName(ctx, logger, vmLabelGitLabBranchName)
	if err != nil {
		return "", fmt.Errorf("unable to fetch GCE metadata for GitLab branch name: %w", err)
	}
	logger.Infof("gitLab branch name: %s", gitLabBranchName)

	return gitLabBranchName, nil
}

func getDemoAppEnable(ctx context.Context, logger *zap.SugaredLogger) (bool, error) {
	tmp, err := getLabelByName(ctx, logger, vmLabelDemoAppName)
	if err != nil {
		return false, fmt.Errorf("unable to fetch GCE metadata for demo-app enablement: %w", err)
	}
	logger.Infof("demo app enable: %s", tmp)

	demoAppEnabled := false
	if tmp == "true" {
		demoAppEnabled = true
	}

	return demoAppEnabled, nil
}

func getLabelByName(
	ctx context.Context,
	logger *zap.SugaredLogger,
	labelName string,
) (string, error) {
	gceHeaders := map[string]string{
		"Metadata-Flavor": "Google",
	}

	logger.Debugf("fetching label %s contents from VMs metadata", labelName)
	url := fmt.Sprintf("http://metadata.google.internal/computeMetadata/v1/instance/attributes/%s", labelName)
	resp, status, err := httpGetReq(ctx, logger, url, gceHeaders, false)
	if err != nil {
		return "", fmt.Errorf("http GET request failed: %w", err)
	}
	if status != http.StatusOK {
		return "", fmt.Errorf("http GET request returned status %d", status)
	}
	return string(resp), nil
}

func getGOBCommitIDs(
	ctx context.Context, logger *zap.SugaredLogger, gobBranchName string,
) (string, error) {
	gobCommitID, err := getCommitID(ctx, logger, GOBProjectID, gobBranchName)
	if err != nil {
		return "", fmt.Errorf("unable to fetch commit ID for gob branch %s: %w", gobBranchName, err)
	}
	logger.Infof("GOB commit ID: %s", gobCommitID)

	return gobCommitID, nil
}

func getGitLabCommitIDs(
	ctx context.Context, logger *zap.SugaredLogger, gitLabBranchName string,
) (string, error) {
	gitLabCommitID, err := getCommitID(ctx, logger, GitLabProjectID, gitLabBranchName)
	if err != nil {
		return "", fmt.Errorf("unable to fetch commit ID for gitLab branch %s: %w", gitLabBranchName, err)
	}
	logger.Infof("GitLab commit ID: %s", gitLabCommitID)

	return gitLabCommitID, nil
}

func getCommitID(
	ctx context.Context,
	logger *zap.SugaredLogger,
	projectID int,
	branchName string,
) (string, error) {
	url := fmt.Sprintf(
		"https://gitlab.com/api/v4/projects/%d/repository/branches/%s",
		projectID, url.QueryEscape(branchName),
	)

	resp, status, err := httpGetReq(ctx, logger, url, nil, false)
	if err != nil {
		return "", fmt.Errorf("http GET request failed: %w", err)
	}
	if status != http.StatusOK {
		return "", fmt.Errorf("http GET request returned status %d", status)
	}

	tmp := struct {
		Commit struct {
			ID string `json:"id"`
		} `json:"commit"`
	}{}

	err = json.Unmarshal(resp, &tmp)
	if err != nil {
		return "", fmt.Errorf("unable to unmarschal response body: %w", err)
	}

	return tmp.Commit.ID, nil
}
