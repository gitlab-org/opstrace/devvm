package railsconsole

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"strings"
	"syscall"
	"time"

	"github.com/egymgmbh/go-prefix-writer/prefixer"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"go.uber.org/zap"
	"go.uber.org/zap/zapio"
)

type RailsConsole struct {
	*exec.Cmd

	stdinPipe   io.WriteCloser
	debugWriter *zapio.Writer
	logger      *zap.SugaredLogger

	buf *bytes.Buffer
}

func New(logger *zap.SugaredLogger) (*RailsConsole, error) {
	var err error

	cmd := exec.Command(
		"sudo", "-u", "dev",
		"bash", "-c",
		". /home/dev/.asdf/asdf.sh; gdk rails console",
	)
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Setpgid: true,
	}
	cmd.Dir = constants.GDKRepositoryPath

	rc := &RailsConsole{
		Cmd: cmd,
		debugWriter: &zapio.Writer{
			Log:   logger.Desugar(),
			Level: zap.DebugLevel,
		},
		logger: logger,
		buf:    new(bytes.Buffer),
	}
	defer rc.buf.Reset()

	rc.stdinPipe, err = rc.StdinPipe()
	if err != nil {
		return nil, fmt.Errorf("unable to create stdin pipe: %w", err)
	}

	prefixedWriter := prefixer.New(rc.debugWriter, func() string { return "RAILS_CONSOLE_OUT | " })

	outWriter := io.MultiWriter(rc.buf, prefixedWriter)

	rc.Stdout = outWriter
	rc.Stderr = outWriter

	err = cmd.Start()
	if err != nil {
		return nil, fmt.Errorf("failed to start rails console process: %w", err)
	}

	// Wait for rails console to come up:
	started := false
	tStart := time.Now()
	tDeadline := tStart.Add(time.Minute)
	for time.Now().Before(tDeadline) {
		time.Sleep(1 * time.Second)
		bufContents := rc.buf.String()
		if strings.Contains(bufContents, "pry(main)") {
			started = true
			break
		}
	}
	if !started {
		if err := rc.Process.Kill(); err != nil {
			logger.Error("failed to kill process: %w", err)
		}
		return nil, errors.New("rails console did not start in time")
	}

	logger.Infof("rails console started in %s", time.Since(tStart).String())
	return rc, nil
}

func (c *RailsConsole) PushCommand(commandString string) (string, error) {
	defer c.buf.Reset()

	_, err := fmt.Fprintln(c.stdinPipe, commandString)
	if err != nil {
		return "", fmt.Errorf("unable to execute rails concole cmd %q: %w", commandString, err)
	}

	// TODO: Rewrite this to use a separate goroutine plus Scanner on
	// line-by-line basis. For now we take a very simple approach as the amount
	// of data is very small on every search of the string and 99.9% of
	// commands should finish executing in 1-2 seconds so well below 10s limit.
	for i := 0; i < 20; i++ {
		time.Sleep(500 * time.Millisecond)
		bufContents := c.buf.String()
		if strings.Contains(bufContents, "pry(main)") {
			return bufContents, nil
		}
	}

	c.logger.Errorf("rails console output: %s", c.buf.String())
	return "", fmt.Errorf("rails console took too long to respond to command %q", commandString)
}

func (c *RailsConsole) Exit() error {
	c.stdinPipe.Close()
	defer c.debugWriter.Close()

	err := c.Wait()
	if err != nil {
		if exitErr, ok := err.(*exec.ExitError); ok {
			return fmt.Errorf("rails console exited with a non-zero exit code %s", exitErr.Error())
		} else {
			return fmt.Errorf("c.Wait() returned an error: %w\n", err)
		}
	}

	return nil
}
