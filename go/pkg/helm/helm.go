package helm

// Based upon https://github.com/kubeshark/kubeshark/kubernetes/helm/helm.go

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/pkg/errors"
	"gitlab.com/gitlab-org/opstrace/devvm/go/pkg/constants"
	"go.uber.org/zap"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/cli"
	"helm.sh/helm/v3/pkg/downloader"
	"helm.sh/helm/v3/pkg/getter"
	"helm.sh/helm/v3/pkg/kube"
	"helm.sh/helm/v3/pkg/registry"
	"helm.sh/helm/v3/pkg/release"
	"helm.sh/helm/v3/pkg/repo"
	"helm.sh/helm/v3/pkg/storage/driver"
	"sigs.k8s.io/yaml"
)

var settings = cli.New()

type Helm struct {
	Repo             string
	ReleaseName      string
	ReleaseNamespace string
	ChartName        string

	Logger *zap.SugaredLogger
}

func NewHelm(
	repo, releaseName, releaseNamespace string,
	logger *zap.SugaredLogger,
) *Helm {
	return &Helm{
		Repo:             repo,
		ChartName:        releaseName,
		ReleaseNamespace: releaseNamespace,

		Logger: logger,
	}
}

func parseOCIRef(chartRef string) (string, string, error) {
	refTagRegexp := regexp.MustCompile(`^(oci://[^:]+(:[0-9]{1,5})?[^:]+):(.*)$`)
	caps := refTagRegexp.FindStringSubmatch(chartRef)
	if len(caps) != 4 {
		return "", "", errors.Errorf("improperly formatted oci chart reference: %s", chartRef)
	}
	chartRef = caps[1]
	tag := caps[3]

	return chartRef, tag, nil
}

func (h *Helm) Install(valuesData string, version string) (*release.Release, error) {
	actionConfig := new(action.Configuration)
	err := actionConfig.Init(
		kube.GetConfig(
			constants.KindK8sConfigPath,
			"",
			h.ReleaseNamespace,
		),
		h.ReleaseNamespace,
		"secret",
		func(format string, v ...interface{},
		) {
			h.Logger.Infof(format, v...)
		},
	)
	if err != nil {
		return nil, fmt.Errorf("unable to initialize actionConfig: %w", err)
	}

	histClient := action.NewHistory(actionConfig)
	histClient.Max = 1
	releases, err := histClient.Run(h.ReleaseName)
	if err == driver.ErrReleaseNotFound {
		client := action.NewInstall(actionConfig)
		client.Namespace = h.ReleaseNamespace
		client.ReleaseName = h.ReleaseName
		client.Version = version
		client.Wait = true
		client.CreateNamespace = true

		chart, err := h.fetchChart(client.ChartPathOptions)
		if err != nil {
			return nil, fmt.Errorf("unable to locate and download chart: %w", err)
		}

		h.Logger.Infow("Installing using Helm",
			"name", chart.Metadata.Name,
			"version", chart.Metadata.Version,
			"source", chart.Metadata.Sources,
			"kube-version", chart.Metadata.KubeVersion,
			"values.yaml", valuesData,
		)

		var configUnmarshalled map[string]interface{}
		err = yaml.Unmarshal([]byte(valuesData), &configUnmarshalled)
		if err != nil {
			return nil, fmt.Errorf("unmarschaling of values data failed: %w", err)
		}

		rel, err := client.Run(chart, configUnmarshalled)
		if err != nil {
			return nil, fmt.Errorf("install of the chart failed: %w", err)
		}

		return rel, nil
	} else if err != nil {
		return nil, fmt.Errorf("unable to verify if helm release is already present")
	}

	// TODO(prozlach) Chart is already present, do an upgrade
	h.Logger.Info("release %s in NS %s already exists", h.ReleaseName, h.ReleaseNamespace)
	return releases[0], nil
}

func (h *Helm) fetchChart(chartPathOptions action.ChartPathOptions) (*chart.Chart, error) {
	chartURL, err := repo.FindChartInRepoURL(h.Repo, h.ChartName, "", "", "", "", getter.All(&cli.EnvSettings{}))
	if err != nil {
		return nil, fmt.Errorf("error occured while finding chart in the repo: %w", err)
	}

	var cp string
	cp, err = chartPathOptions.LocateChart(chartURL, settings)
	if err != nil {
		return nil, fmt.Errorf("error occured while finding chart in the repo: %w", err)
	}

	m := &downloader.Manager{
		Out:              os.Stdout,
		ChartPath:        cp,
		Keyring:          chartPathOptions.Keyring,
		SkipUpdate:       false,
		Getters:          getter.All(settings),
		RepositoryConfig: settings.RepositoryConfig,
		RepositoryCache:  settings.RepositoryCache,
		Debug:            settings.Debug,
	}

	dl := downloader.ChartDownloader{
		Out:              m.Out,
		Verify:           m.Verify,
		Keyring:          m.Keyring,
		RepositoryConfig: m.RepositoryConfig,
		RepositoryCache:  m.RepositoryCache,
		RegistryClient:   m.RegistryClient,
		Getters:          m.Getters,
		Options: []getter.Option{
			getter.WithInsecureSkipVerifyTLS(false),
		},
	}

	repoPath := filepath.Dir(m.ChartPath)
	err = os.MkdirAll(repoPath, os.ModePerm)
	if err != nil {
		return nil, fmt.Errorf("mkdir %s failed: %w", repoPath, err)
	}

	version := ""
	if registry.IsOCI(chartURL) {
		chartURL, version, err = parseOCIRef(chartURL)
		if err != nil {
			return nil, fmt.Errorf("oci ref %s parsing failed: %w", chartURL, err)
		}
		dl.Options = append(dl.Options,
			getter.WithRegistryClient(m.RegistryClient),
			getter.WithTagName(version))
	}

	h.Logger.Infow("Downloading Helm chart:",
		"url", chartURL,
		"repo-path", repoPath,
	)

	if _, _, err = dl.DownloadTo(chartURL, version, repoPath); err != nil {
		return nil, fmt.Errorf(
			"chart %s:%s from %s downlod failed: %w",
			chartURL, version, repoPath, err,
		)
	}

	var chart *chart.Chart
	chart, err = loader.Load(m.ChartPath)
	if err != nil {
		return nil, fmt.Errorf("chart loading from %s failed: %w", m.ChartPath, err)
	}

	return chart, nil
}

func (h *Helm) Uninstall() (resp *release.UninstallReleaseResponse, err error) {
	actionConfig := new(action.Configuration)
	if err = actionConfig.Init(
		kube.GetConfig(
			constants.KindK8sConfigPath,
			"",
			h.ReleaseNamespace,
		),
		h.ReleaseNamespace,
		"secret",
		func(format string, v ...interface{}) {
			h.Logger.Infof(format, v...)
		},
	); err != nil {
		return
	}

	client := action.NewUninstall(actionConfig)

	resp, err = client.Run(h.ReleaseName)
	if err != nil {
		return
	}

	return
}
